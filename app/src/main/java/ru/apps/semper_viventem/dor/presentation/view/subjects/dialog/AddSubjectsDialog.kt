package ru.apps.semper_viventem.dor.presentation.view.subjects.dialog

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pawegio.kandroid.runAsync
import com.pawegio.kandroid.runOnUiThread
import io.adev.rxwrapper.util.observer
import io.adev.rxwrapper.util.useCase
import kotlinx.android.synthetic.main.dialog_add_subject.*
import kotlinx.android.synthetic.main.layout_dialog_buttons.*
import ru.apps.semper_viventem.dor.App
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.data.common.rx.asyncUseCase
import ru.apps.semper_viventem.dor.domain.iteractor.GetSubjectDetails
import ru.apps.semper_viventem.dor.domain.iteractor.GetTeacherList
import ru.apps.semper_viventem.dor.domain.iteractor.SaveLocalSubject
import ru.apps.semper_viventem.dor.presentation.models.SubjectModel
import ru.apps.semper_viventem.dor.presentation.models.TeacherModel
import ru.apps.semper_viventem.dor.presentation.models.mapper.SubjectMapper
import ru.apps.semper_viventem.dor.presentation.models.mapper.TeacherMapper
import ru.apps.semper_viventem.dor.presentation.navigation.Screens
import ru.apps.semper_viventem.dor.presentation.view.subjects.adapter.SubjectTeachersAdapter
import ru.terrakok.cicerone.Router
import timber.log.Timber
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 08.05.2017.
 *
 * TODO Тут должен быть MVP
 */
class AddSubjectsDialog: DialogFragment() {

    @Inject
    lateinit var mSaveSubject: SaveLocalSubject
    @Inject
    lateinit var mSubjectMapper: SubjectMapper
    @Inject
    lateinit var mGetTeacherList: GetTeacherList
    @Inject
    lateinit var mTeacherMapper: TeacherMapper
    @Inject
    lateinit var mGetSubjectDetails: GetSubjectDetails
    @Inject
    lateinit var mRouter: Router


    /** модель предмета */
    private var mSubject: SubjectModel = SubjectModel()

    /** Список выбранных учителей */
    private var mSelectedTeacher: ArrayList<TeacherModel> = ArrayList()

    /** Список учителей из репозитория */
    private var mTeacherList: List<TeacherModel> = emptyList()

    /** Адаптер для списка учителей */
    private lateinit var mTeacherAdapter: SubjectTeachersAdapter

    /** Слушатель успешного добавления */
    private var mSubjectAddedListenerListener: OnSubjectAddedListener? = null

    init {
        App.component.inject(this)
    }

    companion object {
        const val SUBJECT_ID = "subject_id"
        const val NOT_SUBJECT = -1

        fun newInstance(subjectId: Int = NOT_SUBJECT) : AddSubjectsDialog {
            val fragment = AddSubjectsDialog()
            val args = Bundle()
            args.putInt(SUBJECT_ID, subjectId)

            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.dialog_add_subject, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mSubject.teachers = ArrayList<TeacherModel>()
        mTeacherAdapter = SubjectTeachersAdapter(object : SubjectTeachersAdapter.OnSelectListener {
            override fun onSelectedTeacher(index: Int, teacher: TeacherModel) {
                mSelectedTeacher.add(teacher)
            }

            override fun onUnselectedTeacher(index: Int, teacher: TeacherModel) {
                mSelectedTeacher.removeAll { it.id == teacher.id }
            }

        })

        vRecyclerView.layoutManager = LinearLayoutManager(context)
        vRecyclerView.adapter = mTeacherAdapter

        vButtonAddTeacher.setOnClickListener {
            mRouter.navigateTo(Screens.TEACHERS)
            dismiss()
        }
        vButtonNegative.setOnClickListener { dismiss() }
        vButtonPositive.setOnClickListener { saveData() }

        val currentSubject = arguments.getInt(SUBJECT_ID)

        runAsync {
            loadTeacherList()

            if (currentSubject > NOT_SUBJECT) {
                loadSubjectDetails(currentSubject)
            }
        }

    }

    /**
     * Загрузить информацию о выбранном предмете
     * @param subjectId выбранный предмет
     */
    private fun loadSubjectDetails(subjectId: Int) {
        useCase(mGetSubjectDetails).execute(observer({ subject ->
            runOnUiThread {
                mSubject = mSubjectMapper.map(subject)
                updateForm()
            }
        }, { error ->
            Timber.e(error)
        }), subjectId)
    }

    private fun updateForm() {
        vEditTextSubjectName.setText(mSubject.name)
        vEditTextSubjectClass.setText(mSubject.classRoom)
        vEditTextSubjectInfo.setText(mSubject.info)
        mSelectedTeacher = (mSubject.teachers as ArrayList)
        mSelectedTeacher.forEach {
            mTeacherList.find { teacher ->
                teacher.id==it.id
            }?.isChecked = true
        }

        mTeacherAdapter.setData(mTeacherList)
    }

    /**
     * Сохранить предмет в репозиторий
     */
    private fun saveData() {
        if (!inputDataIsCorrect()) return

        asyncUseCase(mSaveSubject).execute(observer({

        }, {
            dismiss()
            mSubjectAddedListenerListener?.onSubjectAdded()
        }, { error ->
            Timber.e(error)
        }), mSubjectMapper.reverseMap(mSubject))
    }

    /**
     * Загрузить список учителей
     */
    private fun loadTeacherList() {
        useCase(mGetTeacherList).execute(observer({ teachers ->
            runOnUiThread {
                mTeacherList = mTeacherMapper.map(teachers)
                checkTeacherList()
            }
        }, { error ->
            //TODO onError
        }))
    }

    /**
     * Проверить и загрузить список преподавателей
     */
    private fun checkTeacherList() {
        mTeacherAdapter.setData(mTeacherList)
        if (mTeacherList.isNotEmpty()) {
            vButtonAddTeacher.visibility = View.GONE
        } else {
            vButtonAddTeacher.visibility = View.VISIBLE
        }
    }

    /**
     * Проверка введенных данных, и доавление этих данных в объект предмета
     * @return корректно ли заполнена форма
     */
    private fun inputDataIsCorrect() : Boolean {
        if (vEditTextSubjectName.text.isEmpty()) {
            vEditTextSubjectName.error = getString(R.string.fragment_subjects_error_name)
            return false
        } else {
            mSubject.name = vEditTextSubjectName.text.toString()
        }
        mSubject.teachers = mSelectedTeacher
        mSubject.classRoom = vEditTextSubjectClass.text.toString()
        mSubject.info = vEditTextSubjectInfo.text.toString()
        return true
    }

    /**
     * Передать слушатель успешного добавления предмета в репозиторий
     *
     * @param listener слушатель [OnSubjectAddedListener]
     */
    fun setOnAddedListener(listener: OnSubjectAddedListener): AddSubjectsDialog {
        mSubjectAddedListenerListener = listener
        return this
    }

    /**
     * Слушатель добавления предмета
     */
    interface OnSubjectAddedListener {

        /**
         * Предмет был успешно добавлен
         */
        fun onSubjectAdded()
    }
}