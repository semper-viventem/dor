package ru.apps.semper_viventem.dor.presentation.models

import android.graphics.Color
import java.io.Serializable

/**
 * @author Kulikov Konstantin
 * @since 05.05.2017.
 *
 * @property date дата
 * @property subjects список предметов в этот день
 * @property note заметка
 * @property color цвет этого дня
 * @property weekDay день недели (1-7) смотреть в константах
 */
class WeekDayModel (
        var date: String = "",
        var weekDay: Int,
        var subjects: List<SubjectModel> = emptyList(),
        var note: String = "",
        var color: Int = Color.WHITE
) : Serializable {

    override fun toString(): String {
        return "WeekDayModel(date='$date', subjects=$subjects, note='$note', color=$color)"
    }

    fun setCallPeriod(calls: List<CallModel>) {

        subjects.forEach {
            it.timeBegin = ""
            it.timeEnd = ""
        }

        subjects.forEachIndexed { i, subject ->
            if (i >= calls.size) return
            subject.timeBegin = calls[i].begin
            subject.timeEnd = calls[i].end
        }
    }
}