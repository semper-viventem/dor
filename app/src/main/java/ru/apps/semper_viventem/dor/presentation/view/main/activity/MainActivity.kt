package ru.apps.semper_viventem.dor.presentation.view.main.activity

import android.animation.Animator
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import ru.apps.semper_viventem.dor.App
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.presentation.common.fragments.BaseFragment
import ru.apps.semper_viventem.dor.presentation.common.fragments.DisableDrawerMarker
import ru.apps.semper_viventem.dor.presentation.common.fragments.FabFragmentMarker
import ru.apps.semper_viventem.dor.presentation.event.SnackBarAction
import ru.apps.semper_viventem.dor.presentation.navigation.Screens
import ru.apps.semper_viventem.dor.presentation.navigation.mapper.MainScreensMapper
import ru.apps.semper_viventem.dor.presentation.navigation.mapper.ScreensMapper
import ru.apps.semper_viventem.dor.presentation.navigation.provider.MainScreensProvider
import ru.apps.semper_viventem.dor.presentation.navigation.provider.ScreensProvider
import ru.apps.semper_viventem.dor.presentation.presenter.main.MainPresenter
import ru.apps.semper_viventem.dor.presentation.view.about.AboutDialog
import ru.apps.semper_viventem.dor.presentation.view.main.MainView
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.commands.Back
import ru.terrakok.cicerone.commands.Forward
import ru.terrakok.cicerone.commands.Replace
import javax.inject.Inject

class MainActivity : MvpAppCompatActivity(), MainView, NavigationView.OnNavigationItemSelectedListener {

    @InjectPresenter
    lateinit var mMainPresenter: MainPresenter

    @ProvidePresenter
    fun providePresenter(): MainPresenter = MainPresenter(this)

    @Inject
    lateinit var mCicerone: Cicerone<Router>

    private lateinit var mNavigatorHolder: NavigatorHolder
    private lateinit var mNavigator: Navigator
    private val mScreensProvider: ScreensProvider = MainScreensProvider()
    private val mScreensMapper: ScreensMapper = MainScreensMapper()
    private var mToolbar: Toolbar? = null

    private val TAB_CONTAINER_ID = R.id.vFragmentContainer

    private var mCurrentFragment: Fragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Настраеваем ориентацию экрана
        if (resources.configuration.smallestScreenWidthDp >= 600)
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        else
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        App.component.inject(this)
        mNavigatorHolder = mCicerone.navigatorHolder
        mNavigator = Navigator { command ->
            when (command) {
                is Forward -> {
                    val fragment = mScreensProvider.provideScreen(command.screenKey, command.transitionData)
                    forwardTo(fragment)
                }

                is Replace -> {
                    val fragment = mScreensProvider.provideScreen(command.screenKey, command.transitionData)
                    forwardTo(fragment, true)
                }

                is Back -> {
                    back()
                }
            }
        }

        vNavigation.setNavigationItemSelectedListener(this)
        mMainPresenter.draverNavigationSelected(Screens.getDefaultScreen())
    }

    override fun onResume() {
        super.onResume()
        mNavigatorHolder.setNavigator(mNavigator)
    }

    override fun onPause() {
        mNavigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun onBackPressed() {
        if (vDraverLayout.isDrawerOpen(GravityCompat.START)) {
            vDraverLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    /**
     * Слушатель нажатий в меню дравера
     */
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        val screen = mScreensMapper.map(id)

        if (screen != null) {
            mMainPresenter.draverNavigationSelected(screen)
        }

        vDraverLayout.closeDrawer(GravityCompat.START)
        return true
    }

    /**
     * Переключить фрагмент
     */
    private fun forwardTo(fragment: Fragment, addToBackStack: Boolean = false) {
        if (fragment is BaseFragment) {
            setToolbar(fragment.getToolbar(this))
        }
        if (fragment is FabFragmentMarker) {
            vFab.show()
            fragment.fabProvided(vFab)
        } else {
            vFab.hide()
        }
        vDraverLayout.isActivated = fragment !is DisableDrawerMarker
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(TAB_CONTAINER_ID, fragment)
        if (addToBackStack) {
            transaction.addToBackStack(null)
        } else {
            //Очищаем бэк-стек
            for (i in 0 until supportFragmentManager.backStackEntryCount) {
                supportFragmentManager.popBackStack()
            }
        }
        transaction.commit()
        mCurrentFragment = fragment
    }

    private fun back() {
        supportFragmentManager.popBackStack()
    }

    /**
     * Передать тулбар
     * @param toolbar тулбар
     */
    fun setToolbar(toolbar: Toolbar? = null) {
        if (toolbar == null) {
            return
        }
        mToolbar = toolbar
        vToolbarContainer.removeAllViews()
        vToolbarContainer.addView(toolbar)
        val toggle = ActionBarDrawerToggle(
                this, vDraverLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        vDraverLayout.setDrawerListener(toggle)
        toggle.syncState()
    }

    override fun onShowSnackBar(message: String, length: Int, action: SnackBarAction?) {
        val snackbar = Snackbar.make(vCoordinatorLayout, message, length)
        if (action != null) {
            snackbar.setAction(action.text, action.listener)
        }
        snackbar.show()
    }

    override fun hidePremiumMenuItem() {
        vNavigation.menu.findItem(R.id.nav_premium).isVisible = false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!mMainPresenter.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun setBackground(image: String?) {
        Glide.with(this)
                .load(image)
                .crossFade()
                .into(vImageBackground)
    }

    override fun showProgresBar() {
        vProgressBar.visibility = View.VISIBLE
        vProgressBar.animate().scaleX(1.0f).scaleY(1.0f).start()
    }

    override fun hideProgresBar() {
        vProgressBar.animate().scaleX(0.0f).scaleY(0.0f).setListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {}
            override fun onAnimationCancel(animation: Animator?) {}
            override fun onAnimationStart(animation: Animator?) {}

            override fun onAnimationEnd(animation: Animator?) {
                vProgressBar.visibility = View.GONE
            }

        }).start()
    }

    override fun showInformationDialog() {
        AboutDialog(this).show()
    }
}
