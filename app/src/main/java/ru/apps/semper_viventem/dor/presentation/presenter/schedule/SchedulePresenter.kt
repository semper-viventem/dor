package ru.apps.semper_viventem.dor.presentation.presenter.schedule

import android.content.Context
import android.graphics.Color
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.pawegio.kandroid.runAsync
import com.pawegio.kandroid.runOnUiThread
import io.adev.rxwrapper.util.observer
import io.adev.rxwrapper.util.useCase
import ru.apps.semper_viventem.dor.App
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.data.DateManager
import ru.apps.semper_viventem.dor.data.common.MAIN
import ru.apps.semper_viventem.dor.data.common.UserData
import ru.apps.semper_viventem.dor.data.common.WEEK_PERIOD
import ru.apps.semper_viventem.dor.data.common.getWeekDayIdList
import ru.apps.semper_viventem.dor.data.common.rx.asyncUseCase
import ru.apps.semper_viventem.dor.data.common.rx.bus.ui.UiBus
import ru.apps.semper_viventem.dor.domain.exception.WeekIsNullException
import ru.apps.semper_viventem.dor.domain.iteractor.GetCallListByDay
import ru.apps.semper_viventem.dor.domain.iteractor.GetNoteByParams
import ru.apps.semper_viventem.dor.domain.iteractor.GetWeekByPeriod
import ru.apps.semper_viventem.dor.domain.iteractor.SaveLocalWeek
import ru.apps.semper_viventem.dor.domain.model.Note
import ru.apps.semper_viventem.dor.presentation.event.NotificationEvent
import ru.apps.semper_viventem.dor.presentation.event.ShowProgresEvent
import ru.apps.semper_viventem.dor.presentation.event.ShowProgresEvent.ProgresStatus.HIDE
import ru.apps.semper_viventem.dor.presentation.event.ShowProgresEvent.ProgresStatus.SHOW
import ru.apps.semper_viventem.dor.presentation.models.SubjectModel
import ru.apps.semper_viventem.dor.presentation.models.TemplateModel
import ru.apps.semper_viventem.dor.presentation.models.WeekDayModel
import ru.apps.semper_viventem.dor.presentation.models.WeekModel
import ru.apps.semper_viventem.dor.presentation.models.mapper.CallModelMapper
import ru.apps.semper_viventem.dor.presentation.models.mapper.WeekMapper
import ru.apps.semper_viventem.dor.presentation.navigation.Screens
import ru.apps.semper_viventem.dor.presentation.view.schedule.ScheduleView
import ru.terrakok.cicerone.Router
import timber.log.Timber
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 02.05.2017.
 */
@InjectViewState
class SchedulePresenter : MvpPresenter<ScheduleView>() {

    @Inject
    lateinit var mRouter: Router
    @Inject
    lateinit var mContext: Context
    @Inject
    lateinit var mWeekMapper: WeekMapper
    @Inject
    lateinit var mGetWeekByPeriod: GetWeekByPeriod
    @Inject
    lateinit var mRxBus: UiBus
    @Inject
    lateinit var mGetCallByDay: GetCallListByDay
    @Inject
    lateinit var mSaveWeek: SaveLocalWeek
    @Inject
    lateinit var mCallMapper: CallModelMapper
    @Inject
    lateinit var mUserData: UserData
    @Inject
    lateinit var mGetNote: GetNoteByParams
    @Inject
    lateinit var mUiBus: UiBus

    init {
        App.component.inject(this)
    }

    private var mEditMode: Boolean = false
    private var mOffset: Int = 0
    private var mPeriod: String = DateManager().getWeekPeriod(mOffset)

    private var mWeek: WeekModel? = null

    /**
     * Загрузить расписание
     */
    fun loadSchedule(needSearchCurrentDay: Boolean = false) {
        mPeriod = DateManager().getWeekPeriod(mOffset)

        mUiBus.post(ShowProgresEvent(SHOW))

        runAsync {

            //Загружаем само расписание
            useCase(mGetWeekByPeriod).execute(observer({ week ->
                mWeek = mWeekMapper.map(week)
            }, { error ->
                if (error is WeekIsNullException) {
                    runOnUiThread { weekIsEmpty() }
                } else {
                    error.printStackTrace()
                    mRxBus.post(NotificationEvent(mContext.getString(R.string.error_with_loading_week_details), NotificationEvent.SHORT))
                }
            }), mPeriod)

            //Загружаем список заметок по занятиям
            mWeek?.weekDays?.forEach { weekDay ->
                weekDay.subjects.forEachIndexed { position, subject ->
                    useCase(mGetNote).execute(observer({ note ->
                        subject.homeWork = note.text
                        subject.imageQuantity = note.photos.size
                    }, { error ->
                        Timber.e(error)
                    }), Note.NoteParams(subject.id, weekDay.date, position))
                }
            }

            //загружаем общее расписание звонков
            useCase(mGetCallByDay).execute(observer({ calls ->
                mWeek?.weekDays?.forEach {
                    it.setCallPeriod(mCallMapper.map(calls))
                }
            }, { error ->
                Timber.e(error)
            }), MAIN)

            // загружаем альтернативные звонки
            mWeek?.weekDays?.forEach { weekDay ->
                useCase(mGetCallByDay).execute(observer({ calls ->
                    if (calls.isNotEmpty()) {
                        weekDay.setCallPeriod(mCallMapper.map(calls))
                    }
                }, { error ->
                    Timber.e(error)
                }), weekDay.weekDay)
            }

            //Выбираем текущий день (если это нужно)
            var currentDay: Int? = null
            if (needSearchCurrentDay) {
                mWeek?.weekDays?.forEachIndexed { index, weekDayModel ->
                    if (weekDayModel.date == DateManager().getNowDate()) currentDay = index
                }
            }

            //Пинаем Ui
            runOnUiThread {
                if (mWeek != null) viewState.onWeekLoadedComplite(mWeek!!, currentDay)
                mUiBus.post(ShowProgresEvent(HIDE))
            }
        }
    }

    /**
     * Создать и сохранить пустое расписание
     */
    private fun createAndSaveEmptyWeek() {
        val emptyWeek = WeekModel(
                period = mPeriod,
                weekDays = getWeekDayIdList().map {
                    WeekDayModel(
                            date = DateManager().getWeekdayDate(it, mOffset),
                            color = Color.WHITE,
                            weekDay = it
                    )
                }
        )

        saveWeek(emptyWeek)
    }

    /**
     * Сохранить неделю
     * @param weekModel модель недели
     */
    fun saveWeek(weekModel: WeekModel) {
        asyncUseCase(mSaveWeek).execute(observer({
            // onNext
        }, {
            loadSchedule()
        }, { error ->
            error.printStackTrace()
        }), mWeekMapper.reverseMap(weekModel))
    }

    /**
     * Если неделя пришла пустая
     */
    private fun weekIsEmpty() {
        //TODO
        viewState.onWeekIsEmpty()
        createAndSaveEmptyWeek()
    }

    /**
     * Увеличить смещение
     */
    fun nextOffset() {
        mOffset += WEEK_PERIOD
        loadSchedule()
    }

    /**
     * Уменьшить смещение
     */
    fun previousOffset() {
        mOffset -= WEEK_PERIOD
        loadSchedule()
    }

    /**
     * Обнулить смещение
     */
    fun nowOffset() {
        mOffset = 0
        loadSchedule(true)
    }

    /**
     * Выбран какой-то предмет
     * В зависимости от режима будет выполнено разное действие
     *
     * @param position позиция предмета в списке предметов за день
     * @param weekDay день недели
     * @param subject модель предмета
     */
    fun onSubjectSelected(position: Int, weekDay: Int, subject: SubjectModel) {
        if (mEditMode) {
            viewState.showReplaceSubject(position, weekDay)
        } else {
            if (mWeek != null)
                viewState.showSubjectDetails(subject, mWeek!!.getWeekday(weekDay)!!.date, position)
        }
    }

    /**
     * Нажата кнопка долгого тапа на предмет
     * @param subject модель предмета
     */
    fun onSubjectLongSelected(subject: SubjectModel) {
        if (mEditMode) {
            //TODO удалять выбранный предмет
        } else {
            //TODO показывать диалог детализации предмета
        }
    }

    /**
     * Нажата кнопка добавления нового предмета
     * @param weekDay день недели
     */
    fun onSubjectAddSelected(weekDay: Int) {
        viewState.showAddSubject(weekDay)
    }

    /**
     * Добавить новый предмет в расписание
     * @param weekDay день недели (из констант)
     * @param subject предмет
     */
    fun onAddSubject(weekDay: Int, subject: SubjectModel) {
        (mWeek ?: return).addNewSubject(weekDay, subject)
        saveWeek(mWeek ?: return)
    }

    /**
     * Заменить предмет в расписании
     * @param weekDay день недели (из констант)
     * @param oldSubjectPosition позиция старого предмета
     * @param newSubject модель нового предмета
     */
    fun onReplaceSubject(weekDay: Int, oldSubjectPosition: Int, newSubject: SubjectModel) {
        (mWeek ?: return).replaceSubject(weekDay, oldSubjectPosition, newSubject)
        saveWeek(mWeek ?: return)
    }

    /**
     * Удалить предмет из расписания
     * @param weekDay день недели
     * @param position позиция предмета
     */
    fun onRemoveSubject(weekDay: Int, position: Int) {
        ((mWeek ?: return).getWeekday(weekDay)?.subjects as ArrayList).removeAt(position)
        saveWeek(mWeek ?: return)
    }

    fun onSwitchToAddSubjectMenu() {
        mRouter.navigateTo(Screens.SUBJECTS)
    }

    /**
     * Переключить режим расписания (редактирование/просмотр)
     */
    fun switchEditMode() {
        mEditMode = !mEditMode
        checkEditMode()
    }

    /**
     * Проверить режим и преобразовать вью под него
     */
    private fun checkEditMode() {
        if (mEditMode)
            viewState.turnOnEditMode()
        else
            viewState.turnOffEditMode()
    }

    /**
     * Нажата кнопка меню "выбрать шаблон расписания"
     */
    fun onSelectTemplateSelected() {
        viewState.showDialogSelectTemplate()
    }

    fun onTemplateSelected(template: TemplateModel) {
        if (mWeek == null) return
        template.days.forEachIndexed { index, weekDayModel ->
            weekDayModel.date = mWeek!!.weekDays[index].date //TODO может быть обращение к несуществующему индексу
        }
        mWeek?.weekDays = template.days
        saveWeek(mWeek!!)
    }

    /**
     * Нажата кнопка меню "сохранить шаблон расписания"
     */
    fun onSaveTemplateSelected() {
        if (mWeek != null) {
            viewState.showDialogSaveTemplate(mWeek!!)
        }
    }

    /**
     * Событие, вызываемое после успешного сохранения шаблона
     * @param name имя сохраненного шаблона
     */
    fun onTemplateIsSaved(name: String)
            = mRxBus.post(NotificationEvent(mContext.getString(R.string.notification_template_was_be_saved, name)))
}