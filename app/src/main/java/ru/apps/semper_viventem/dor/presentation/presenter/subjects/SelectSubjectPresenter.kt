package ru.apps.semper_viventem.dor.presentation.presenter.subjects

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.adev.rxwrapper.util.observer
import ru.apps.semper_viventem.dor.App
import ru.apps.semper_viventem.dor.data.common.rx.asyncUseCase
import ru.apps.semper_viventem.dor.domain.iteractor.GetLocalSubjectList
import ru.apps.semper_viventem.dor.presentation.common.comporator.SubjectComparator
import ru.apps.semper_viventem.dor.presentation.models.SubjectModel
import ru.apps.semper_viventem.dor.presentation.models.mapper.SubjectMapper
import ru.apps.semper_viventem.dor.presentation.view.schedule.SelectSubjectDialogView
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 26.06.2017.
 */
@InjectViewState
class SelectSubjectPresenter : MvpPresenter<SelectSubjectDialogView>() {

    @Inject
    lateinit var mGetSubjectList: GetLocalSubjectList
    @Inject
    lateinit var mSubjectModelMapper: SubjectMapper

    private var mSubjectList: List<SubjectModel> = emptyList()

    init {
        App.component.inject(this)
    }

    fun loadSubjects() {
        asyncUseCase(mGetSubjectList).execute(observer({ subjects ->
            mSubjectList = mSubjectModelMapper.map(subjects)
            mSubjectList = mSubjectList.sortedWith(SubjectComparator())
            viewState.onSubjectLoaded(mSubjectList)
        }, { error ->
            error.printStackTrace()
        }))
    }
}