package ru.apps.semper_viventem.dor.domain.model

/**
 * @author Kulikov Konstantin
 * @since 11.06.2017.
 */
class Call (
        var id: Int = 0,
        var begin: String = "",
        var end: String = "",
        var day: Int = 0
) {
    override fun toString(): String {
        return "Call(id=$id, begin='$begin', end='$end', day=$day)"
    }
}