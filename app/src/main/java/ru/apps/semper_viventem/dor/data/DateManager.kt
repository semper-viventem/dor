package ru.apps.semper_viventem.dor.data

import ru.apps.semper_viventem.dor.data.common.MONDAY
import ru.apps.semper_viventem.dor.data.common.SUNDAY
import ru.apps.semper_viventem.dor.presentation.common.WeekDayIdMapper
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Singleton

/**
 * @author Kulikov Konstantin
 * @since 01.05.2017.
 */

@Singleton
class DateManager {

    private val mDateFormat: SimpleDateFormat = SimpleDateFormat("dd.MM.yyyy")
    private val mCalendar: Calendar = Calendar.getInstance()

    /**
     * Узнать день недели по дате
     *
     * @param weekDay дань недели
     * @param offset смещение даты в днях
     */
    fun getWeekdayDate(weekDay: Int, offset: Int) : String {
        mCalendar.time = Date()
        mCalendar.add(Calendar.DATE, offset)
        mCalendar.set(Calendar.DAY_OF_WEEK, WeekDayIdMapper.mapToCalendar(weekDay))

        return mDateFormat.format(mCalendar.time)
    }

    /**
     * Период недели
     *
     * @param offset смещение даты в днях
     */
    fun getWeekPeriod(offset: Int = 0) : String {
        val monday = getWeekdayDate(MONDAY, offset)
        val sunday = getWeekdayDate(SUNDAY, offset)
        return monday + sunday
    }

    fun getNowDate(): String {
        return mDateFormat.format(Date())
    }
}
