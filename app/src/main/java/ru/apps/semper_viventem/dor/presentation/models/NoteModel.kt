package ru.apps.semper_viventem.dor.presentation.models

import java.io.Serializable

/**
 * @author Kulikov Konstantin
 * @since 13.07.2017.
 *
 * @property id идентификатор
 * @property text текст заметки
 * @property params параметры заметки
 * @property photos список прикрепленных фотографий
 */
class NoteModel (
        var id: Int = 0,
        var text: String = "",
        var params: NoteParams? = null,
        var photos: ArrayList<String> = arrayListOf()
) {
    /**
     * Параметры идентификации заметки
     *
     * @property parentId идентификатор родителя
     * @property date дата к которой зкметка относится
     * @property position позиция этой заметки в расписании
     */
    class NoteParams (
            var parentId: Int = 0,
            var date: String = "",
            var position: Int = 0
    ) : Serializable
}
