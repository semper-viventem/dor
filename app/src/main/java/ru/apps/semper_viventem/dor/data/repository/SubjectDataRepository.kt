package ru.apps.semper_viventem.dor.data.repository

import io.realm.Realm
import ru.apps.semper_viventem.dor.data.entity.SubjectEntity
import ru.apps.semper_viventem.dor.data.entity.mapper.SubjectEntityMapper
import ru.apps.semper_viventem.dor.domain.model.Subject
import ru.apps.semper_viventem.dor.domain.repository.SubjectRepository
import javax.inject.Inject
import javax.inject.Singleton


/**
 * @author Kulikov Konstantin
 * @since 08.05.2017.
 */
@Singleton
class SubjectDataRepository @Inject constructor(
        private val mSubjectEntityMapper: SubjectEntityMapper
) : SubjectRepository {

    override fun getSubjects(): List<Subject> {
        return Realm.getDefaultInstance().use { realm ->
            val result = realm.where(SubjectEntity::class.java)
                    .findAll()
            mSubjectEntityMapper.map(result)
        }
    }

    override fun addSubject(subject: Subject) {
        val item = mSubjectEntityMapper.reverseMap(subject)
        Realm.getDefaultInstance().use { realm ->
            if (item.id == null || item.id == 0) {
                item.id = getNextKey(realm)
            }
            realm.executeTransaction { realm ->
                realm.copyToRealmOrUpdate(item)
            }
        }
    }

    override fun removeSubjectById(subjectId: Int) {
        Realm.getDefaultInstance().use { realm ->
            realm.executeTransaction { realm ->
                val result = realm.where(SubjectEntity::class.java)
                        .equalTo(SubjectEntity.ID, subjectId)
                        .findAll()
                result.deleteAllFromRealm()
            }
        }
    }

    override fun getSubjectById(subjectId: Int): Subject {

        Realm.getDefaultInstance().use { realm ->
            val result = realm.where(SubjectEntity::class.java)
                    .equalTo(SubjectEntity.ID, subjectId)
                    .findFirst()
            if (result == null) {
                throw IllegalStateException("subject item not found with id='$subjectId'")
            } else {
                return mSubjectEntityMapper.map(result)
            }
        }
    }

    fun getNextKey(realm: Realm): Int {
        val maxKey = realm.where(SubjectEntity::class.java).max(SubjectEntity.ID)
        return if (maxKey != null) maxKey.toInt() + 1 else 1
    }
}