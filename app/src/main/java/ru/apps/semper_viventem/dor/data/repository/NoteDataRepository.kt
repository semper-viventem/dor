package ru.apps.semper_viventem.dor.data.repository

import io.realm.Realm
import ru.apps.semper_viventem.dor.data.entity.NoteEntity
import ru.apps.semper_viventem.dor.data.entity.mapper.NoteEntityMapper
import ru.apps.semper_viventem.dor.domain.model.Note
import ru.apps.semper_viventem.dor.domain.repository.NoteRepository
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Kulikov Konstantin
 * @since 22.07.2017.
 */
@Singleton
class NoteDataRepository @Inject constructor(
        private val mNoteEntityMapper: NoteEntityMapper
): NoteRepository {

    override fun addNote(note: Note) {
        Realm.getDefaultInstance().use { realm ->
            if (note.id == 0) note.id = getNextKey(realm)

            realm.executeTransaction { realm ->
                realm.copyToRealmOrUpdate(mNoteEntityMapper.reverseMap(note))
            }
        }
    }

    override fun getAllNotes(): List<Note> {
        return Realm.getDefaultInstance().use { realm ->
            val result = realm.where(NoteEntity::class.java)
                    .findAll()
            mNoteEntityMapper.map(result)
        }
    }

    override fun getNoteById(noteId: Int): Note {
        return Realm.getDefaultInstance().use { realm ->
            val result = realm.where(NoteEntity::class.java)
                    .equalTo(NoteEntity.ID, noteId)
                    .findFirst()
            if (result == null) {
                throw IllegalStateException("note item not found with id='$noteId'")
            } else {
                return mNoteEntityMapper.map(result)
            }
        }
    }

    override fun getNoteByParas(noteParams: Note.NoteParams): Note {
        return Realm.getDefaultInstance().use { realm ->
            val result = realm.where(NoteEntity::class.java)
                    .equalTo(NoteEntity.PARENT_ID, noteParams.parentId)
                    .equalTo(NoteEntity.POSITION, noteParams.position)
                    .equalTo(NoteEntity.DATE, noteParams.date)
                    .findFirst()
            if (result == null) {
                throw IllegalStateException("note item not found")
            } else {
                return mNoteEntityMapper.map(result)
            }
        }
    }

    private fun getNextKey(realm: Realm): Int {
        val maxKey = realm.where(NoteEntity::class.java).max(NoteEntity.ID)
        return if (maxKey != null) maxKey.toInt() + 1 else 1
    }
}