package ru.apps.semper_viventem.dor.data.common

/**
 * @author Kulikov Konstantin
 * @since 11.06.2017.
 */

/** Смещение на одну неделю **/
const val WEEK_PERIOD = 7

/** Общее рассписание **/
const val MAIN = 0

/** Понедельник **/
const val MONDAY = 1

/** Вторник **/
const val TUESDAY = 2

/** Среда **/
const val WEDNESDAY = 3

/** Четверг **/
const val THURSDAY = 4

/** Пятница **/
const val FRIDAY = 5

/** Суббота **/
const val SATURDAY = 6

/** Воскресенье **/
const val SUNDAY = 7

fun getWeekDayIdList(): List<Int> = arrayListOf(
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY,
        SUNDAY
)