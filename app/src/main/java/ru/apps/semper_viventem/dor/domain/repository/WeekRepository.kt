package ru.apps.semper_viventem.dor.domain.repository

import ru.apps.semper_viventem.dor.domain.model.Week

/**
 * @author Kulikov Konstantin
 * @since 25.06.2017.
 */
interface WeekRepository {

    /**
     * Добавить новую неделю
     * @param week модель недели
     */
    fun addNewDay(week: Week)

    /**
     * Получить неделю по ее периоду
     * @param period период искомой недели
     * @return Модель недели [Week], или [null]
     */
    fun getWeek(period: String): Week?

    /**
     * Получить список всех недель
     * @return список недель [Week]
     */
    fun getAllWeek(): List<Week>
}