package ru.apps.semper_viventem.dor.domain.repository

import ru.apps.semper_viventem.dor.domain.model.Call


/**
 * @author Kulikov Konstantin
 * @since 11.06.2017.
 */
interface CallRepository {

    /**
     * Добавить новый звонок
     * @param call объект звонка
     */
    fun addCall(call: Call)

    /**
     * Вернуть список звонков на какой-то день
     * @param day день недели [data.common.const]
     * @return список звонков
     */
    fun getCallByDay(day: Int): List<Call>

    /**
     * Вернуть все звонки
     * @return список всех звонков
     */
    fun getAllCalls(): List<Call>

    /**
     * Вернуть звонок по идентификатору
     * @param id идентификатор искомого звонка
     */
    fun getCallById(id: Int): Call

    /**
     * Удалить звонок по идентификатору
     * @param id идентификатор
     */
    fun removeCall(id: Int)
}