package ru.apps.semper_viventem.dor.presentation.view.template.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.pawegio.kandroid.inflateLayout
import kotlinx.android.synthetic.main.holder_template.view.*
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.presentation.models.TemplateModel

/**
 * @author Kulikov Konstantin
 * @since 10.07.2017.
 */
class TemplateAdapter (
        private val mListener: OnSelectListener
) : RecyclerView.Adapter<TemplateAdapter.TemplateHolder>() {

    private var mTemplateList: List<TemplateModel> = emptyList()

    fun setData(templates: List<TemplateModel>) {
        mTemplateList = templates
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TemplateHolder {
        return TemplateHolder(parent.context.inflateLayout(R.layout.holder_template, attachToRoot = false))
    }

    override fun onBindViewHolder(holder: TemplateHolder?, position: Int) {
        holder?.bind(mTemplateList[position])
    }

    override fun getItemCount(): Int {
        return mTemplateList.size
    }

    inner class TemplateHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(template: TemplateModel) {
            itemView.vTextTemplateName.text = template.name
            itemView.setOnClickListener { mListener.onSelected(template) }
            itemView.vButtonRemove.setOnClickListener {
                notifyItemRemoved(position)
                (mTemplateList as MutableList).remove(template)
                mListener.onRemove(template)
            }
        }
    }

    interface OnSelectListener {

        /**
         * Выбран элемент
         * @param templateModel выбранный шаблон
         */
        fun onSelected(templateModel: TemplateModel)

        /**
         * Выбран элемент для удаления
         * @param templateModel выбранный шаблон
         */
        fun onRemove(templateModel: TemplateModel)
    }
}