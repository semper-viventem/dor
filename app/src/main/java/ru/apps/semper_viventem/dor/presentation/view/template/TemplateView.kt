package ru.apps.semper_viventem.dor.presentation.view.template

import com.arellomobile.mvp.MvpView
import ru.apps.semper_viventem.dor.presentation.models.TemplateModel

/**
 * @author Kulikov Konstantin
 * @since 05.05.2017.
 */
interface TemplateView: MvpView {

    fun onTemplateListLoaded(templates: List<TemplateModel>)
}