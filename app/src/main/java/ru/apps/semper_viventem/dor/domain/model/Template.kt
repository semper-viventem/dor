package ru.apps.semper_viventem.dor.domain.model

/**
 * @author Kulikov Konstantin
 * @since 08.07.2017.
 */
class Template (
        var id: Int?,
        val name: String,
        val days: List<WeekDay>
)