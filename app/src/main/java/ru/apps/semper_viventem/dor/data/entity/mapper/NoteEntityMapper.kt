package ru.apps.semper_viventem.dor.data.entity.mapper

import io.realm.RealmList
import ru.apps.semper_viventem.dor.data.entity.NoteEntity
import ru.apps.semper_viventem.dor.data.entity.RealmString
import ru.apps.semper_viventem.dor.domain.model.Note
import ru.apps.semper_viventem.dor.domain.model.mapper.TwoWayMapper
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 22.07.2017.
 */
class NoteEntityMapper @Inject constructor() : TwoWayMapper<NoteEntity, Note> {

    override fun reverseMap(from: Note): NoteEntity = NoteEntity().apply {
        id = from.id
        text = from.text
        photos = RealmList()
        from.photos.mapTo(photos!!) { RealmString().apply { text = it } }
        parentId = from.params?.parentId?:0
        date = from.params?.date?:""
        position = from.params?.position?:0
    }

    override fun map(from: NoteEntity): Note = Note(
            id = from.id,
            text = from.text,
            photos = ArrayList(from.photos?.map{ it.text }?.toList()),
            params = Note.NoteParams(
                    parentId = from.parentId,
                    date = from.date,
                    position = from.position
            )
    )
}