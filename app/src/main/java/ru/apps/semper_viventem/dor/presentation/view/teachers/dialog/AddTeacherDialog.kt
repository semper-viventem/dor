package ru.apps.semper_viventem.dor.presentation.view.teachers.dialog

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.adev.rxwrapper.util.observer
import kotlinx.android.synthetic.main.dialog_add_teacher.*
import kotlinx.android.synthetic.main.layout_dialog_buttons.*
import ru.apps.semper_viventem.dor.App
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.data.common.rx.asyncUseCase
import ru.apps.semper_viventem.dor.domain.iteractor.GetTeacherById
import ru.apps.semper_viventem.dor.domain.iteractor.SaveLocalTeacher
import ru.apps.semper_viventem.dor.presentation.models.TeacherModel
import ru.apps.semper_viventem.dor.presentation.models.mapper.TeacherMapper
import timber.log.Timber
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 10.05.2017.
 *
 * TODO Тут должен быть MVP
 */
class AddTeacherDialog : DialogFragment() {

    companion object {
        const val TEACHER_ID = "teacher_id"
        const val NOT_TEACHER = -1

        fun newInstance(teacherId: Int = NOT_TEACHER): AddTeacherDialog {
            val dialog = AddTeacherDialog()
            val args = Bundle()

            args.putInt(TEACHER_ID, teacherId)

            dialog.arguments = args
            return dialog
        }
    }

    @Inject
    lateinit var mSaveTeacher: SaveLocalTeacher
    @Inject
    lateinit var mTeacherMapper: TeacherMapper
    @Inject
    lateinit var mGetTeacherById: GetTeacherById

    /** Слушатель добавления */
    private var mTeacherAddedLisener: OnTeacherAddedListener? = null

    /** Модель преподавателя */
    private var mTeacherModel: TeacherModel = TeacherModel()

    init {
        App.component.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.dialog_add_teacher, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val teacherId = arguments.getInt(TEACHER_ID)

        if (teacherId > NOT_TEACHER) {
            loadTeacherById(teacherId)
        }

        vButtonNegative.setOnClickListener { dismiss() }
        vButtonPositive.setOnClickListener { saveData() }
    }

    /**
     * Загрузить данные текущего преподавателя
     */
    private fun loadTeacherById(teacherId: Int) {
        asyncUseCase(mGetTeacherById).execute(observer({ teacher ->
            mTeacherModel = mTeacherMapper.map(teacher)
            updateForm()
        }, { error ->
            Timber.e(error)
        }), teacherId)
    }

    private fun updateForm() {
        vEditTextFirstName.setText(mTeacherModel.firstName)
        vEditTextLastName.setText(mTeacherModel.lastName)
        vEditTextPatronymic.setText(mTeacherModel.patronymic)
        vEditTextEmail.setText(mTeacherModel.email)
        vEditTextPhone.setText(mTeacherModel.phone)
        vEditTextAddress.setText(mTeacherModel.address)
        vEditTextTeacherInfo.setText(mTeacherModel.info)
    }

    /**
     * Сохранить данные формы
     */
    private fun saveData() {
        if (!inputDataIsCorrect()) return

        asyncUseCase(mSaveTeacher).execute(observer({

        }, {
            dismiss()
            mTeacherAddedLisener?.onTeacherAdded()
        }, { error ->
            Timber.e(error)
        }), mTeacherMapper.reverseMap(mTeacherModel))
    }

    /**
     * Проверить, корректно ли заполнена форма
     * @return корректность заполнения формы [Boolean]
     */
    private fun inputDataIsCorrect(): Boolean {
        if (vEditTextLastName.text.isEmpty() && vEditTextFirstName.text.isEmpty()) {
            vEditTextFirstName.error = getString(R.string.error_with_teacher_data_form)
            vEditTextLastName.error = getString(R.string.error_with_teacher_data_form)
            return false
        }
        mTeacherModel.firstName = vEditTextFirstName.text.toString()
        mTeacherModel.lastName = vEditTextLastName.text.toString()
        mTeacherModel.patronymic = vEditTextPatronymic.text.toString()
        mTeacherModel.phone = vEditTextPhone.text.toString()
        mTeacherModel.email = vEditTextEmail.text.toString()
        mTeacherModel.address = vEditTextAddress.text.toString()
        mTeacherModel.info = vEditTextTeacherInfo.text.toString()
        return true
    }

    /**
     * Передать слушатель успешного добавления препода в репозиторий
     *
     * @param listener слушатель [OnTeacherAddedListener]
     */
    fun setOnAddedListener(listener: OnTeacherAddedListener): AddTeacherDialog {
        mTeacherAddedLisener = listener
        return this
    }

    /**
     * Слушатель добавления препода
     */
    interface OnTeacherAddedListener {

        /**
         * Препод был успешно добавлен
         */
        fun onTeacherAdded()
    }
}