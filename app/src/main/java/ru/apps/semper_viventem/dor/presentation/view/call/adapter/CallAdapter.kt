package ru.apps.semper_viventem.dor.presentation.view.call.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.pawegio.kandroid.inflateLayout
import kotlinx.android.synthetic.main.holder_add_call.view.*
import kotlinx.android.synthetic.main.holder_call.view.*
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.presentation.models.CallModel

/**
 * @author Kulikov Konstantin
 * @since 17.06.2017.
 */
class CallAdapter(
        private val mListener: OnSelectListener
) : RecyclerView.Adapter<CallAdapter.BaseCallHolder>() {

    companion object {
        const val TYPE_CALL = 0
        const val TYPE_ADD_CALL = 1
    }

    private var mData: List<CallModel> = emptyList()

    fun setData(data: List<CallModel>) {
        mData = data
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            mData.size -> TYPE_ADD_CALL
            else -> TYPE_CALL
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseCallHolder {
        return when (viewType) {
            TYPE_CALL -> CallHolder(parent.context.inflateLayout(R.layout.holder_call, attachToRoot = false))
            TYPE_ADD_CALL -> AddCallHolder(parent.context.inflateLayout(R.layout.holder_add_call, attachToRoot = false))
            else -> throw Exception("unknown type: $viewType")
        }
    }

    override fun onBindViewHolder(holder: BaseCallHolder?, position: Int) {
        when (holder) {
            is CallHolder -> holder.bind(mData[position])
            is AddCallHolder -> holder.bind()
        }
    }

    override fun getItemCount(): Int {
        return mData.size+1
    }

    abstract class BaseCallHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    inner class CallHolder(itemView: View) : BaseCallHolder(itemView) {
        fun bind(call: CallModel) {
            itemView.vTextCallPosition.text = itemView.context.getString(R.string.fragment_call_position, position+1)
            itemView.vTextBegin.text = call.begin
            itemView.vTextEnt.text = call.end
            itemView.setOnClickListener { mListener.onItemSelected(call, position+1) }
            itemView.vButtonRemove.setOnClickListener {
                notifyItemRemoved(position)
                (mData as MutableList).remove(call)
                mListener.onRemoveItemSelected(call)
            }
        }
    }

    inner class AddCallHolder(itemView: View) : BaseCallHolder(itemView) {
        fun bind() {
            itemView.vButtonAddCall.setOnClickListener { mListener.onAddCallSelected(position+1) }
        }
    }

    /**
     * Слушатели
     */
    interface OnSelectListener {

        /**
         * Нажатие на элемент звонка
         * @param call модель звонка
         */
        fun onItemSelected(call: CallModel, position: Int)

        /**
         * Нажатие на кнопку удаления звонка
         * @param call мдель звонка
         */
        fun onRemoveItemSelected(call: CallModel)

        /**
         * Нажата кнопка для добавления звонков
         */
        fun onAddCallSelected(position: Int)
    }
}