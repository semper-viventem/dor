package ru.apps.semper_viventem.dor.data.entity.mapper

import ru.apps.semper_viventem.dor.data.entity.TemplateEntity
import ru.apps.semper_viventem.dor.domain.model.Template
import ru.apps.semper_viventem.dor.domain.model.mapper.TwoWayMapper
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 08.07.2017.
 */
class TemplateEntittyMapper @Inject constructor(
        private val mWeekEntityMapper: WeekDayEntityMapper
) : TwoWayMapper<TemplateEntity, Template> {

    override fun reverseMap(from: Template): TemplateEntity {
        return TemplateEntity().apply {
            id = from.id
            name = from.name
            days = RealmListMapper(mWeekEntityMapper).map(from.days)
        }
    }

    override fun map(from: TemplateEntity): Template {
        return Template(
                id = from.id,
                name = from.name,
                days = mWeekEntityMapper.map(from.days ?: emptyList())
        )
    }
}