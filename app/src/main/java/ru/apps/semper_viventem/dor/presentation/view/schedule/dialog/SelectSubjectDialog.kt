package ru.apps.semper_viventem.dor.presentation.view.schedule.dialog

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatDialogFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.dialog_select_subject.*
import kotlinx.android.synthetic.main.layout_header.*
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.presentation.models.SubjectModel
import ru.apps.semper_viventem.dor.presentation.presenter.subjects.SelectSubjectPresenter
import ru.apps.semper_viventem.dor.presentation.view.schedule.SelectSubjectDialogView
import ru.apps.semper_viventem.dor.presentation.view.schedule.adapter.SelectSubjectAdapter

/**
 * @author Kulikov Konstantin
 * @since 26.06.2017.
 */
class SelectSubjectDialog : MvpAppCompatDialogFragment(), SelectSubjectDialogView {

    private var onSubjectSelected: (subject: SubjectModel) -> Unit = {}
    private var onRemoveSelected: () -> Unit = {}
    private var onSubjectsIsEmpty: () -> Unit = {}

    companion object {
        private const val ARG_SHOW_REMOVE_BUTTON = "show_remove_button"

        fun newInstance(
                showRemoveSubjectButton: Boolean = false,
                selectListener: (subject: SubjectModel) -> Unit = {},
                removeListener: () -> Unit = {},
                subjectsIsEmptyListener: () -> Unit = {}
        ): SelectSubjectDialog {
            val fragment = SelectSubjectDialog()
            val args = Bundle()
            args.putBoolean(ARG_SHOW_REMOVE_BUTTON, showRemoveSubjectButton)

            fragment.arguments = args
            fragment.onSubjectSelected = selectListener
            fragment.onRemoveSelected = removeListener
            fragment.onSubjectsIsEmpty = subjectsIsEmptyListener

            return fragment
        }
    }

    @InjectPresenter
    lateinit var mSelectSubjectPresenter: SelectSubjectPresenter

    private lateinit var mAdapter: SelectSubjectAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.dialog_select_subject, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        vTextHeadTitle.text = getString(R.string.select_subject)
        val showRemoveButton = arguments.getBoolean(ARG_SHOW_REMOVE_BUTTON, false)

        mAdapter = SelectSubjectAdapter(object : SelectSubjectAdapter.OnSelectListener {
            override fun onSelected(subject: SubjectModel) {
                onSubjectSelected(subject)
                dismiss()
            }

            override fun removeSubject() {
                onRemoveSelected()
                dismiss()
            }
        }, showRemoveButton)

        vRecyclerView.layoutManager = LinearLayoutManager(context)
        vRecyclerView.adapter = mAdapter

        vButtonNegative.setOnClickListener { dismiss() }
        vButtonAddSubject.setOnClickListener {
            onSubjectsIsEmpty()
            dismiss()
        }

        mSelectSubjectPresenter.loadSubjects()
    }

    override fun onSubjectLoaded(subjects: List<SubjectModel>) {
        if (subjects.isNotEmpty()) {
            mAdapter.setData(subjects)
            vButtonAddSubject.visibility = View.GONE
        } else {
            vButtonAddSubject.visibility = View.VISIBLE
        }
    }
}