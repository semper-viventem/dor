package ru.apps.semper_viventem.dor.presentation.presenter.guide

import android.content.Context
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.apps.semper_viventem.dor.App
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.data.common.UserData
import ru.apps.semper_viventem.dor.presentation.models.GuideModel
import ru.apps.semper_viventem.dor.presentation.navigation.Screens
import ru.apps.semper_viventem.dor.presentation.view.guide.GuideView
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 27.08.2017.
 */
@InjectViewState
class GuidePresenter: MvpPresenter<GuideView>() {

    init {
        App.component.inject(this)
    }

    @Inject
    lateinit var mRouter: Router
    @Inject
    lateinit var mContext: Context
    @Inject
    lateinit var mUserData: UserData

    private var mPages: ArrayList<GuideModel> = arrayListOf()

    fun loadGuide() {
        mPages = arrayListOf(
                GuideModel(
                        mContext.getString(R.string.guide_1_title),
                        mContext.getString(R.string.guide_1_description),
                        R.drawable.img_guide_1
                ),
                GuideModel(
                        mContext.getString(R.string.guide_2_title),
                        mContext.getString(R.string.guide_2_description),
                        R.drawable.img_guide_2
                ),
                GuideModel(
                        mContext.getString(R.string.guide_3_title),
                        mContext.getString(R.string.guide_3_description),
                        R.drawable.img_guide_3
                ),
                GuideModel(
                        mContext.getString(R.string.guide_4_title),
                        mContext.getString(R.string.guide_4_description),
                        R.drawable.img_guide_4
                )
        )

        viewState.guideLoaded(mPages)
    }

    fun onNextSelected() {
        mUserData.setFirstSession(false)
        mRouter.navigateTo(Screens.SCHEDULE)
    }
}