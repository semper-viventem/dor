package ru.apps.semper_viventem.dor.presentation.view.call

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.apps.semper_viventem.dor.presentation.models.CallModel

/**
 * @author Kulikov Konstantin
 * @since 02.05.2017.
 */
interface CallView: MvpView {

    /**
     * Рассписание звонков загружено
     * @param calls список звонков
     */
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun callsLoaded(calls: List<CallModel>)

    /**
     * Открыть диалог добавленя звонков
     * @param weekDay текущий день недели
     */
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showTimeSelectDialog(weekDay: Int, position: Int, callModelId: Int? = null)
}