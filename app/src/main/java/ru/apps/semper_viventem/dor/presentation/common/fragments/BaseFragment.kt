package ru.apps.semper_viventem.dor.presentation.common.fragments

import android.content.Context
import android.support.v7.widget.Toolbar
import com.arellomobile.mvp.MvpAppCompatFragment

/**
 * @author Kulikov Konstantin
 * @since 02.05.2017.
 */
abstract class BaseFragment : MvpAppCompatFragment() {

    /**
     * Возвращает toolbar
     * Внимание! Для доступа к ресурсам использовать [activityContext], поскольку
     * метод вызывается до аттачинга фрагмента!
     *
     * @param activityContext контекст активности
     * @return тулбар для интеграции [Toolbar]
     */
    open fun getToolbar(activityContext: Context): Toolbar? {
        return null
    }
}