package ru.apps.semper_viventem.dor.presentation.models.mapper

import ru.apps.semper_viventem.dor.domain.model.Call
import ru.apps.semper_viventem.dor.domain.model.mapper.TwoWayMapper
import ru.apps.semper_viventem.dor.presentation.models.CallModel
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 11.06.2017.
 */
class CallModelMapper @Inject constructor(): TwoWayMapper<Call, CallModel> {

    override fun reverseMap(from: CallModel): Call {
        return Call(
                id = from.id,
                begin = from.begin,
                end = from.end,
                day = from.day
        )
    }

    override fun map(from: Call): CallModel {
        return CallModel(
                id = from.id,
                begin = from.begin,
                end = from.end,
                day = from.day
        )
    }
}