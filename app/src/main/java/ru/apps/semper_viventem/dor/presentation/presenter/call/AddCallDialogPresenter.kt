package ru.apps.semper_viventem.dor.presentation.presenter.call

import android.content.Context
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.adev.rxwrapper.util.observer
import ru.apps.semper_viventem.dor.App
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.data.common.rx.asyncUseCase
import ru.apps.semper_viventem.dor.data.common.rx.bus.ui.UiBus
import ru.apps.semper_viventem.dor.domain.iteractor.GetCallById
import ru.apps.semper_viventem.dor.domain.iteractor.SaveLocalCall
import ru.apps.semper_viventem.dor.presentation.event.NotificationEvent
import ru.apps.semper_viventem.dor.presentation.models.CallModel
import ru.apps.semper_viventem.dor.presentation.models.mapper.CallModelMapper
import ru.apps.semper_viventem.dor.presentation.view.call.AddCallDialogView
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 18.06.2017.
 */
@InjectViewState
class AddCallDialogPresenter : MvpPresenter<AddCallDialogView>() {

    @Inject
    lateinit var mContext: Context
    @Inject
    lateinit var mGetCallById: GetCallById
    @Inject
    lateinit var mSaveCall: SaveLocalCall
    @Inject
    lateinit var mCallModelMapper: CallModelMapper
    @Inject
    lateinit var mUiBus: UiBus

    init {
        App.component.inject(this)
    }

    private var mCurrentCall: CallModel? = null

    /**
     * Загрузить выбранный звонок
     */
    fun loadCurrentCalls(currentDay: Int, callId: Int?) {
        if (callId != null) {
            asyncUseCase(mGetCallById).execute(observer({ call ->
                mCurrentCall = mCallModelMapper.map(call)
                viewState.callLoaded(mCurrentCall!!)
            }, { error ->
                error.printStackTrace()
            }), callId)
        } else {
            mCurrentCall = CallModel(day = currentDay)
        }

    }

    /**
     * Сохранить звонок
     */
    fun saveCall() {
        if (mCurrentCall == null) {
            mUiBus.post(NotificationEvent(mContext.getString(R.string.notification_error_with_X3)))
            return
        }
        if (mCurrentCall?.begin?.isBlank()?:true || mCurrentCall?.end?.isBlank()?:true) {
            mUiBus.post(NotificationEvent(mContext.getString(R.string.notification_error_add_call_is_blank)))
            return
        }
        asyncUseCase(mSaveCall).execute(observer({

        }, {
            viewState.closeDialog()
        }, { error ->
            mUiBus.post(NotificationEvent(mContext.getString(R.string.error_with_save_call)))
        }), mCallModelMapper.reverseMap(mCurrentCall!!))
    }

    /**
     * Начало урока
     * @param begin строка со временем
     */
    fun setBegin(begin: String) {
        mCurrentCall?.begin = begin
    }

    /**
     * Конец урока
     * @param end строка со временем
     */
    fun setEnd(end: String) {
        mCurrentCall?.end = end
    }
}