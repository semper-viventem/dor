package ru.apps.semper_viventem.dor.presentation.view.guide.fragment

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.google.android.gms.analytics.HitBuilders
import com.google.android.gms.analytics.Tracker
import com.pawegio.kandroid.inflateLayout
import kotlinx.android.synthetic.main.fragment_guide.*
import ru.apps.semper_viventem.dor.App
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.presentation.common.fragments.BaseFragment
import ru.apps.semper_viventem.dor.presentation.common.fragments.DisableDrawerMarker
import ru.apps.semper_viventem.dor.presentation.models.GuideModel
import ru.apps.semper_viventem.dor.presentation.presenter.guide.GuidePresenter
import ru.apps.semper_viventem.dor.presentation.view.guide.GuideView
import ru.apps.semper_viventem.dor.presentation.view.guide.adapter.GuideAdapter
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 27.08.2017.
 */
class GuideFragment: BaseFragment(), GuideView, DisableDrawerMarker {

    companion object {
        fun newInstance() : GuideFragment {
            val fragment = GuideFragment()
            val args = Bundle()

            fragment.arguments = args
            return fragment
        }
    }

    @InjectPresenter
    lateinit var mPresenter: GuidePresenter
    @Inject
    lateinit var mTracker: Tracker

    private lateinit var mAdapter: GuideAdapter

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater?.inflate(R.layout.fragment_guide, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        App.component.inject(this)

        mTracker.setScreenName(this::class.java.simpleName)
        mTracker.send(HitBuilders.ScreenViewBuilder().build())


        mAdapter = GuideAdapter()
        vPager.offscreenPageLimit = 4
        vPager.adapter = mAdapter

        vButtonGuideOk.setOnClickListener { mPresenter.onNextSelected() }
        mPresenter.loadGuide()
    }

    override fun getToolbar(activityContext: Context): Toolbar? {
        val toolbar = activityContext.inflateLayout(R.layout.toolbar_default, attachToRoot = false) as Toolbar
        toolbar.setTitle(R.string.app_name)
        return toolbar
    }

    override fun guideLoaded(pages: List<GuideModel>) {
        mAdapter.setData(pages)

        vIndicator.setViewPager(vPager)
    }
}