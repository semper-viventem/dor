package ru.apps.semper_viventem.dor.presentation.navigation.mapper

import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.presentation.navigation.Screens

/**
 * @author Kulikov Konstantin
 * @since 02.05.2017.
 */
class MainScreensMapper: ScreensMapper {

    override fun map(menuItemId: Int): String? {
        return when (menuItemId) {
            R.id.nav_main -> Screens.SCHEDULE
            R.id.nav_calls -> Screens.CALL
            R.id.nav_templates -> Screens.TEMPLATE
            R.id.nav_teachers -> Screens.TEACHERS
            R.id.nav_subjects -> Screens.SUBJECTS
            R.id.nav_settings -> Screens.SETTINGS
            R.id.nav_information -> Screens.INFORMATION
            R.id.nav_premium -> Screens.PREMIUM
            else -> null
        }
    }

}