package ru.apps.semper_viventem.dor.presentation.models

import java.io.Serializable

@Suppress("UNREACHABLE_CODE")
/**
 * @author Kulikov Konstantin
 * @since 05.05.2017.
 *
 * @property id идентификатор
 * @property name имя предмета
 * @property timeBegin время начала
 * @property timeEnd время окончания
 * @property homeWork задание
 * @property teachers список преподавателей данного предмета [TeacherModel]
 * @property classRoom наименование стандартной аудитории для проведения занятий
 * @property images список изображений
 */
class SubjectModel(
        var id: Int = 0,
        var name: String = "",
        var timeBegin: String = "",
        var timeEnd: String = "",
        var homeWork: String = "",
        var teachers: List<TeacherModel> = emptyList(),
        var classRoom: String = "",
        var imageQuantity: Int = 0,
        var info: String = ""
) : Serializable {

    companion object {
        const val KEY_NAME = "name"
        const val KEY_NOTE = "note"
        const val KEY_ROOM = "room"
        const val KEY_TIME = "time"
        const val KEY_POSTFIX = "postfix"
    }

    fun getTimePeriod(): String {
        if (timeBegin.isEmpty() || timeEnd.isEmpty()) return ""
        return "$timeBegin - $timeEnd"
    }

    override fun toString(): String =
            "SubjectModel(id='$id', name='$name', timeBegin='$timeBegin', timeEnd='$timeEnd', homeWork='$homeWork', teachers=$teachers, classRoom='$classRoom', images=$imageQuantity, info='$info')"

}