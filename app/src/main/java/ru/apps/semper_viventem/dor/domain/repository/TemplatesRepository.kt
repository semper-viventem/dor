package ru.apps.semper_viventem.dor.domain.repository

import ru.apps.semper_viventem.dor.domain.model.Template

/**
 * @author Kulikov Konstantin
 * @since 08.07.2017.
 */
interface TemplatesRepository {

    /**
     * Добавить новый шаблон
     * @param template модель недели для шаблонизации
     */
    fun addTemplate(template: Template)

    /**
     * Получить список всех шаблонов
     * @return список моделей [Template]
     */
    fun getTemplateList(): List<Template>

    /**
     * Получить шаблон по его идентификатору
     * @param id идентификатор шаблона
     * @return модель недели шаблонизации [Template]
     */
    fun getTemplateById(id: Int): Template

    /**
     * Удалить шаблон по его идентификатору
     * @param id идентификатор шаблона
     */
    fun removeTemplate(id: Int)
}