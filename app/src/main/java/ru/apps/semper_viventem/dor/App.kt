package ru.apps.semper_viventem.dor

import android.app.Application
import android.util.Log
import com.crashlytics.android.Crashlytics
import com.facebook.stetho.Stetho
import com.uphyca.stetho_realm.RealmInspectorModulesProvider
import io.fabric.sdk.android.Fabric
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.log.RealmLog
import ru.apps.semper_viventem.dor.di.component.AppComponent
import ru.apps.semper_viventem.dor.di.component.DaggerAppComponent
import ru.apps.semper_viventem.dor.di.module.AppModule
import ru.apps.semper_viventem.dor.di.module.DataModule
import ru.apps.semper_viventem.dor.di.module.NavigationModule
import timber.log.Timber

/**
 * @author Kulikov Konstantin
 * @since 30.04.2017.
 */
class App : Application() {

    companion object {
        lateinit var component: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        Fabric.with(this, Crashlytics())

        initAppComponent()
        initTimber()
        initRealm()
        initStetho()
    }

    private fun initStetho() {
        if (BuildConfig.DEBUG) {
            Stetho.initialize(Stetho.newInitializerBuilder(this)
                    .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                    .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                    .build())
        }
    }

    private fun initAppComponent() {
        component = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .dataModule(DataModule())
                .navigationModule(NavigationModule())
                .build()
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun initRealm() {
        Realm.init(this)
        if (BuildConfig.DEBUG) {
            RealmLog.setLevel(Log.VERBOSE)
        }
        val realmConfiguration = RealmConfiguration.Builder().build()
        Realm.setDefaultConfiguration(realmConfiguration)
    }

    private class CrashReportingTree : Timber.Tree() {

        companion object {
            private const val CRASHLYTICS_KEY_PRIORITY = "priority"
            private const val CRASHLYTICS_KEY_TAG = "tag"
            private const val CRASHLYTICS_KEY_MESSAGE = "message"
        }

        override fun log(priority: Int, tag: String, message: String, throwable: Throwable?) {
            if (priority == Log.VERBOSE || priority == Log.DEBUG || priority == Log.INFO) {
                return
            }
        }

    }
}