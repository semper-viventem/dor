package ru.apps.semper_viventem.dor.domain.repository

import ru.apps.semper_viventem.dor.domain.model.Subject

/**
 * @author Kulikov Konstantin
 * @since 08.05.2017.
 */
interface SubjectRepository {

    /**
     * Получить список всех предметов из базы
     * @return список предметов [Subject]
     */
    fun getSubjects(): List<Subject>

    /**
     * Добавить предмет в базу
     * @param subject модель предмета
     */
    fun addSubject(subject: Subject)

    /**
     * Удалить предмет по его идентификатору
     * @param subjectId идентификатор предмета
     */
    fun removeSubjectById(subjectId: Int)

    /**
     * Получить детализацию о предмете по идентификатору
     */
    fun getSubjectById(subjectId: Int): Subject
}