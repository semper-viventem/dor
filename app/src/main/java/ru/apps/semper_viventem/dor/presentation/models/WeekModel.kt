package ru.apps.semper_viventem.dor.presentation.models

import ru.apps.semper_viventem.dor.data.common.*

/**
 * @author Kulikov Konstantin
 * @since 05.05.2017.
 */
class WeekModel (
        var period: String,
        var weekDays: List<WeekDayModel> = emptyList()
) {

    companion object {
        const val WEEK_MONDAY = MONDAY
        const val WEEK_TUESDAY = TUESDAY
        const val WEEK_WEDNESDAY = WEDNESDAY
        const val WEEK_THURSDAY = THURSDAY
        const val WEEK_FRIDAY = FRIDAY
        const val WEEK_SATURDAY = SATURDAY
        const val WEEK_SUNDAY = SUNDAY
    }

    /**
     * Получить день недели
     *
     * @param day ключ [Int] варианты ключей есть в модели [WeekDayModel]
     */
    fun getWeekday(day: Int): WeekDayModel? = weekDays.find { it.weekDay == day }

    /**
     * Получить день недели
     *
     * @param date текущая дата [String] по которой происходит поиск
     * @return день недели либо [null], если совпадений нет
     */
    fun getWeekdayByDate(date: String): WeekDayModel? = weekDays.find { it.date == date }

    /**
     * Добавить новый предмет в расписание
     * @param weekDay день недели (из констант)
     * @param subject предмет
     */
    fun addNewSubject(weekDay: Int, subject: SubjectModel) {
        (weekDays.find { it.weekDay == weekDay }?.subjects as ArrayList).add(subject)
    }

    /**
     * Заменить предмет в расписании
     * @param weekDay день недели (из констант)
     * @param oldSubjectPosition позиция старого предмета
     * @param newSubject модель нового предмета
     */
    fun replaceSubject(weekDay: Int, oldSubjectPosition: Int, newSubject: SubjectModel) {
        try {
            (weekDays.find { it.weekDay == weekDay }?.subjects as ArrayList)[oldSubjectPosition] = newSubject
        } catch (error: Exception) {
            //TODO обработка ошибки
        }
    }

    override fun toString(): String {
        return "WeekModel(period='$period', mWeekDays=$weekDays)"
    }

}