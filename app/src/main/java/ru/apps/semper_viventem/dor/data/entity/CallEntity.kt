package ru.apps.semper_viventem.dor.data.entity

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import ru.apps.semper_viventem.dor.data.common.MAIN

/**
 * @author Kulikov Konstantin
 * @since 11.06.2017.
 */
@RealmClass
open class CallEntity: RealmObject() {

    companion object {
        const val ID = "id"
    }

    /** Идентификатор **/
    @PrimaryKey
    open var id: Int? = null

    /** Начало **/
    open var begin: String = ""

    /** Конец **/
    open var end: String = ""

    /** день недели смотреть в [data.common.const] **/
    open var day: Int = MAIN
}