package ru.apps.semper_viventem.dor.domain.iteractor

import io.adev.rxwrapper.RxAdapter
import io.reactivex.ObservableEmitter
import ru.apps.semper_viventem.dor.domain.repository.TeacherRepository
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 10.05.2017.
 */
class RemoveTeacher @Inject constructor(
        private val mTeacherRepository: TeacherRepository
) : RxAdapter<Void, Int>() {
    override fun execute(emitter: ObservableEmitter<Void>, criteria: Int?) {
        mTeacherRepository.removeTeacherById(criteria!!)
    }
}