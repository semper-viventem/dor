package ru.apps.semper_viventem.dor.presentation.navigation.mapper

/**
 * @author Kulikov Konstantin
 * @since 02.05.2017.
 */
interface ScreensMapper {

    /**
     * Возвращает выбранный экран [String] в зависимости от
     * идентификатора элемента меню [Int]
     *
     * @param menuItemId идентификатор меню
     * @return соответствующий экран [Screens]
     */
    fun map(menuItemId: Int): String?
}