package ru.apps.semper_viventem.dor.presentation.event

import android.support.design.widget.Snackbar
import ru.apps.semper_viventem.dor.data.common.rx.bus.ui.UiEvent

/**
 * @author Kulikov Konstantin
 * @since 06.05.2017.
 *
 * @property message текст сообщения
 * @property length долгота показа сообщения
 */
class NotificationEvent (
        val message: String,
        val length: Int = SHORT,
        val action: SnackBarAction? = null
) : UiEvent {
    companion object {
        const val LONG = Snackbar.LENGTH_LONG
        const val SHORT = Snackbar.LENGTH_SHORT
        const val INDEFINITE = Snackbar.LENGTH_INDEFINITE
    }
}