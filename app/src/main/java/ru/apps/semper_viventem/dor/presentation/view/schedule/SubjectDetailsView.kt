package ru.apps.semper_viventem.dor.presentation.view.schedule

import com.arellomobile.mvp.MvpView
import ru.apps.semper_viventem.dor.presentation.models.NoteModel

/**
 * @author Kulikov Konstantin
 * @since 13.07.2017.
 */
interface SubjectDetailsView: MvpView {

    /**
     * Заметка загружена
     *
     * @param note модель заметки
     */
    fun noteLoaded(note: NoteModel)

    /**
     * Закрыть диалог
     */
    fun closeDialog()
}