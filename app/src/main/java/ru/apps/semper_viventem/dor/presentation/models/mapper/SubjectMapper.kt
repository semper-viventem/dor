package ru.apps.semper_viventem.dor.presentation.models.mapper

import ru.apps.semper_viventem.dor.domain.model.Subject
import ru.apps.semper_viventem.dor.domain.model.mapper.TwoWayMapper
import ru.apps.semper_viventem.dor.presentation.models.SubjectModel
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 06.05.2017.
 */
class SubjectMapper @Inject constructor(
        private val mTeacherMapper: TeacherMapper
) : TwoWayMapper<Subject, SubjectModel> {
    override fun reverseMap(from: SubjectModel): Subject {
        return Subject(
                id = from.id,
                name = from.name,
                teachers = mTeacherMapper.reverseMap(from.teachers),
                classRoom = from.classRoom,
                info = from.info
        )
    }

    override fun map(from: Subject): SubjectModel {
        return SubjectModel(
                id = from.id,
                name = from.name,
                teachers = mTeacherMapper.map(from.teachers),
                classRoom = from.classRoom,
                info = from.info
        )
    }
}