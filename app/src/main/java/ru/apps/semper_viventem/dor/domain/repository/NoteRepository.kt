package ru.apps.semper_viventem.dor.domain.repository

import ru.apps.semper_viventem.dor.domain.model.Note

/**
 * @author Kulikov Konstantin
 * @since 22.07.2017.
 */
interface NoteRepository {

    /**
     * Добавить заметку
     *
     * @param note модель заметки
     */
    fun addNote(note: Note)

    /**
     * Получить список всех заметок
     *
     * @return список заметок
     */
    fun getAllNotes(): List<Note>

    /**
     * Получить заметку по ее идентификатору
     *
     * @param noteId идентификатор заметки
     * @return модель заметки
     */
    fun getNoteById(noteId: Int): Note

    /**
     * Получить заметку по ее параметрам.
     * <p>Делает выборку по заметкам с учетом переданных параметровё.
     * Возвращает первую найденую модель</p>
     *
     * @param noteParams параметры заметки
     * @return модель заметки
     */
    fun getNoteByParas(noteParams: Note.NoteParams): Note
}