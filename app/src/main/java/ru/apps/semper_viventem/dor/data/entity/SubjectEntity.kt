package ru.apps.semper_viventem.dor.data.entity

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/**
 * @author Kulikov Konstantin
 * @since 09.05.2017.
 */
@RealmClass
open class SubjectEntity: RealmObject() {

    companion object {
        const val ID = "id"
    }

    @PrimaryKey
    open var id: Int? = null

    open var name: String = ""

    open var classRoom: String = ""

    open var teachers: RealmList<TeacherEntity>? = null

    open var info: String = ""
}