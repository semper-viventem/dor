package ru.apps.semper_viventem.dor.presentation.navigation

/**
 * @author Kulikov Konstantin
 * @since 02.05.2017.
 */
object Screens {

    const val SCHEDULE = "main"
    const val CALL = "call"
    const val TEMPLATE = "template"
    const val SUBJECTS = "subjects"
    const val TEACHERS = "teachers"
    const val SETTINGS = "settings"
    const val INFORMATION = "information"
    const val PREMIUM = "premium"
    const val LOGIN = "login"
    const val USER = "user"
    const val GUIDE = "guide"

    fun getDefaultScreen() = SCHEDULE
}