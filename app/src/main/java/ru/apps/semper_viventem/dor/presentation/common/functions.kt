package ru.apps.semper_viventem.dor.presentation.common

import android.content.Context
import android.net.Uri
import android.os.Environment
import android.support.v4.content.FileProvider
import ru.apps.semper_viventem.dor.BuildConfig
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.presentation.models.TeacherModel
import java.io.File

/**
 * @author Kulikov Konstantin
 * @since 07.05.2017.
 */

fun formatTeacherName(teachers: List<TeacherModel>): String {
    var name: String = ""
    teachers.forEachIndexed { index, teacherModel ->
        name += teacherModel.getNameFormat()
        if (index < teachers.lastIndex) name += "\n"
    }
    return name
}

fun generateFileUri(context: Context): Uri? {
    val directory = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
            context.resources.getString(R.string.app_name))

    if (!directory.exists()) {
        directory.mkdirs()
    }

    val file = File(directory.path + "/" + "photo_" + System.currentTimeMillis() + ".jpg")

    //Данное решение необходимо для работы в Android 7.0 и выше
    return FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", file)
}