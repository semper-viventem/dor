package ru.apps.semper_viventem.dor.presentation.navigation.provider

import android.support.v4.app.Fragment
import ru.apps.semper_viventem.dor.presentation.navigation.Screens
import ru.apps.semper_viventem.dor.presentation.view.call.fragment.CallFragment
import ru.apps.semper_viventem.dor.presentation.view.guide.fragment.GuideFragment
import ru.apps.semper_viventem.dor.presentation.view.schedule.fragment.ScheduleFragment
import ru.apps.semper_viventem.dor.presentation.view.settings.fragment.SettingsFragment
import ru.apps.semper_viventem.dor.presentation.view.subjects.fragment.SubjectsFragment
import ru.apps.semper_viventem.dor.presentation.view.teachers.fragment.TeachersFragment
import ru.apps.semper_viventem.dor.presentation.view.template.fragment.TemplateFragment

/**
 * @author Kulikov Konstantin
 * @since 02.05.2017.
 */
class MainScreensProvider: ScreensProvider {

    override fun provideScreen(screenName: String, data: Any?): Fragment {
        return when (screenName) {

            Screens.SCHEDULE -> ScheduleFragment.newInstance()
            Screens.CALL -> CallFragment.newInstance()
            Screens.TEMPLATE -> TemplateFragment.newInstance()
            Screens.SUBJECTS -> SubjectsFragment.newInstance()
            Screens.TEACHERS -> TeachersFragment.newInstance()
            Screens.SETTINGS -> SettingsFragment.newInstance()
            Screens.INFORMATION -> TODO()
            Screens.PREMIUM -> TODO()
            Screens.GUIDE -> GuideFragment.newInstance()

            else -> throw IllegalArgumentException("incorrect screen key")
        }
    }

    override fun provideFirstTabScreenName(tabName: String): String = tabName
}