package ru.apps.semper_viventem.dor.data.entity

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/**
 * @author Kulikov Konstantin
 * @since 08.07.2017.
 */
@RealmClass
open class TemplateEntity : RealmObject() {

    companion object {
        const val ID = "id"
    }

    @PrimaryKey
    var id: Int? = null

    var name: String = ""

    var days: RealmList<WeekDayEntity>? = null
}