package ru.apps.semper_viventem.dor.presentation.view.teachers.fragment

import android.content.Context
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.google.android.gms.analytics.HitBuilders
import com.google.android.gms.analytics.Tracker
import com.pawegio.kandroid.inflateLayout
import kotlinx.android.synthetic.main.fragment_teachers.*
import ru.apps.semper_viventem.dor.App
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.presentation.common.factory.DecoratorFactory
import ru.apps.semper_viventem.dor.presentation.common.fragments.BaseFragment
import ru.apps.semper_viventem.dor.presentation.common.fragments.FabFragmentMarker
import ru.apps.semper_viventem.dor.presentation.models.TeacherModel
import ru.apps.semper_viventem.dor.presentation.presenter.teachers.TeachersPresenter
import ru.apps.semper_viventem.dor.presentation.view.teachers.TeachersView
import ru.apps.semper_viventem.dor.presentation.view.teachers.adapter.TeachersAdapter
import ru.apps.semper_viventem.dor.presentation.view.teachers.dialog.AddTeacherDialog
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 05.05.2017.
 */
class TeachersFragment : BaseFragment(), TeachersView, FabFragmentMarker {

    companion object {
        fun newInstance(): TeachersFragment {
            val fragment = TeachersFragment()
            val args = Bundle()

            fragment.arguments = args
            return fragment
        }
    }

    @InjectPresenter
    lateinit var mTeachersPresenter: TeachersPresenter
    @Inject
    lateinit var mTracker: Tracker

    private var mFab: FloatingActionButton? = null

    private lateinit var mTeachersAdapter: TeachersAdapter

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_teachers, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        App.component.inject(this)

        mTracker.setScreenName(this::class.java.simpleName)
        mTracker.send(HitBuilders.ScreenViewBuilder().build())


        mFab?.setImageDrawable(resources.getDrawable(R.drawable.ic_add))
        mFab?.setOnClickListener { mTeachersPresenter.addTeacherSelected() }

        mTeachersAdapter = TeachersAdapter(object : TeachersAdapter.OnSelectListener {
            override fun onRemoveTeacher(teacherModel: TeacherModel) {
                mTeachersPresenter.removeTeacherSelected(teacherModel)
            }

            override fun onSelectedTeacher(teacherModel: TeacherModel) {
                mTeachersPresenter.openTeacherSelected(teacherModel)
            }

        })
        vRecyclerView.layoutManager = LinearLayoutManager(context)
        vRecyclerView.adapter = mTeachersAdapter
        vRecyclerView.addItemDecoration(DecoratorFactory().getDefaultDecorator())
        vRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                if (dy > 0)
                    mFab?.hide()
                else if (dy < 0)
                    mFab?.show()
            }
        })
        mTeachersPresenter.loadTeachers()
    }

    override fun getToolbar(activityContext: Context): Toolbar? {
        val toolbar = activityContext.inflateLayout(R.layout.toolbar_default, attachToRoot = false) as Toolbar
        toolbar.setTitle(R.string.fragment_teachers_title)
        return toolbar
    }

    override fun onTeachersLoaded(subjects: List<TeacherModel>) {
        mTeachersAdapter.setData(subjects)
        if (subjects.isEmpty()) {
            vImageIsEmpty.visibility = View.VISIBLE
        } else {
            vImageIsEmpty.visibility = View.GONE
        }
    }

    override fun showAddTeacherDialog() {
        AddTeacherDialog.newInstance()
                .setOnAddedListener(object : AddTeacherDialog.OnTeacherAddedListener {
                    override fun onTeacherAdded() {
                        mTeachersPresenter.loadTeachers()
                    }
                })
                .show(fragmentManager, "")
    }

    override fun showSelectedTeacherDialog(teacherId: Int) {
        AddTeacherDialog.newInstance(teacherId)
                .setOnAddedListener(object : AddTeacherDialog.OnTeacherAddedListener {
                    override fun onTeacherAdded() {
                        mTeachersPresenter.loadTeachers()
                    }
                })
                .show(fragmentManager, "")
    }

    override fun fabProvided(fab: FloatingActionButton) {
        mFab = fab
    }
}