package ru.apps.semper_viventem.dor.data.entity

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/**
 * @author Kulikov Konstantin
 * @since 22.07.2017.
 */
@RealmClass
open class NoteEntity : RealmObject() {

    companion object {
        const val ID = "id"
        const val PARENT_ID = "parentId"
        const val DATE = "date"
        const val POSITION = "position"
    }

    @PrimaryKey
    var id: Int = 0

    var text: String = ""

    var photos: RealmList<RealmString>? = null

    var parentId: Int = 0

    var date: String = ""

    var position: Int = 0
}