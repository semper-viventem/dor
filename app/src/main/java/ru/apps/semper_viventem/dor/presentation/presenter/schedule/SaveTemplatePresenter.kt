package ru.apps.semper_viventem.dor.presentation.presenter.schedule

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.adev.rxwrapper.util.observer
import ru.apps.semper_viventem.dor.App
import ru.apps.semper_viventem.dor.data.common.rx.asyncUseCase
import ru.apps.semper_viventem.dor.domain.iteractor.SaveLocalTemplate
import ru.apps.semper_viventem.dor.presentation.models.TemplateModel
import ru.apps.semper_viventem.dor.presentation.models.WeekModel
import ru.apps.semper_viventem.dor.presentation.models.mapper.TemplateModelMapper
import ru.apps.semper_viventem.dor.presentation.view.schedule.SaveTemplateView
import timber.log.Timber
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 08.07.2017.
 */
@InjectViewState
class SaveTemplatePresenter : MvpPresenter<SaveTemplateView>() {

    @Inject
    lateinit var mSaveLocalTemplate: SaveLocalTemplate
    @Inject
    lateinit var mTemplateMapper: TemplateModelMapper

    init {
        App.component.inject(this)
    }

    var mTemplate: TemplateModel? = null

    /**
     * Создать новыый шаблон исходя из текущей недели
     * @param week неделя (как бы логично то ни было :D )
     */
    fun createTemplateByWeek(week: WeekModel) {
        mTemplate = TemplateModel(
                id = 0,
                name = "",
                days = week.weekDays
        )
    }

    /**
     * Сохранить текущий шаблон
     * @param name имя шаблона
     */
    fun saveTemplate(name: String) {
        mTemplate?.name = name
        asyncUseCase(mSaveLocalTemplate).execute(observer({
            // onNext
        }, {
            viewState.templateIsSaved(name)
        }, { error ->
            Timber.e(error)
            //TODO обработка ошибок
        }), mTemplateMapper.reverseMap(mTemplate!!))
    }
}