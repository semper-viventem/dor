package ru.apps.semper_viventem.dor.widget

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.RemoteViews
import com.pawegio.kandroid.runAsync
import com.pawegio.kandroid.runOnUiThread
import io.adev.rxwrapper.util.observer
import io.adev.rxwrapper.util.useCase
import ru.apps.semper_viventem.dor.App
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.data.DateManager
import ru.apps.semper_viventem.dor.data.common.MAIN
import ru.apps.semper_viventem.dor.domain.exception.WeekIsNullException
import ru.apps.semper_viventem.dor.domain.iteractor.GetCallListByDay
import ru.apps.semper_viventem.dor.domain.iteractor.GetNoteByParams
import ru.apps.semper_viventem.dor.domain.iteractor.GetWeekByPeriod
import ru.apps.semper_viventem.dor.domain.model.Note
import ru.apps.semper_viventem.dor.presentation.common.WeekDayIdMapper
import ru.apps.semper_viventem.dor.presentation.models.SubjectModel
import ru.apps.semper_viventem.dor.presentation.models.WeekModel
import ru.apps.semper_viventem.dor.presentation.models.mapper.CallModelMapper
import ru.apps.semper_viventem.dor.presentation.models.mapper.WeekMapper
import ru.apps.semper_viventem.dor.presentation.view.main.activity.MainActivity
import timber.log.Timber
import java.util.*
import javax.inject.Inject


/**
 * @author Kulikov Konstantin
 * @since 01.09.2017.
 *
 * TODO: ВНИМАНИЕ, ГОВНОКОД!
 *       Если вы дорожите своим психологическим здоровьем,
 *       и чувством прекрасного, то убедительная
 *       просьба - дальше не смотреть.
 *
 * А если серьезно, то этот вижет есть смысл переделать. Но это будет потом.
 */
class ScheduleWidget : AppWidgetProvider() {

    @Inject
    lateinit var mGetWeekByPeriod: GetWeekByPeriod
    @Inject
    lateinit var mGetCallByDay: GetCallListByDay
    @Inject
    lateinit var mCallMapper: CallModelMapper
    @Inject
    lateinit var mWeekMapper: WeekMapper
    @Inject
    lateinit var mGetNote: GetNoteByParams

    init {
        App.component.inject(this)
    }

    companion object {
        private const val RECEIVE_UPDATE = "receive_update"
        private const val RECEIVE_NEXT = "receive_next"
        private const val RECEIVE_BACK = "receive_back"
    }

    private var mOffset = 0

    private var mPeriod: String = DateManager().getWeekPeriod(mOffset)
    private var mNowDate: String = DateManager().getNowDate()

    private var mWeek: WeekModel? = null

    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray?) {
        super.onUpdate(context, appWidgetManager, appWidgetIds)

        mPeriod = DateManager().getWeekPeriod(mOffset)
        mNowDate = DateManager().getNowDate()

        appWidgetIds?.forEach {
            loadWeekData(context, it, appWidgetManager)
        }
    }

    override fun onDeleted(context: Context?, appWidgetIds: IntArray?) {
        super.onDeleted(context, appWidgetIds)
    }

    private fun getEmplyRemotedView(context: Context, widgetId: Int): RemoteViews {
        val remoteViews = RemoteViews(context.packageName, R.layout.layout_widget)
        if (mWeek != null) {
            val currentDay = mWeek!!.getWeekdayByDate(mNowDate)
            val weekDayTextId = WeekDayIdMapper.mapToTextId(currentDay!!.weekDay)
            //Текст заголовка
            remoteViews.setTextViewText(R.id.vTextWidgetTitle, context.getString(weekDayTextId))
        }

        remoteViews.setViewVisibility(R.id.vTextSubjectListEmpty, View.VISIBLE)

        //Передать данные в список
        val adapter = Intent(context, WidgetAdapterService::class.java)
        adapter.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId)
        adapter.data = Uri.parse(adapter.toUri(Intent.URI_INTENT_SCHEME))
        remoteViews.setRemoteAdapter(R.id.vListView, adapter)

        //Открыть приложение по нажатию
        val openIntent = Intent(context, MainActivity::class.java)
        val pIntent = PendingIntent.getActivity(context, 0, openIntent, 0)
        remoteViews.setOnClickPendingIntent(R.id.vPlaceWidgetHeader, pIntent)

        //Обновление по нажатию
        val updIntent = Intent(context, ScheduleWidget::class.java)
        updIntent.action = RECEIVE_UPDATE
        updIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId)
        val updPIntent = PendingIntent.getBroadcast(context, widgetId, updIntent, 0)
        remoteViews.setOnClickPendingIntent(R.id.vButtonUpdate, updPIntent)


        return remoteViews
    }

    private fun getUpdatedRemoteView(context: Context, widgetId: Int): RemoteViews {
        val currentDay = mWeek!!.getWeekdayByDate(mNowDate)
        val weekDayTextId = WeekDayIdMapper.mapToTextId(currentDay!!.weekDay)

        if (currentDay.subjects.isEmpty()) return getEmplyRemotedView(context, widgetId)

        //Текст заголовка
        val remoteViews = RemoteViews(context.packageName, R.layout.layout_widget)
        remoteViews.setTextViewText(R.id.vTextWidgetTitle, context.getString(weekDayTextId))

        remoteViews.setViewVisibility(R.id.vTextSubjectListEmpty, View.GONE)

        //Передать данные в список
        val adapter = Intent(context, WidgetAdapterService::class.java)
        val postfix = (Math.random() * 1000).toInt() //Потому что хуй его знает
        adapter.putExtra(SubjectModel.KEY_POSTFIX, postfix)
        adapter.putStringArrayListExtra(SubjectModel.KEY_NAME+postfix, currentDay.subjects.map { it.name } as ArrayList<String>)
        adapter.putStringArrayListExtra(SubjectModel.KEY_NOTE+postfix, currentDay.subjects.map { it.homeWork } as ArrayList<String>)
        adapter.putStringArrayListExtra(SubjectModel.KEY_TIME+postfix, currentDay.subjects.map { it.getTimePeriod() } as ArrayList<String>)
        adapter.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId)
        adapter.data = Uri.parse(adapter.toUri(Intent.URI_INTENT_SCHEME))
        remoteViews.setRemoteAdapter(R.id.vListView, adapter)

        //Открыть приложение по нажатию
        val openIntent = Intent(context, MainActivity::class.java)
        val pIntent = PendingIntent.getActivity(context, 0, openIntent, 0)
        remoteViews.setOnClickPendingIntent(R.id.vPlaceWidgetHeader, pIntent)

        //Обновление по нажатию
        val updIntent = Intent(context, ScheduleWidget::class.java)
        updIntent.action = RECEIVE_UPDATE
        updIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId)
        val updPIntent = PendingIntent.getBroadcast(context, widgetId, updIntent, 0)
        remoteViews.setOnClickPendingIntent(R.id.vButtonUpdate, updPIntent)

        return remoteViews
    }

    override fun onReceive(context: Context, intent: Intent) {
        super.onReceive(context, intent)

        when (intent.action) {
            RECEIVE_UPDATE -> {
                val widgetId = intent.extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID)
                if (widgetId == AppWidgetManager.INVALID_APPWIDGET_ID)  return

                loadWeekData(context, widgetId, null)
            }
            RECEIVE_NEXT -> {

            }
        }
    }

    /**
     * Если пришла пустая неделя
     */
    fun weekIsEmpty(context: Context, widgetId: Int, appWidgetManager: AppWidgetManager? = null) {
        val remoteView = getEmplyRemotedView(context, widgetId)

        if (appWidgetManager == null) {
            val thisWidget = ComponentName(context, ScheduleWidget::class.java)
            AppWidgetManager.getInstance(context).updateAppWidget(thisWidget, remoteView)
            val newAppWidgetManager = AppWidgetManager.getInstance(context)
            newAppWidgetManager.updateAppWidget(widgetId, remoteView)
            newAppWidgetManager.notifyAppWidgetViewDataChanged(widgetId, R.id.vListView)
        } else {
            appWidgetManager.updateAppWidget(widgetId, remoteView)
            appWidgetManager.notifyAppWidgetViewDataChanged(widgetId, R.id.vListView)
        }
    }

    /**
     * Неделя была успешно загружена
     */
    fun weekWasBeLoaded(context: Context, widgetId: Int, appWidgetManager: AppWidgetManager? = null) {
        if (mWeek == null) {
            weekIsEmpty(context, widgetId, appWidgetManager); return
        }

        val remoteView = getUpdatedRemoteView(context, widgetId)

        if (appWidgetManager == null) {
            val thisWidget = ComponentName(context, ScheduleWidget::class.java)
            AppWidgetManager.getInstance(context).updateAppWidget(thisWidget, remoteView)
            val newAppWidgetManager = AppWidgetManager.getInstance(context)
            newAppWidgetManager.updateAppWidget(widgetId, remoteView)
            newAppWidgetManager.notifyAppWidgetViewDataChanged(widgetId, R.id.vListView)
        } else {
            appWidgetManager.updateAppWidget(widgetId, remoteView)
            appWidgetManager.notifyAppWidgetViewDataChanged(widgetId, R.id.vListView)
        }
    }

    /**
     * Загрузить расписание
     */
    fun loadWeekData(context: Context, widgetId: Int, appWidgetManager: AppWidgetManager? = null) {

        runAsync {

            //Загружаем само расписание
            useCase(mGetWeekByPeriod).execute(observer({ week ->
                mWeek = mWeekMapper.map(week)
            }, { error ->
                if (error is WeekIsNullException) {
                    runOnUiThread { weekIsEmpty(context, widgetId, appWidgetManager) }
                } else {
                    error.printStackTrace()
                }
            }), mPeriod)

            //Загружаем список заметок по занятиям
            mWeek?.weekDays?.forEach { weekDay ->
                weekDay.subjects.forEachIndexed { position, subject ->
                    useCase(mGetNote).execute(observer({ note ->
                        subject.homeWork = note.text
                        subject.imageQuantity = note.photos.size
                    }, { error ->
                        Timber.e(error)
                    }), Note.NoteParams(subject.id, weekDay.date, position))
                }
            }

            //загружаем общее расписание звонков
            useCase(mGetCallByDay).execute(observer({ calls ->
                mWeek?.weekDays?.forEach {
                    it.setCallPeriod(mCallMapper.map(calls))
                }
            }, { error ->
                Timber.e(error)
            }), MAIN)

            // загружаем альтернативные звонки
            mWeek?.weekDays?.forEach { weekDay ->
                useCase(mGetCallByDay).execute(observer({ calls ->
                    if (calls.isNotEmpty()) {
                        weekDay.setCallPeriod(mCallMapper.map(calls))
                    }
                }, { error ->
                    Timber.e(error)
                }), weekDay.weekDay)
            }

            //Пинаем Ui
            runOnUiThread {
                weekWasBeLoaded(context, widgetId, appWidgetManager)
            }
        }
    }
}