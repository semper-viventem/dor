package ru.apps.semper_viventem.dor.presentation.presenter.teachers

import android.content.Context
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.adev.rxwrapper.util.observer
import ru.apps.semper_viventem.dor.App
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.data.common.rx.asyncUseCase
import ru.apps.semper_viventem.dor.data.common.rx.bus.ui.UiBus
import ru.apps.semper_viventem.dor.domain.iteractor.GetTeacherList
import ru.apps.semper_viventem.dor.domain.iteractor.RemoveTeacher
import ru.apps.semper_viventem.dor.domain.iteractor.SaveLocalTeacher
import ru.apps.semper_viventem.dor.presentation.event.NotificationEvent
import ru.apps.semper_viventem.dor.presentation.event.ShowProgresEvent
import ru.apps.semper_viventem.dor.presentation.event.ShowProgresEvent.ProgresStatus
import ru.apps.semper_viventem.dor.presentation.event.SnackBarAction
import ru.apps.semper_viventem.dor.presentation.models.TeacherModel
import ru.apps.semper_viventem.dor.presentation.models.mapper.TeacherMapper
import ru.apps.semper_viventem.dor.presentation.view.teachers.TeachersView
import timber.log.Timber
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 05.05.2017.
 */
@InjectViewState
class TeachersPresenter: MvpPresenter<TeachersView>() {

    @Inject
    lateinit var mContext: Context
    @Inject
    lateinit var mGetTeachers: GetTeacherList
    @Inject
    lateinit var mRemoveTeacher: RemoveTeacher
    @Inject
    lateinit var mTeacherMapper: TeacherMapper
    @Inject
    lateinit var mSaveTeacher: SaveLocalTeacher
    @Inject
    lateinit var mUiBus: UiBus

    init {
        App.component.inject(this)
    }

    private var mTeacherList: List<TeacherModel> = emptyList()

    /**
     * Загрузить спиок преподавателей
     */
    fun loadTeachers() {
        mUiBus.post(ShowProgresEvent(ProgresStatus.SHOW))
        asyncUseCase(mGetTeachers).execute(observer({ teachers ->
            mTeacherList = mTeacherMapper.map(teachers)
            viewState.onTeachersLoaded(mTeacherList)
            mUiBus.post(ShowProgresEvent(ProgresStatus.HIDE))
        },{ error ->
            Timber.e(error)
            mUiBus.post(NotificationEvent(mContext.getString(R.string.error_with_loading_teachers)))
            mUiBus.post(ShowProgresEvent(ProgresStatus.HIDE))
        }))
    }

    /**
     * Запрос на открытие диалога добавления препода
     */
    fun addTeacherSelected() {
        viewState.showAddTeacherDialog()
    }

    /**
     * Удалить преподавателя
     *
     * @param teacher одель препода
     */
    fun removeTeacherSelected(teacher: TeacherModel) {
        asyncUseCase(mRemoveTeacher).execute(observer({
            //onNext Void
        },{
            (mTeacherList as MutableList).remove(teacher)
            if (mTeacherList.isEmpty()) viewState.onTeachersLoaded(mTeacherList)
            mUiBus.post(NotificationEvent(mContext.getString(R.string.teacher_was_be_loaded), action = SnackBarAction(mContext.getString(R.string.cancel), {
                saveTeacher(teacher)
            })))
        },{ error ->
            Timber.e(error)
        }), teacher.id)
    }

    /**
     * Выбрана модель препода
     *
     * @param teacher модель препода
     */
    fun openTeacherSelected(teacher: TeacherModel) {
        viewState.showSelectedTeacherDialog(teacher.id)
    }

    /**
     * Сохранить преподавателя
     *
     * @param teacher модель преподавателя
     */
    private fun saveTeacher(teacher: TeacherModel) {
        asyncUseCase(mSaveTeacher).execute(observer({
            // onNext
        }, {
            loadTeachers()
        }, { error ->
            error.printStackTrace()
        }), mTeacherMapper.reverseMap(teacher))
    }
}