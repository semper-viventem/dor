package ru.apps.semper_viventem.dor.presentation.view.guide

import com.arellomobile.mvp.MvpView
import ru.apps.semper_viventem.dor.presentation.models.GuideModel

/**
 * @author Kulikov Konstantin
 * @since 27.08.2017.
 */
interface GuideView: MvpView {

    fun guideLoaded(pages: List<GuideModel>)
}