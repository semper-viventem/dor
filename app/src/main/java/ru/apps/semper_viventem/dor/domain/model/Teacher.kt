package ru.apps.semper_viventem.dor.domain.model

/**
 * @author Kulikov Konstantin
 * @since 08.05.2017.
 *
 * @property id идентификатор
 * @property firstName имя преподавателя
 * @property lastName фамилия преподавателя
 * @property patronymic отчество преподавателя
 * @property patronymic телефонный номер преподавателя
 * @property email адрес электронной почты преподавателя
 * @property address адрес преподавателя
 * @property info дополнительная информация о преподавателе
 */
class Teacher(
        val id: Int = 0,
        val firstName: String = "",
        val lastName: String = "",
        val patronymic: String = "",
        val phone: String = "",
        val email: String = "",
        val address: String = "",
        val info: String = ""
) {
    override fun toString(): String {
        return "TeacherModel(id='$id', firstName='$firstName', lastName='$lastName', patronymic='$patronymic', phone='$phone', email='$email', address='$address', info='$info')"
    }
}