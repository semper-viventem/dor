package ru.apps.semper_viventem.dor.data.entity.mapper

import ru.apps.semper_viventem.dor.data.entity.WeekEntity
import ru.apps.semper_viventem.dor.domain.model.Week
import ru.apps.semper_viventem.dor.domain.model.mapper.TwoWayMapper
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 25.06.2017.
 *
 * Этот маппер очень важен.
 */
class WeekEntityMapper @Inject constructor(
        private val mWeekDayEntityMapper: WeekDayEntityMapper
): TwoWayMapper<WeekEntity?, Week?> {

    override fun reverseMap(from: Week?): WeekEntity {
        return WeekEntity().apply {
            period = from?.period
            days = RealmListMapper(mWeekDayEntityMapper).map(from?.weekDays?: emptyList())
        }
    }

    override fun map(from: WeekEntity?): Week? {
        if (from == null) return null
        return Week(
                period = from.period!!,
                weekDays = mWeekDayEntityMapper.map(from.days!!)
        )
    }
}