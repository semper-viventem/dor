package ru.apps.semper_viventem.dor.di.module

import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router
import javax.inject.Singleton

/**
 * @author Kulikov Konstantin
 * @since 02.05.2017.
 */
@Module
class NavigationModule {

    @Singleton
    @Provides
    fun provideCicerone() : Cicerone<Router> = Cicerone.create()

    @Singleton
    @Provides
    fun provideRouter(cicerone: Cicerone<Router>) : Router = cicerone.router
}