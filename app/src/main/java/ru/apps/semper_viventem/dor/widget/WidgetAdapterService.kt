package ru.apps.semper_viventem.dor.widget

import android.content.Intent
import android.widget.RemoteViewsService

/**
 * @author Kulikov Konstantin
 * @since 02.09.2017.
 */
class WidgetAdapterService: RemoteViewsService() {
    override fun onGetViewFactory(intent: Intent): RemoteViewsFactory =
            WidgetAdapter(applicationContext, intent)
}