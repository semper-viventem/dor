package ru.apps.semper_viventem.dor.data.common

/**
 * @author Kulikov Konstantin
 * @since 08.07.2017.
 */

/**
 * Добавить префикс для форматирования дней шаблонов
 */

private const val PREFIX = "template"
private var mBillingStatus: Boolean = false

fun formatTemplateDate(date: String, templateId: String = ""): String = "$PREFIX $templateId $date"

/**
 * Премиальный ли пользовательь?
 */
fun getPremiumStatus(): Boolean {
    return mBillingStatus
}

fun setBillingStatus(status: Boolean) {
    mBillingStatus = status
}