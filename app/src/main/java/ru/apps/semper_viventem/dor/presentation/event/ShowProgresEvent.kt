package ru.apps.semper_viventem.dor.presentation.event

import ru.apps.semper_viventem.dor.data.common.rx.bus.ui.UiEvent

/**
 * @author Kulikov Konstantin
 * @since 26.08.2017.
 */
class ShowProgresEvent(
        val status: ShowProgresEvent.ProgresStatus
) : UiEvent {

    enum class ProgresStatus{
        SHOW,
        HIDE
    }
}