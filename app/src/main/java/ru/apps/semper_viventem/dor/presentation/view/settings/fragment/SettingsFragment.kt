package ru.apps.semper_viventem.dor.presentation.view.settings.fragment

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.bumptech.glide.Glide
import com.google.android.gms.analytics.HitBuilders
import com.google.android.gms.analytics.Tracker
import com.pawegio.kandroid.inflateLayout
import kotlinx.android.synthetic.main.content_settings.*
import kotlinx.android.synthetic.main.fragment_settings.*
import ru.apps.semper_viventem.dor.App
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.presentation.common.PERMISSION_REQUEST_STORAGE
import ru.apps.semper_viventem.dor.presentation.common.RESULT_LOAD_IMAGE_FROM_GALLERY
import ru.apps.semper_viventem.dor.presentation.common.fragments.BaseFragment
import ru.apps.semper_viventem.dor.presentation.presenter.settings.SettingsPresenter
import ru.apps.semper_viventem.dor.presentation.view.settings.SettingsView
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 13.08.2017.
 */
class SettingsFragment : BaseFragment(), SettingsView {

    companion object {
        fun newInstance(): SettingsFragment {
            val fragment = SettingsFragment()
            val args = Bundle()

            fragment.arguments = args
            return  fragment
        }
    }

    @InjectPresenter
    lateinit var mSettingsPresenter: SettingsPresenter
    @Inject
    lateinit var mTracker: Tracker

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        App.component.inject(this)

        mTracker.setScreenName(this::class.java.simpleName)
        mTracker.send(HitBuilders.ScreenViewBuilder().build())


        mSettingsPresenter.loadSettingsData()
    }

    override fun setPremiumMode() {
        vPlacePremiumInfo.visibility = View.GONE
        vSettingItemBackground.setOnClickListener {
            checkPermission()
        }
    }

    override fun setNotPremiumMode() {
        vPlacePremiumInfo.visibility = View.VISIBLE
    }

    override fun setCurrentImage(currentImage: String) {
        vPlaceImageContainer.visibility = View.VISIBLE
        vSettingRemoveBackground.setOnClickListener { mSettingsPresenter.removeBackground() }
        Glide.with(context)
                .load(currentImage)
                .crossFade()
                .into(vImageBackgroundPreview)
    }

    override fun hideCurrentImage() {
        vPlaceImageContainer.visibility = View.GONE
    }

    private fun checkPermission() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), PERMISSION_REQUEST_STORAGE)
            return
        }
        val photoGaleryIntent = Intent(Intent.ACTION_PICK)
        photoGaleryIntent.type = "image/*"
        startActivityForResult(photoGaleryIntent, RESULT_LOAD_IMAGE_FROM_GALLERY)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode != Activity.RESULT_OK) return

        when (requestCode) {
            RESULT_LOAD_IMAGE_FROM_GALLERY -> if (data != null) mSettingsPresenter.setBackgroundImage(data.data)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_STORAGE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    checkPermission()
            }
        }
    }

    override fun getToolbar(activityContext: Context): Toolbar? {
        val toolbar = activityContext.inflateLayout(R.layout.toolbar_default, attachToRoot = false) as Toolbar
        toolbar.setTitle(R.string.fragment_settings_title)
        return toolbar
    }
}