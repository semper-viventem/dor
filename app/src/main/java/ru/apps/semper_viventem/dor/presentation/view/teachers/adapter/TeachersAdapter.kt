package ru.apps.semper_viventem.dor.presentation.view.teachers.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.chauthai.swipereveallayout.ViewBinderHelper
import com.pawegio.kandroid.inflateLayout
import kotlinx.android.synthetic.main.holder_teachers.view.*
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.presentation.models.TeacherModel

/**
 * @author Kulikov Konstantin
 * @since 10.05.2017.
 */
class TeachersAdapter(
        private val mOnSelectListener: OnSelectListener
) : RecyclerView.Adapter<TeachersAdapter.TeacherHolder>() {

    private val mViewBinderHolder = ViewBinderHelper()
    private var mTeacherList: List<TeacherModel> = emptyList()

    fun setData(teachers: List<TeacherModel>) {
        mTeacherList = teachers
        notifyDataSetChanged()
    }

    init {
        mViewBinderHolder.setOpenOnlyOne(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeacherHolder {
        return TeacherHolder(parent.context.inflateLayout(R.layout.holder_teachers))
    }

    override fun onBindViewHolder(holder: TeacherHolder?, position: Int) {
        holder?.bind(mTeacherList[position])
    }

    override fun getItemCount(): Int {
        return mTeacherList.size
    }

    inner class TeacherHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(teacherModel: TeacherModel) {
            mViewBinderHolder.bind(itemView.vSwipeLayout, teacherModel.id.toString())
            itemView.vTextTeacherName.text = teacherModel.getNameFormat()
            itemView.vTextTeacherInfo.text = teacherModel.info
            itemView.vLayoutBody.setOnClickListener { mOnSelectListener.onSelectedTeacher(teacherModel) }
            itemView.vLayoutRemove.setOnClickListener {
                mViewBinderHolder.closeLayout(teacherModel.id.toString())
                mOnSelectListener.onRemoveTeacher(teacherModel)
                (mTeacherList as MutableList).remove(teacherModel)
                notifyItemRemoved(position)
            }
        }
    }

    /**
     * Слушатель нажатий на элементы списка
     */
    interface OnSelectListener {

        /**
         * Слушатель для удаления преподавателя
         * @param teacherModel модель преподавателя
         */
        fun onRemoveTeacher(teacherModel: TeacherModel)

        /**
         * Слушатель выбора преподавателя
         * @param teacherModel мдель преподавателя
         */
        fun onSelectedTeacher(teacherModel: TeacherModel)
    }
}