package ru.apps.semper_viventem.dor.presentation.view.schedule

import com.arellomobile.mvp.MvpView
import ru.apps.semper_viventem.dor.presentation.models.TemplateModel

/**
 * @author Kulikov Konstantin
 * @since 08.07.2017.
 */
interface SelectTemplateView : MvpView {

    /**
     * Список шаблонов загружен
     * @param template список моделей шаблонов
     */
    fun onTemplateLoaded(template: List<TemplateModel>)
}