package ru.apps.semper_viventem.dor.presentation.view.schedule.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.pawegio.kandroid.inflateLayout
import kotlinx.android.synthetic.main.holder_subject_week.view.*
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.presentation.models.SubjectModel

/**
 * @author Kulikov Konstantin
 * @since 26.06.2017.
 */
class ScheduleSubjectsAdapter(
        private val mListener: OnSelectListener,
        private val mWeekDay: Int
) : RecyclerView.Adapter<ScheduleSubjectsAdapter.BaseSubjectHolder>() {

    companion object {
        const val TYPE_ADD = 0
        const val TYPE_DEFAULT = 1
    }

    private var mSubjectList: List<SubjectModel> = emptyList()
    private var isEditMode: Boolean = false

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            mSubjectList.lastIndex + 1 -> TYPE_ADD
            else -> TYPE_DEFAULT
        }
    }

    fun setData(data: List<SubjectModel>): ScheduleSubjectsAdapter {
        mSubjectList = data
        notifyDataSetChanged()
        return this
    }

    fun setEditMode(editMode: Boolean) {
        isEditMode = editMode
//        if (editMode)
//            notifyItemInserted(mSubjectList.lastIndex + 1)
//        else
//            notifyItemRemoved(mSubjectList.lastIndex + 1)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseSubjectHolder {
        return when (viewType) {
            TYPE_DEFAULT -> ScheduleSubjectsHolder(parent.context.inflateLayout(R.layout.holder_subject_week, attachToRoot = false))
            TYPE_ADD -> AddSubjectHolder(parent.context.inflateLayout(R.layout.holder_add_subject_to_schedule, attachToRoot = false))
            else -> throw Exception("unknown type: $viewType")
        }
    }

    override fun onBindViewHolder(holder: BaseSubjectHolder?, position: Int) {
        when (holder) {
            is ScheduleSubjectsHolder -> holder.bind(mSubjectList[position])
            is AddSubjectHolder -> holder.bind()
        }
    }

    override fun getItemCount(): Int {
        return if (isEditMode)
            mSubjectList.size + 1
        else
            mSubjectList.size
    }

    abstract class BaseSubjectHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    inner class AddSubjectHolder(itemView: View) : BaseSubjectHolder(itemView) {
        fun bind() {
            itemView.setOnClickListener { mListener.onAddNewSubjectSelected(mWeekDay) }
        }
    }

    inner class ScheduleSubjectsHolder(itemView: View) : BaseSubjectHolder(itemView) {
        fun bind(subject: SubjectModel) {

            itemView.vTextSubjectName.text = subject.name
            itemView.vTextSubjectClass.text = subject.getTimePeriod()
            itemView.vTextSubjectNote.text = subject.homeWork

            if (subject.imageQuantity > 0) {
                itemView.vPlaceImageCounter.visibility = View.VISIBLE
                itemView.vTextImageCounter.text = subject.imageQuantity.toString()
            } else {
                itemView.vPlaceImageCounter.visibility = View.GONE
            }

            itemView.setOnClickListener { mListener.onSelected(position, mWeekDay, subject) }
            itemView.setOnLongClickListener {
                mListener.onLongSelected(subject)
                false
            }
        }
    }

    interface OnSelectListener {

        fun onSelected(position: Int, weekDay: Int, subject: SubjectModel)

        fun onLongSelected(subject: SubjectModel)

        fun onAddNewSubjectSelected(weekDay: Int)
    }
}