package ru.apps.semper_viventem.dor.domain.iteractor

import io.adev.rxwrapper.RxAdapter
import io.reactivex.ObservableEmitter
import ru.apps.semper_viventem.dor.domain.model.Teacher
import ru.apps.semper_viventem.dor.domain.repository.TeacherRepository
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 10.05.2017.
 */
class GetTeacherById @Inject constructor(
        private val mTeacherRepository: TeacherRepository
) : RxAdapter<Teacher, Int>() {
    override fun execute(emitter: ObservableEmitter<Teacher>, criteria: Int?) {
        val result = mTeacherRepository.getTeacherById(criteria!!)
        emitter.onNext(result)
    }
}