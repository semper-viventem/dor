package ru.apps.semper_viventem.dor.domain.iteractor

import io.adev.rxwrapper.RxAdapter
import io.reactivex.ObservableEmitter
import ru.apps.semper_viventem.dor.domain.model.Call
import ru.apps.semper_viventem.dor.domain.repository.CallRepository
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 11.06.2017.
 */
class SaveLocalCall @Inject constructor(
        private val mCallRepository: CallRepository
) : RxAdapter<Void, Call>() {

    override fun execute(emitter: ObservableEmitter<Void>, criteria: Call?) {
        mCallRepository.addCall(criteria!!)
    }
}