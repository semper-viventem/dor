package ru.apps.semper_viventem.dor.data.entity

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/**
 * @author Kulikov Konstantin
 * @since 25.06.2017.
 */
@RealmClass
open class WeekEntity: RealmObject() {

    companion object {
        const val PERIOD = "period"
    }

    @PrimaryKey
    var period: String? = null

    var days: RealmList<WeekDayEntity>? = null
}