package ru.apps.semper_viventem.dor.presentation.view.schedule.dialog

import android.content.Context
import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import ru.apps.semper_viventem.dor.R

/**
 * @author Kulikov Konstantin
 * @since 12.09.2017.
 */
class WeekIsEmptyDIalog (
        mContext: Context,
        mPositiveListener: (dialog: DialogInterface, which: Int) -> Unit = { _, _ -> },
        mNegativeListener: (dialog: DialogInterface, which: Int) -> Unit = { _, _ -> }
) {

    private val dialog = AlertDialog.Builder(mContext)
            .setTitle(mContext.getString(R.string.dialog_week_empty_title))
            .setMessage(mContext.getString(R.string.dialog_week_empty_message))
            .setPositiveButton(mContext.getString(R.string.yes), mPositiveListener)
            .setNegativeButton(mContext.getString(R.string.no), mNegativeListener)
            .create()

    fun show() {
        dialog.show()
    }
}