package ru.apps.semper_viventem.dor.presentation.view.call.fragment

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.google.android.gms.analytics.HitBuilders
import com.google.android.gms.analytics.Tracker
import com.pawegio.kandroid.inflateLayout
import kotlinx.android.synthetic.main.fragment_call.*
import ru.apps.semper_viventem.dor.App
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.data.common.*
import ru.apps.semper_viventem.dor.presentation.common.factory.SpinnerAdapterFactory
import ru.apps.semper_viventem.dor.presentation.common.fragments.BaseFragment
import ru.apps.semper_viventem.dor.presentation.models.CallModel
import ru.apps.semper_viventem.dor.presentation.presenter.call.CallPresenter
import ru.apps.semper_viventem.dor.presentation.view.call.CallView
import ru.apps.semper_viventem.dor.presentation.view.call.adapter.CallAdapter
import ru.apps.semper_viventem.dor.presentation.view.call.dialog.AddCallDialog
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 02.05.2017.
 */
class CallFragment : BaseFragment(), CallView {

    companion object {
        fun newInstance(): CallFragment {
            val fragment = CallFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    @InjectPresenter
    lateinit var mCallPresenter: CallPresenter
    @Inject
    lateinit var mTracker: Tracker

    private lateinit var mAdapter: CallAdapter

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_call, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        App.component.inject(this)

        mTracker.setScreenName(this::class.java.simpleName)
        mTracker.send(HitBuilders.ScreenViewBuilder().build())


        vSpinnerWeekDay.adapter = SpinnerAdapterFactory(
                MAIN to R.string.main,
                MONDAY to R.string.monday,
                TUESDAY to R.string.tuesday,
                WEDNESDAY to R.string.wednesday,
                THURSDAY to R.string.thursday,
                FRIDAY to R.string.friday,
                SATURDAY to R.string.saturday,
                SUNDAY to R.string.sunday
        ).create(context)
        vSpinnerWeekDay.setSelection(MAIN)
        vSpinnerWeekDay.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                mCallPresenter.setSpinnerSelected(position)
            }
        }

        mAdapter = CallAdapter(object : CallAdapter.OnSelectListener {
            override fun onItemSelected(call: CallModel, position: Int) {
                mCallPresenter.callSelected(call, position)
            }

            override fun onRemoveItemSelected(call: CallModel) {
                mCallPresenter.removeCall(call)
            }

            override fun onAddCallSelected(position: Int) {
                mCallPresenter.addCallSelected(position)
            }
        })
        vRecyclerView.layoutManager = LinearLayoutManager(context)
        vRecyclerView.adapter = mAdapter

    }

    override fun getToolbar(activityContext: Context): Toolbar? {
        val toolbar = activityContext.inflateLayout(R.layout.toolbar_default, attachToRoot = false) as Toolbar
        toolbar.setTitle(R.string.fragment_call_title)
        return toolbar
    }

    override fun callsLoaded(calls: List<CallModel>) {
        mAdapter.setData(calls)
    }

    override fun showTimeSelectDialog(weekDay: Int, position: Int, callModelId: Int?) {
        AddCallDialog.newInstance(weekDay, position, callModelId)
                .setCancelListener(object : AddCallDialog.OnCanceledListener {
                    override fun onCanceled() {
                        mCallPresenter.loadCallList()
                    }
                })
                .show(fragmentManager, "")
    }
}