package ru.apps.semper_viventem.dor.presentation.view.schedule

import com.arellomobile.mvp.MvpView
import ru.apps.semper_viventem.dor.presentation.models.SubjectModel
import ru.apps.semper_viventem.dor.presentation.models.WeekModel

/**
 * @author Kulikov Konstantin
 * @since 02.05.2017.
 */
interface ScheduleView : MvpView {

    /**
     * Неделя успешно загружена
     *
     * @param weekModel модель недели
     */
    fun onWeekLoadedComplite(weekModel: WeekModel, selectPosition: Int? = null)

    /**
     * Событие открытия диалогового окна для добавления нового предмета в какой-то день
     * @param weekDay день недели (из констант)
     */
    fun showAddSubject(weekDay: Int)

    /**
     * Открыть диалог для сохранения шаблона
     * @param week неделя для ее шаблонизации
     */
    fun showDialogSaveTemplate(week: WeekModel)

    /**
     * Открыть диалог для выбора шаблона
     */
    fun showDialogSelectTemplate()

    /**
     * Событие для открытия окна для замены предмета в какой-то день
     * @param weekDay день недели (из констант)
     * @param oldSubjectPosition позиция старого предмета
     */
    fun showReplaceSubject(oldSubjectPosition: Int, weekDay: Int)

    /**
     * Расписание на данную неделю отсутствует
     */
    fun onWeekIsEmpty()

    /**
     * Включить режим редактирования
     */
    fun turnOnEditMode()

    /**
     * Выключить режим редактирования
     */
    fun turnOffEditMode()

    /**
     * Показать диалог детализации
     *
     * @param subject модель предмета
     * @param date дата
     * @param position позиция в списке
     */
    fun showSubjectDetails(subject: SubjectModel, date: String, position: Int)
}