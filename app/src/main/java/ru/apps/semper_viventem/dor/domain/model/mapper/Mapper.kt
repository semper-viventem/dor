package ru.apps.semper_viventem.dor.domain.model.mapper

/**
 * @author Konstantin Kulikov
 * @since 13.03.17
 */
interface Mapper<From, To> {

    fun map(from: From): To

    fun map(fromList: List<From>): List<To> {
        return fromList.map { from -> map(from) }
    }

}