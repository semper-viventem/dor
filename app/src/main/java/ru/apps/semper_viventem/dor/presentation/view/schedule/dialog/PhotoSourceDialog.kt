package ru.apps.semper_viventem.dor.presentation.view.schedule.dialog

import android.os.Bundle
import android.support.v7.app.AppCompatDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.dialog_photo_source.*
import ru.apps.semper_viventem.dor.R

/**
 * @author Kulikov Konstantin
 * @since 14.07.2017.
 */
class PhotoSourceDialog : AppCompatDialogFragment() {

    private var mOnCameraSelected: () -> Unit = {}
    private var mOnGalerySelected: () -> Unit = {}

    companion object {

        fun newInstance(cameraSelected: () -> Unit, galerySelected: () -> Unit ) : PhotoSourceDialog{
            val dialog = PhotoSourceDialog()
            val args = Bundle()

            dialog.mOnCameraSelected = cameraSelected
            dialog.mOnGalerySelected = galerySelected
            dialog.arguments = args

            return dialog
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_photo_source, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        vButtonCamera.setOnClickListener { mOnCameraSelected(); dismiss() }
        vButtonGalery.setOnClickListener { mOnGalerySelected(); dismiss() }
    }
}