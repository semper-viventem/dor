package ru.apps.semper_viventem.dor.data.common

import android.content.SharedPreferences
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Kulikov Konstantin
 * @since 19.06.2017.
 */

@Singleton
class UserData @Inject constructor(
        private val mSharedPreferences: SharedPreferences
) {

    private val mPrefEditor = mSharedPreferences.edit()

    companion object {
        private const val BACKGROUND_IMAGE = "background_image"
        private const val FIRST_SESSION = "first_session"
    }

    /** ссылка на бэкграунд [String] */
    fun getBackground(): String? =
            mSharedPreferences.getString(BACKGROUND_IMAGE, null)

    fun setBackground(value: String?) {
        mPrefEditor.putString(BACKGROUND_IMAGE, value).commit()
    }

    /** Первая ли это сессия */
    fun getFirstSession(): Boolean =
            mSharedPreferences.getBoolean(FIRST_SESSION, true)

    fun setFirstSession(value: Boolean) {
        mPrefEditor.putBoolean(FIRST_SESSION, value).commit()
    }
}