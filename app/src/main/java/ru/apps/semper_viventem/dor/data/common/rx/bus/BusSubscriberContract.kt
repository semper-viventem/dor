package ru.apps.semper_viventem.dor.data.common.rx.bus

import ru.apps.semper_viventem.dor.data.common.rx.bus.BusEvent
import ru.apps.semper_viventem.dor.data.common.rx.bus.RxBus
import kotlin.reflect.KClass

/**
 * @author Kulikov Konstantin
 * @since 06.05.17
 */
interface BusSubscriberContract<E : BusEvent> {

    fun bus(bus: RxBus<E>)
    fun <T : E> subscribe(clazz: KClass<T>, callback: (T) -> Unit)
    fun unsubscribe()

}