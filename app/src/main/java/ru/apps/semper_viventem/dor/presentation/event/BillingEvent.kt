package ru.apps.semper_viventem.dor.presentation.event

import ru.apps.semper_viventem.dor.data.common.rx.bus.ui.UiEvent

/**
 * @author Kulikov Konstantin
 * @since 13.08.2017.
 */
class BillingEvent : UiEvent