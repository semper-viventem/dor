package ru.apps.semper_viventem.dor.presentation.view.schedule.dialog

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatDialogFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.github.rubensousa.gravitysnaphelper.GravitySnapHelper
import kotlinx.android.synthetic.main.dialog_subject_details.*
import kotlinx.android.synthetic.main.layout_dialog_buttons.*
import kotlinx.android.synthetic.main.layout_header.*
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.presentation.common.*
import ru.apps.semper_viventem.dor.presentation.models.NoteModel
import ru.apps.semper_viventem.dor.presentation.presenter.schedule.SubjectDetailsPresenter
import ru.apps.semper_viventem.dor.presentation.view.schedule.SubjectDetailsView
import ru.apps.semper_viventem.dor.presentation.view.schedule.adapter.NotePhotoAdapter
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 13.07.2017.
 */
class SubjectDetailsDialog : MvpAppCompatDialogFragment(), SubjectDetailsView {

    @Inject
    lateinit var mAppContext: Context

    private var mOnSavedListener: () -> Unit = {}
    private var mOnCanceledListener: () -> Unit = {}

    companion object {

        private const val PARAMS = "params"
        private const val TITLE = "title"
        private const val PARENT_ID = "parent_id"

        fun newInstance(
                parentId: Int = -1,
                title: String = "",
                params: NoteModel.NoteParams,
                saveListener: () -> Unit = {},
                cancelListener: () -> Unit = {}
        ): SubjectDetailsDialog {

            val dialog = SubjectDetailsDialog()
            val args = Bundle()

            args.putInt(PARENT_ID, parentId)
            args.putSerializable(PARAMS, params)
            args.putString(TITLE, title)

            dialog.arguments = args
            dialog.mOnSavedListener = saveListener
            dialog.mOnCanceledListener = cancelListener

            return dialog
        }
    }

    @InjectPresenter
    lateinit var mPresenter: SubjectDetailsPresenter

    private var mLastPhotoUri: Uri? = null

    private lateinit var mPhotoAdapter: NotePhotoAdapter
    private val mSnapHelper = GravitySnapHelper(Gravity.START)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_subject_details, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {

        vEditTextHomeWork.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                mPresenter.onTextUpdate(vEditTextHomeWork.text.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
        vButtonPositive.setOnClickListener {
            mOnSavedListener()
            mPresenter.onSaveNote()
        }
        vButtonNegative.setOnClickListener {
            mOnCanceledListener()
            closeDialog()
        }

        val params = arguments.getSerializable(PARAMS) as NoteModel.NoteParams
        val parentId = arguments.getInt(PARENT_ID)
        val title = arguments.getString(TITLE, "")

        vTextHeadTitle.text = title


        mPhotoAdapter = NotePhotoAdapter(object : NotePhotoAdapter.OnSelectListener {
            override fun onRemoveSelectListener(uri: String, position: Int) {
                mPresenter.onRemovePhoto(uri)
            }

            override fun onSelectListener(uri: String, position: Int) {
                startActivity(Intent(Intent.ACTION_VIEW).apply {
                    setDataAndType(Uri.parse(uri), "image/*")
                })
            }

            override fun onAddPhotoSelected() {
                checkPermissions()
            }
        })

        vHorizontalRecyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        vHorizontalRecyclerView.adapter = mPhotoAdapter
        mSnapHelper.attachToRecyclerView(vHorizontalRecyclerView)

        mPresenter.loadNote(parentId, params)
    }

    override fun noteLoaded(note: NoteModel) {
        vEditTextHomeWork.setText(note.text)
        mPhotoAdapter.setData(note.photos)
    }

    private fun checkPermissions() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), PERMISSION_REQUEST_CAMERA)
            return
        }
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), PERMISSION_REQUEST_STORAGE)
            return
        }
        try {
            showSelectPhotoSourceDialog()
        } catch (error: Exception) {}
    }

    private fun showSelectPhotoSourceDialog() {
        PhotoSourceDialog.newInstance(
                cameraSelected = {
                    val photoCameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    mLastPhotoUri = generateFileUri(context)
                    photoCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, mLastPhotoUri)
                    photoCameraIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    startActivityForResult(photoCameraIntent, RESULT_LOAD_IMAGE_FROM_CAMERA)
                },
                galerySelected = {
                    val photoGaleryIntent = Intent(Intent.ACTION_PICK)
                    photoGaleryIntent.type = "image/*"
                    startActivityForResult(photoGaleryIntent, RESULT_LOAD_IMAGE_FROM_GALLERY)
                }
        ).show(fragmentManager, "")
    }

    override fun closeDialog() {
        dismiss()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode != Activity.RESULT_OK) return

        when (requestCode) {
            RESULT_LOAD_IMAGE_FROM_GALLERY -> if (data != null) mPresenter.onAddPhotoGalery(data.data)
            RESULT_LOAD_IMAGE_FROM_CAMERA -> if (mLastPhotoUri != null) mPresenter.onAddPhotoCamera(mLastPhotoUri!!)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CAMERA -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    checkPermissions()
            }
            PERMISSION_REQUEST_STORAGE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    checkPermissions()
            }
        }
    }
}