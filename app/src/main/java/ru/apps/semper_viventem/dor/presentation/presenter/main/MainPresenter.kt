package ru.apps.semper_viventem.dor.presentation.presenter.main

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.apps.semper_viventem.dor.App
import ru.apps.semper_viventem.dor.BuildConfig
import ru.apps.semper_viventem.dor.data.common.*
import ru.apps.semper_viventem.dor.data.common.rx.bus.ui.UiBus
import ru.apps.semper_viventem.dor.data.common.rx.bus.ui.UiBusSubscriber
import ru.apps.semper_viventem.dor.presentation.event.BillingEvent
import ru.apps.semper_viventem.dor.presentation.event.NotificationEvent
import ru.apps.semper_viventem.dor.presentation.event.ShowProgresEvent
import ru.apps.semper_viventem.dor.presentation.event.ShowProgresEvent.ProgresStatus.HIDE
import ru.apps.semper_viventem.dor.presentation.event.ShowProgresEvent.ProgresStatus.SHOW
import ru.apps.semper_viventem.dor.presentation.event.UpdateBackgroundEvent
import ru.apps.semper_viventem.dor.presentation.navigation.Screens
import ru.apps.semper_viventem.dor.presentation.view.main.MainView
import ru.terrakok.cicerone.Router
import timber.log.Timber
import util.IabHelper
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 02.05.2017.
 */
@InjectViewState
class MainPresenter(
        private var mActivity: AppCompatActivity
) : MvpPresenter<MainView>() {

    @Inject
    lateinit var mUiBus: UiBus
    @Inject
    lateinit var mContext: Context
    @Inject
    lateinit var mRouter: Router
    @Inject
    lateinit var mUserData: UserData
    @Inject
    lateinit var mBusSubscriber: UiBusSubscriber

    private var mIapHelper: IabHelper? = null

    private val mGotInventoryListener = IabHelper.QueryInventoryFinishedListener { result, inventory ->
        if (result.isFailure) {
            Timber.e("error with get billing")
        } else {
            val isPremium = inventory.hasPurchase(BILLING_PREMIUM)

            if (isPremium) {
                setPremium()
            } else {
                setBillingStatus(false)
            }
        }
    }

    //Google in-app billng
    private val mPurchaseFinishedListener = IabHelper.OnIabPurchaseFinishedListener { result, purchase ->
        if (result.isFailure) {
            Timber.e("error with billing")
        } else if (purchase.sku == BILLING_PREMIUM) {
            setPremium()
        }
    }

    init {
        App.component.inject(this)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        mBusSubscriber.subscribe(NotificationEvent::class, { event ->
            onNotificated(event)
        })
        mBusSubscriber.subscribe(BillingEvent::class, {
            onBillingRecived()
        })
        mBusSubscriber.subscribe(UpdateBackgroundEvent::class, {
            onLoadBackgroundImage()
        })
        mBusSubscriber.subscribe(ShowProgresEvent::class, { event ->
            onProgressBarRecived(event)
        })


        mIapHelper = IabHelper(mContext, BuildConfig.GOOGLE_BILLING_KEY)
        mIapHelper?.enableDebugLogging(BuildConfig.DEBUG)

        mIapHelper?.startSetup({ result ->
            if (result.isSuccess) {
                Timber.d("Horay, IAB is fully set up!")
                mIapHelper?.queryInventoryAsync(mGotInventoryListener)
            } else {
                Timber.d("Proglem setting up in-app Billing: $result")
            }
        })

        if (mUserData.getFirstSession()) mRouter.navigateTo(Screens.GUIDE)
    }

    fun draverNavigationSelected(screen: String) {
        when (screen) {
            Screens.PREMIUM -> mUiBus.post(BillingEvent())
            Screens.INFORMATION -> viewState.showInformationDialog()
            else -> mRouter.navigateTo(screen)
        }
    }

    private fun onNotificated(event: NotificationEvent) {
        viewState.onShowSnackBar(event.message, event.length, event.action)
    }

    private fun onBillingRecived() {
        if (mIapHelper == null) return

        mIapHelper!!.launchPurchaseFlow(
                mActivity,
                BILLING_PREMIUM,
                BILLING_PREMIUM_REQUEST,
                mPurchaseFinishedListener
        )
    }

    private fun loginUser() {
        //TODO загрузка данных о пользователе. Когда будет нормальный сервак
    }


    /**
     * Загрузить бэкграунд (доступно только в премиальной версии)
     */
    private fun onLoadBackgroundImage() {
        if (getPremiumStatus()) {
            val image = mUserData.getBackground()
            viewState.setBackground(image)
        }
    }

    private fun onProgressBarRecived(event: ShowProgresEvent) {
        when (event.status) {
            SHOW -> viewState.showProgresBar()
            HIDE -> viewState.hideProgresBar()
        }
    }

    fun handleActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
        return mIapHelper?.handleActivityResult(requestCode, resultCode, data) ?: false
    }

    override fun onDestroy() {
        mBusSubscriber.unsubscribe()
        if (mIapHelper != null) mIapHelper?.dispose()
        mIapHelper = null
        super.onDestroy()
    }

    private fun setPremium() {
        setBillingStatus(true)
        mUiBus.post(UpdateBackgroundEvent())
        viewState.hidePremiumMenuItem()
    }
}