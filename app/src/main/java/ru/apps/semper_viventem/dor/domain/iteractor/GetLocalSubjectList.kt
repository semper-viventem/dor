package ru.apps.semper_viventem.dor.domain.iteractor

import io.adev.rxwrapper.RxAdapter
import io.reactivex.ObservableEmitter
import ru.apps.semper_viventem.dor.domain.model.Subject
import ru.apps.semper_viventem.dor.domain.repository.SubjectRepository
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 08.05.2017.
 */
class GetLocalSubjectList @Inject constructor(
        private val mSubjectRepository: SubjectRepository
): RxAdapter<List<Subject>, Void>() {

    override fun execute(emitter: ObservableEmitter<List<Subject>>, criteria: Void?) {
        val result = mSubjectRepository.getSubjects()
        emitter.onNext(result)
    }
}