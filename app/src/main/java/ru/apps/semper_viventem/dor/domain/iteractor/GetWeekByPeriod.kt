package ru.apps.semper_viventem.dor.domain.iteractor

import io.adev.rxwrapper.RxAdapter
import io.reactivex.ObservableEmitter
import ru.apps.semper_viventem.dor.domain.exception.WeekIsNullException
import ru.apps.semper_viventem.dor.domain.model.Week
import ru.apps.semper_viventem.dor.domain.repository.WeekRepository
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 25.06.2017.
 */
class GetWeekByPeriod @Inject constructor(
        private val mWeekRepository: WeekRepository
) : RxAdapter<Week, String>() {
    override fun execute(emitter: ObservableEmitter<Week>, criteria: String?) {
        val result = mWeekRepository.getWeek(criteria!!)

        if (result == null) {
            emitter.onError(WeekIsNullException())
        } else {
            emitter.onNext(result)
        }
    }
}