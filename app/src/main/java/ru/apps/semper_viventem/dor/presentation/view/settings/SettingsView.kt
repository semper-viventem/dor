package ru.apps.semper_viventem.dor.presentation.view.settings

import com.arellomobile.mvp.MvpView

/**
 * @author Kulikov Konstantin
 * @since 13.08.2017.
 */
interface SettingsView : MvpView {

    fun setPremiumMode()

    fun setNotPremiumMode()

    fun setCurrentImage(currentImage: String)

    fun hideCurrentImage()
}