package ru.apps.semper_viventem.dor.presentation.common.comporator

import ru.apps.semper_viventem.dor.presentation.models.SubjectModel

/**
 * @author Kulikov Konstantin
 * @since 03.07.2017.
 */
class SubjectComparator : Comparator<SubjectModel> {
    override fun compare(o1: SubjectModel?, o2: SubjectModel?): Int {
        return (o1 ?: return 0).name.compareTo((o2 ?: return 0).name)
    }
}