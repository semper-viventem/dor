package ru.apps.semper_viventem.dor.presentation.presenter.settings

import android.net.Uri
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.apps.semper_viventem.dor.App
import ru.apps.semper_viventem.dor.data.common.UserData
import ru.apps.semper_viventem.dor.data.common.getPremiumStatus
import ru.apps.semper_viventem.dor.data.common.rx.bus.ui.UiBus
import ru.apps.semper_viventem.dor.presentation.event.UpdateBackgroundEvent
import ru.apps.semper_viventem.dor.presentation.view.settings.SettingsView
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 13.08.2017.
 */
@InjectViewState
class SettingsPresenter : MvpPresenter<SettingsView>() {

    @Inject
    lateinit var mUserData: UserData
    @Inject
    lateinit var mUiBus: UiBus

    init {
        App.component.inject(this)
    }

    fun loadSettingsData() {
        if (getPremiumStatus()) {
            viewState.setPremiumMode()
            val bg = mUserData.getBackground()
            if (bg != null) viewState.setCurrentImage(bg)
        } else {
            viewState.setNotPremiumMode()
        }
    }

    fun setBackgroundImage(image: Uri) {
        if (getPremiumStatus())  {
            mUserData.setBackground(image.toString())
            viewState.setCurrentImage(image.toString())
            mUiBus.post(UpdateBackgroundEvent())
        }
    }

    fun removeBackground() {
        mUserData.setBackground(null)
        viewState.hideCurrentImage()
        mUiBus.post(UpdateBackgroundEvent())
    }
}