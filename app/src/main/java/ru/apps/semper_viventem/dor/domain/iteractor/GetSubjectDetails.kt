package ru.apps.semper_viventem.dor.domain.iteractor

import io.adev.rxwrapper.RxAdapter
import io.reactivex.ObservableEmitter
import ru.apps.semper_viventem.dor.domain.model.Subject
import ru.apps.semper_viventem.dor.domain.repository.SubjectRepository
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 09.05.2017.
 */
class GetSubjectDetails @Inject constructor(
        private var mSubjectRepository: SubjectRepository
): RxAdapter<Subject, Int>() {

    override fun execute(emitter: ObservableEmitter<Subject>, criteria: Int?) {
        val result = mSubjectRepository.getSubjectById(criteria!!)
        emitter.onNext(result)
    }
}