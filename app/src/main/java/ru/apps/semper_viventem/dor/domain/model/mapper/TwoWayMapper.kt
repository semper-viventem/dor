package ru.apps.semper_viventem.dor.domain.model.mapper

import ru.apps.semper_viventem.dor.domain.model.mapper.Mapper
import ru.napoleonit.oas.domain.model.mapper.ReverseMapper

/**
 * @author Konstantin Kulikov
 * @since 13.03.17
 */
interface TwoWayMapper<From, To> : Mapper<From, To>, ReverseMapper<From, To>