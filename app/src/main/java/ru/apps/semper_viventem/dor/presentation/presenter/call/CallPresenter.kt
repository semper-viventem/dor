package ru.apps.semper_viventem.dor.presentation.presenter.call

import android.content.Context
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.adev.rxwrapper.util.observer
import ru.apps.semper_viventem.dor.App
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.data.common.MAIN
import ru.apps.semper_viventem.dor.data.common.rx.asyncUseCase
import ru.apps.semper_viventem.dor.data.common.rx.bus.ui.UiBus
import ru.apps.semper_viventem.dor.domain.iteractor.GetCallListByDay
import ru.apps.semper_viventem.dor.domain.iteractor.RemoveCall
import ru.apps.semper_viventem.dor.domain.iteractor.SaveLocalCall
import ru.apps.semper_viventem.dor.presentation.event.NotificationEvent
import ru.apps.semper_viventem.dor.presentation.event.ShowProgresEvent
import ru.apps.semper_viventem.dor.presentation.event.ShowProgresEvent.ProgresStatus
import ru.apps.semper_viventem.dor.presentation.event.SnackBarAction
import ru.apps.semper_viventem.dor.presentation.models.CallModel
import ru.apps.semper_viventem.dor.presentation.models.mapper.CallModelMapper
import ru.apps.semper_viventem.dor.presentation.view.call.CallView
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 02.05.2017.
 */
@InjectViewState
class CallPresenter: MvpPresenter<CallView>() {

    @Inject
    lateinit var mContext: Context
    @Inject
    lateinit var mGetCallListByDay: GetCallListByDay
    @Inject
    lateinit var mRemoveCallById: RemoveCall
    @Inject
    lateinit var mCallModelMapper: CallModelMapper
    @Inject
    lateinit var mAddCall: SaveLocalCall
    @Inject
    lateinit var mUiBus: UiBus

    init {
        App.component.inject(this)
    }

    private var mWeekDay: Int = MAIN
    private var mCallList: List<CallModel> = emptyList()

    fun loadCallList() {
        mUiBus.post(ShowProgresEvent(ProgresStatus.SHOW))
        asyncUseCase(mGetCallListByDay).execute(observer({ callList ->
            mCallList = mCallModelMapper.map(callList)
            viewState.callsLoaded(mCallList)
            mUiBus.post(ShowProgresEvent(ProgresStatus.HIDE))
        }, {
            mUiBus.post(NotificationEvent(mContext.getString(R.string.notification_error_with_cll_loaded)))
            mUiBus.post(ShowProgresEvent(ProgresStatus.HIDE))
        }), mWeekDay)
    }

    fun setSpinnerSelected(weekDay: Int) {
        mWeekDay = weekDay
        loadCallList()
    }

    /**
     * Нажата кнопка для добавления звонка
     */
    fun addCallSelected(position: Int) {
        viewState.showTimeSelectDialog(mWeekDay, position)
    }

    /**
     * Удалить тот или иной звонок
     * @param id идентификатор звонка
     */
    fun removeCall(call: CallModel) {
        asyncUseCase(mRemoveCallById).execute(observer({

        }, {
            (mCallList as MutableList).remove(call)
            mUiBus.post(NotificationEvent(mContext.getString(R.string.call_period_was_be_removed), action = SnackBarAction(mContext.getString(R.string.cancel), {
                addCall(call)
            })))
        }, {
            mUiBus.post(NotificationEvent(mContext.getString(R.string.notification_error_with_remove_call)))
        }), call.id)
    }

    /**
     * Выбран какой-то элемент звонка
     * @param call модеь данного элемента
     */
    fun callSelected(call: CallModel, position: Int) {
        viewState.showTimeSelectDialog(mWeekDay, position, call.id)
    }

    /**
     * Добавить звонок
     * @param call модель звонка
     */
    fun addCall(call: CallModel) {
        call.day = mWeekDay
        asyncUseCase(mAddCall).execute(observer({

        }, {
            loadCallList()
        }, {
            mUiBus.post(NotificationEvent(mContext.getString(R.string.notification_error_with_add_call)))
        }), mCallModelMapper.reverseMap(call))
    }
}