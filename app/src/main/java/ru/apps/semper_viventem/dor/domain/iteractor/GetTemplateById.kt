package ru.apps.semper_viventem.dor.domain.iteractor

import io.adev.rxwrapper.RxAdapter
import io.reactivex.ObservableEmitter
import ru.apps.semper_viventem.dor.domain.model.Template
import ru.apps.semper_viventem.dor.domain.repository.TemplatesRepository
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 08.07.2017.
 */
class GetTemplateById @Inject constructor(
        private val mTemplatesRepository: TemplatesRepository
) : RxAdapter<Template, Int>() {

    override fun execute(emitter: ObservableEmitter<Template>, criteria: Int?) {
        val result = mTemplatesRepository.getTemplateById(criteria!!)
        emitter.onNext(result)
    }
}