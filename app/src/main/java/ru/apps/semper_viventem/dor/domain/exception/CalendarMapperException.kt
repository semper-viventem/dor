package ru.apps.semper_viventem.dor.domain.exception

/**
 * @author Kulikov Konstantin
 * @since 25.06.2017.
 */
class CalendarMapperException (
        val weekFail: Int,
        val text: String
): Exception(text)