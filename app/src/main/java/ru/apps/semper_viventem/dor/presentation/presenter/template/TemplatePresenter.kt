package ru.apps.semper_viventem.dor.presentation.presenter.template

import android.content.Context
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.adev.rxwrapper.util.observer
import ru.apps.semper_viventem.dor.App
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.data.common.rx.asyncUseCase
import ru.apps.semper_viventem.dor.data.common.rx.bus.ui.UiBus
import ru.apps.semper_viventem.dor.domain.iteractor.GetAllTemplates
import ru.apps.semper_viventem.dor.domain.iteractor.RemoveTemplateById
import ru.apps.semper_viventem.dor.domain.iteractor.SaveLocalTemplate
import ru.apps.semper_viventem.dor.presentation.event.NotificationEvent
import ru.apps.semper_viventem.dor.presentation.event.ShowProgresEvent
import ru.apps.semper_viventem.dor.presentation.event.ShowProgresEvent.ProgresStatus.HIDE
import ru.apps.semper_viventem.dor.presentation.event.ShowProgresEvent.ProgresStatus.SHOW
import ru.apps.semper_viventem.dor.presentation.event.SnackBarAction
import ru.apps.semper_viventem.dor.presentation.models.TemplateModel
import ru.apps.semper_viventem.dor.presentation.models.mapper.TemplateModelMapper
import ru.apps.semper_viventem.dor.presentation.view.template.TemplateView
import timber.log.Timber
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 05.05.2017.
 */
@InjectViewState
class TemplatePresenter : MvpPresenter<TemplateView>() {

    @Inject
    lateinit var mContext: Context
    @Inject
    lateinit var mGetTemplateList: GetAllTemplates
    @Inject
    lateinit var mRemoveTemplate: RemoveTemplateById
    @Inject
    lateinit var mAddTemplate: SaveLocalTemplate
    @Inject
    lateinit var mUiBus: UiBus
    @Inject
    lateinit var mTemplateModelMapper: TemplateModelMapper

    init {
        App.component.inject(this)
    }

    private var mTemplateList: List<TemplateModel> = emptyList()

    fun loadTemplate() {
        mUiBus.post(ShowProgresEvent(SHOW))
        asyncUseCase(mGetTemplateList).execute(observer({ templates ->
            mTemplateList = mTemplateModelMapper.map(templates)
            viewState.onTemplateListLoaded(mTemplateList)
            mUiBus.post(ShowProgresEvent(HIDE))
        }, { error ->
            Timber.e(error)
            mUiBus.post(ShowProgresEvent(HIDE))
        }))
    }

    fun removeTemplate(templateModel: TemplateModel) {
        asyncUseCase(mRemoveTemplate).execute(observer({
            // onNext
        }, {
            (mTemplateList as MutableList).remove(templateModel)
            if (mTemplateList.isEmpty()) viewState.onTemplateListLoaded(mTemplateList)
            mUiBus.post(NotificationEvent(mContext.getString(R.string.template_was_be_removed), action = SnackBarAction(mContext.getString(R.string.cancel), {
                saveTemplate(templateModel)
            })))
        }, { error ->
            Timber.e(error)
        }), templateModel.id)
    }

    private fun saveTemplate(template: TemplateModel) {
        asyncUseCase(mAddTemplate).execute(observer({
            // onNext
        }, {
            loadTemplate()
        }, { error ->
            Timber.e(error)
        }), mTemplateModelMapper.reverseMap(template))
    }

    fun onTemplateSelected(template: TemplateModel) {
        //TODO
    }
}