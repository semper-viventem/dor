package ru.apps.semper_viventem.dor.domain.iteractor

import io.adev.rxwrapper.RxAdapter
import io.reactivex.ObservableEmitter
import ru.apps.semper_viventem.dor.domain.model.Template
import ru.apps.semper_viventem.dor.domain.repository.TemplatesRepository
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 08.07.2017.
 */
class GetAllTemplates @Inject constructor(
        private val mTemplatesRepository: TemplatesRepository
) : RxAdapter<List<Template>, Void>() {

    override fun execute(emitter: ObservableEmitter<List<Template>>, criteria: Void?) {
        val result = mTemplatesRepository.getTemplateList()
        emitter.onNext(result)
    }
}