package ru.apps.semper_viventem.dor.presentation.models.mapper

import ru.apps.semper_viventem.dor.domain.model.Week
import ru.apps.semper_viventem.dor.domain.model.mapper.TwoWayMapper
import ru.apps.semper_viventem.dor.presentation.models.WeekModel
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 06.05.2017.
 */
class WeekMapper @Inject constructor(
        private val mWeekDayMapper: WeekDayMapper
) : TwoWayMapper<Week?, WeekModel?> {

    override fun reverseMap(from: WeekModel?): Week {
        return Week(
                weekDays = mWeekDayMapper.reverseMap(from!!.weekDays),
                period = from.period)
    }

    override fun map(from: Week?): WeekModel? {
        if (from == null) return null
        return WeekModel(
                weekDays = mWeekDayMapper.map(from.weekDays),
                period = from.period
        )
    }
}