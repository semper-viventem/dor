package ru.apps.semper_viventem.dor.domain.iteractor

import io.adev.rxwrapper.RxAdapter
import io.reactivex.ObservableEmitter
import ru.apps.semper_viventem.dor.domain.model.Note
import ru.apps.semper_viventem.dor.domain.repository.NoteRepository
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 22.07.2017.
 */
class GetNoteById @Inject constructor(
        private val mNoteRepository: NoteRepository
) : RxAdapter<Note, Int>() {
    override fun execute(emitter: ObservableEmitter<Note>, criteria: Int?) {
        val result = mNoteRepository.getNoteById(criteria!!)
        emitter.onNext(result)
    }
}