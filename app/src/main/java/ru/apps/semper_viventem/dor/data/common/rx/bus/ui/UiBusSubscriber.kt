package ru.apps.semper_viventem.dor.data.common.rx.bus.ui

import io.reactivex.Scheduler
import ru.apps.semper_viventem.dor.data.common.rx.bus.BusSubscriber
import javax.inject.Inject
import kotlin.reflect.KClass

/**
 * @author Kulikov Konstantin
 * @since 06.05.17
 */
class UiBusSubscriber @Inject constructor(
        val bus: UiBus
) : BusSubscriber<UiEvent>() {

    init {
        bus(bus)
    }

    fun <T : UiEvent> subscribe(clazz: KClass<T>, observeScheduler: Scheduler, callback: (T) -> Unit) {
        subscribe(createSubscriptions(clazz).observeOn(observeScheduler), callback)
    }

}