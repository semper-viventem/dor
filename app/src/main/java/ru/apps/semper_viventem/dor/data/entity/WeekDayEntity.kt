package ru.apps.semper_viventem.dor.data.entity

import android.graphics.Color
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/**
 * @author Kulikov Konstantin
 * @since 25.06.2017.
 */

@RealmClass
open class WeekDayEntity : RealmObject() {

    @PrimaryKey
    var date: String = ""

    var subjects: RealmList<SubjectEntity>? = null

    var note: String = ""

    var color: Int = Color.WHITE

    var weekDay: Int = 0
}