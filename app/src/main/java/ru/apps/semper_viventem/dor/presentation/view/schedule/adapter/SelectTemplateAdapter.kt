package ru.apps.semper_viventem.dor.presentation.view.schedule.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.pawegio.kandroid.inflateLayout
import kotlinx.android.synthetic.main.holder_select_template.view.*
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.presentation.models.TemplateModel

/**
 * @author Kulikov Konstantin
 * @since 09.07.2017.
 */
class SelectTemplateAdapter(
        private val mListener: OnSelectListener
) : RecyclerView.Adapter<SelectTemplateAdapter.SelectTemplateHolder>() {

    private var mTemplateList: List<TemplateModel> = emptyList()

    fun setData(templates: List<TemplateModel>) {
        mTemplateList = templates
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelectTemplateHolder {
        return SelectTemplateHolder(parent.context.inflateLayout(R.layout.holder_select_template, attachToRoot = false))
    }

    override fun onBindViewHolder(holder: SelectTemplateHolder?, position: Int) {
        holder?.bind(mTemplateList[position])
    }

    override fun getItemCount(): Int {
        return mTemplateList.size
    }

    inner class SelectTemplateHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(template: TemplateModel) {
            itemView.vTextTemplateName.text = template.name
            itemView.setOnClickListener { mListener.onSelected(template) }

            if (template.isOnline) {
                itemView.vImageIsCloud.visibility = View.VISIBLE
            } else {
                itemView.vImageIsCloud.visibility = View.GONE
            }
        }
    }

    interface OnSelectListener {

        /**
         * Выбран шаблон расписания
         * @param template модель шаблона
         */
        fun onSelected(template: TemplateModel)
    }
}