package ru.apps.semper_viventem.dor.presentation.navigation.provider

import android.support.v4.app.Fragment


/**
 * @author Kulikov Konstantin
 * @since 02.05.2017.
 */
interface ScreensProvider {

    fun provideRootTabScreen(tabName: String, data: Any? = null): Fragment {
        val firstScreenName = provideFirstTabScreenName(tabName)
        val fragment = provideScreen(firstScreenName, data)
        return fragment
    }

    fun provideFirstTabScreenName(tabName: String): String

    fun provideScreen(screenName: String, data: Any? = null): Fragment
}