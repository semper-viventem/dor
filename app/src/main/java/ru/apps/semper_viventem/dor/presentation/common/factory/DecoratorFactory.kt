package ru.apps.semper_viventem.dor.presentation.common.factory

import android.content.Context
import android.support.v7.widget.RecyclerView
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration
import ru.apps.semper_viventem.dor.App
import ru.apps.semper_viventem.dor.R
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Kulikov Konstantin
 * @since 10.05.2017.
 */
@Singleton
class DecoratorFactory {

    @Inject
    lateinit var mContext: Context

    init {
        App.component.inject(this)
    }

    /**
     * Построить стандартный декоратор
     */
    fun getDefaultDecorator(): RecyclerView.ItemDecoration {
        return HorizontalDividerItemDecoration.Builder(mContext)
                .colorResId(R.color.gray_300)
                .visibilityProvider { position, parent ->
                    position >= parent.adapter.itemCount - 1
                }
                .build()
    }
}