package ru.apps.semper_viventem.dor.data.entity.mapper

import io.realm.RealmList
import io.realm.RealmModel
import ru.apps.semper_viventem.dor.domain.model.mapper.Mapper
import ru.apps.semper_viventem.dor.domain.model.mapper.TwoWayMapper
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 10.05.2017.
 */
class RealmListMapper<FromModel, ToModel: RealmModel> @Inject constructor(
        private val mModelMapper: TwoWayMapper<ToModel, FromModel>
) : Mapper<List<FromModel>, RealmList<ToModel>> {

    override fun map(from: List<FromModel>): RealmList<ToModel> {
        val list = RealmList<ToModel>()
        list.addAll(mModelMapper.reverseMap(from))
        return list
    }
}