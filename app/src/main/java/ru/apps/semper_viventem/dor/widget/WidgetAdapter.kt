package ru.apps.semper_viventem.dor.widget

import android.content.Context
import android.content.Intent
import android.widget.RemoteViews
import android.widget.RemoteViewsService
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.presentation.models.SubjectModel

/**
 * @author Kulikov Konstantin
 * @since 02.09.2017.
 */

class WidgetAdapter(
        private var context: Context,
        private var mIntent: Intent
) : RemoteViewsService.RemoteViewsFactory {

    private var mData: ArrayList<SubjectModel> = arrayListOf()

    override fun onCreate() {}

    override fun getCount(): Int = mData.size

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getLoadingView(): RemoteViews? = null

    override fun getViewAt(position: Int): RemoteViews {
        val rView = RemoteViews(context.packageName,
                R.layout.holder_widget)

//        rView.setTextViewText(R.id.vTextSubjectName, mData[position])
        rView.setTextViewText(R.id.vTextSubjectName, mData[position].name)
        rView.setTextViewText(R.id.vTextSubjectNote, mData[position].homeWork)
        rView.setTextViewText(R.id.vTextSubjectClass, mData[position].classRoom)

        //        Intent clickIntent = new Intent();
        //        clickIntent.putExtra(Widget.ITEM_POSITION, position);
        //        rView.setOnClickFillInIntent(R.id.WidgetListView, clickIntent);

        return rView
    }

    override fun getViewTypeCount(): Int = 1

    override fun hasStableIds(): Boolean = true

    override fun onDataSetChanged() {
        mData.clear()

        val postfix = mIntent.getIntExtra(SubjectModel.KEY_POSTFIX, -1)
        if (postfix == -1) return

        val items = mIntent.getStringArrayListExtra(SubjectModel.KEY_NAME+postfix)
        items.mapIndexedTo(mData) { index, it ->
            val name = it
            val note = mIntent.getStringArrayListExtra(SubjectModel.KEY_NOTE+postfix)[index]
            val room = mIntent.getStringArrayListExtra(SubjectModel.KEY_TIME+postfix)[index]
            SubjectModel(
                    name = name,
                    homeWork = note,
                    classRoom = room
            )
        }
    }

    override fun onDestroy() {}

}
