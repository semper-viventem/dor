package ru.apps.semper_viventem.dor.domain.iteractor

import io.adev.rxwrapper.RxAdapter
import io.reactivex.ObservableEmitter
import ru.apps.semper_viventem.dor.domain.model.Week
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 06.05.2017.
 */
class GetScheduleByDate @Inject constructor(): RxAdapter<Week, String>() {

    override fun execute(emitter: ObservableEmitter<Week>, criteria: String?) {
        val week = Week(criteria!!)

        emitter.onNext(week)
    }
}