package ru.apps.semper_viventem.dor.presentation.common

import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.data.common.*
import ru.apps.semper_viventem.dor.domain.exception.CalendarMapperException
import java.util.*

/**
 * @author Kulikov Konstantin
 * @since 25.06.2017.
 */
object WeekDayIdMapper {

    fun mapToCalendar(weekDay: Int): Int {
        return when (weekDay) {
            MONDAY -> Calendar.MONDAY
            TUESDAY -> Calendar.TUESDAY
            WEDNESDAY -> Calendar.WEDNESDAY
            THURSDAY -> Calendar.THURSDAY
            FRIDAY -> Calendar.FRIDAY
            SATURDAY -> Calendar.SATURDAY
            SUNDAY -> Calendar.SUNDAY
            else -> throw CalendarMapperException(weekDay, "Unknown week day: $weekDay")
        }
    }

    fun mapToTextId(weekDay: Int): Int {
        return when (weekDay) {
            MONDAY -> R.string.monday
            TUESDAY -> R.string.tuesday
            WEDNESDAY -> R.string.wednesday
            THURSDAY -> R.string.thursday
            FRIDAY -> R.string.friday
            SATURDAY -> R.string.saturday
            SUNDAY -> R.string.sunday
            else -> throw CalendarMapperException(weekDay, "Unknown week day: $weekDay")
        }
    }
}