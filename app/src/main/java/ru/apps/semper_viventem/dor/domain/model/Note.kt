package ru.apps.semper_viventem.dor.domain.model

/**
 * @author Kulikov Konstantin
 * @since 22.07.2017.
 */
class Note (
var id: Int = 0,
var text: String = "",
var params: NoteParams? = null,
var photos: ArrayList<String> = arrayListOf()
) {
    /**
     * Параметры идентификации заметки
     *
     * @property parentId идентификатор родителя
     * @property date дата к которой зкметка относится
     * @property position позиция этой заметки в расписании
     */
    class NoteParams (
            var parentId: Int = 0,
            var date: String = "",
            var position: Int = 0
    )
}