package ru.apps.semper_viventem.dor.domain.iteractor

import io.adev.rxwrapper.RxAdapter
import io.reactivex.ObservableEmitter
import ru.apps.semper_viventem.dor.domain.model.Week
import ru.apps.semper_viventem.dor.domain.repository.WeekRepository
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 25.06.2017.
 */
class GetWeeks @Inject constructor(
        private val mWeekRepository: WeekRepository
) : RxAdapter<List<Week>, Void>() {

    override fun execute(emitter: ObservableEmitter<List<Week>>, criteria: Void?) {
        val result = mWeekRepository.getAllWeek()
        emitter.onNext(result)
    }
}