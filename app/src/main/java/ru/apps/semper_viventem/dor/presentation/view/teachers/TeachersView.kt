package ru.apps.semper_viventem.dor.presentation.view.teachers

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.apps.semper_viventem.dor.presentation.models.TeacherModel

/**
 * @author Kulikov Konstantin
 * @since 05.05.2017.
 */
interface TeachersView : MvpView{

    /**
     * Список предметов успешно загружен
     *
     * @param subjects список моделей преподавателей
     */
    fun onTeachersLoaded(subjects: List<TeacherModel>)

    /**
     * Показать диалог добавления товара
     */
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showAddTeacherDialog()

    /**
     * Открыть выбранного преподавателя
     *
     * @param teacherId идентификатор преподавателя
     */
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showSelectedTeacherDialog(teacherId: Int)
}