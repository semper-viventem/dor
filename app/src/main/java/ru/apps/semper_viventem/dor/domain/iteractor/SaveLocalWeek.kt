package ru.apps.semper_viventem.dor.domain.iteractor

import io.adev.rxwrapper.RxAdapter
import io.reactivex.ObservableEmitter
import ru.apps.semper_viventem.dor.domain.model.Week
import ru.apps.semper_viventem.dor.domain.repository.WeekRepository
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 25.06.2017.
 */
class SaveLocalWeek @Inject constructor(
        private val mWeekRepository: WeekRepository
) : RxAdapter<Void, Week>() {

    override fun execute(emitter: ObservableEmitter<Void>, criteria: Week?) {
        mWeekRepository.addNewDay(criteria!!)
    }
}