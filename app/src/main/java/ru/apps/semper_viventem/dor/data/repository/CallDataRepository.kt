package ru.apps.semper_viventem.dor.data.repository

import io.realm.Realm
import ru.apps.semper_viventem.dor.data.entity.CallEntity
import ru.apps.semper_viventem.dor.data.entity.mapper.CallEntityMapper
import ru.apps.semper_viventem.dor.domain.model.Call
import ru.apps.semper_viventem.dor.domain.repository.CallRepository
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Kulikov Konstantin
 * @since 11.06.2017.
 */
@Singleton
class CallDataRepository @Inject constructor(
        private val mCallEntityMapper: CallEntityMapper
): CallRepository {

    override fun addCall(call: Call) {
        Realm.getDefaultInstance().use { realm ->
            val item = mCallEntityMapper.reverseMap(call)
            if (item.id == null || item.id == 0) {
                item.id = getNextKey(realm)
            }
            realm.executeTransaction { realm ->
                realm.copyToRealmOrUpdate(item)
            }
        }
    }

    override fun getCallByDay(day: Int): List<Call> {
        return Realm.getDefaultInstance().use { realm ->
            val result = realm.where(CallEntity::class.java)
                    .findAll()
                    .filter { it.day == day }

            mCallEntityMapper.map(result)
        }
    }

    override fun getAllCalls(): List<Call> {
        return Realm.getDefaultInstance().use { realm ->
            val result = realm.where(CallEntity::class.java)
                    .findAll()

            mCallEntityMapper.map(result)
        }
    }

    override fun getCallById(id: Int): Call {
        return Realm.getDefaultInstance().use { realm ->
            val result = realm.where(CallEntity::class.java)
                    .equalTo(CallEntity.ID, id)
                    .findFirst()

            mCallEntityMapper.map(result)
        }
    }

    override fun removeCall(id: Int) {
        Realm.getDefaultInstance().use { realm ->
            realm.executeTransaction { realm ->
                val result = realm.where(CallEntity::class.java)
                        .equalTo(CallEntity.ID, id)
                        .findAll()
                result.deleteAllFromRealm()
            }
        }
    }

    fun getNextKey(realm: Realm): Int {
        val maxKey = realm.where(CallEntity::class.java).max(CallEntity.ID)
        return if (maxKey != null) maxKey.toInt() + 1 else 1
    }
}