package ru.apps.semper_viventem.dor.presentation.models

/**
 * @author Kulikov Konstantin
 * @since 31.08.2017.
 */
class GuideModel(
        val title: String,
        val description: String,
        val image: Int
)