package ru.apps.semper_viventem.dor.presentation.view.call.dialog

import android.app.TimePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.arellomobile.mvp.MvpAppCompatDialogFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.dialog_add_call.*
import kotlinx.android.synthetic.main.layout_dialog_buttons.*
import kotlinx.android.synthetic.main.layout_header.*
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.presentation.models.CallModel
import ru.apps.semper_viventem.dor.presentation.presenter.call.AddCallDialogPresenter
import ru.apps.semper_viventem.dor.presentation.view.call.AddCallDialogView


/**
 * @author Kulikov Konstantin
 * @since 18.06.2017.
 */
class AddCallDialog: MvpAppCompatDialogFragment(), AddCallDialogView {

    companion object {
        const val CURRENT_CALL = "current_call"
        const val LESSEN_NUMBER = "lessen_number"
        const val CURRENT_DAY = "current_day"

        fun newInstance(currentDay: Int, lessenNumber: Int, currentCallId: Int? = null): AddCallDialog {
            val fragment = AddCallDialog()

            val args = Bundle()
            args.putInt(LESSEN_NUMBER, lessenNumber)
            args.putInt(CURRENT_DAY, currentDay)
            if (currentCallId != null)
                args.putInt(CURRENT_CALL, currentCallId)
            fragment.arguments = args

            return fragment
        }
    }

    @InjectPresenter
    lateinit var mPresenter: AddCallDialogPresenter

    private var mCloseListener: OnCanceledListener? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.dialog_add_call, container)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val currentCallId = arguments.getInt(CURRENT_CALL)
        val lessenNumber = arguments.getInt(LESSEN_NUMBER)
        val currentDay = arguments.getInt(CURRENT_DAY)

        vTextHeadTitle.text = getString(R.string.dialog_add_call_title, lessenNumber)
        vButtonPositive.setOnClickListener {
            mPresenter.setBegin(vEditTextBegin.text.toString())
            mPresenter.setEnd(vEditTextEnd.text.toString())
            mPresenter.saveCall()
        }
        vButtonNegative.setOnClickListener { closeDialog() }

        setEditTextTimeListener(vEditTextBegin)
        setEditTextTimeListener(vEditTextEnd)

        mPresenter.loadCurrentCalls(currentDay, if (currentCallId != 0) currentCallId else null)
    }

    override fun closeDialog() {
        dismiss()
        if (mCloseListener != null) mCloseListener?.onCanceled()
    }

    override fun callLoaded(callModel: CallModel) {
        vEditTextBegin.setText(callModel.begin)
        vEditTextEnd.setText(callModel.end)

        val beginInt = callModel.begin.split(":").mapNotNull(String::toInt)
        setEditTextTimeListener(vEditTextBegin, beginInt[0], beginInt[1])

        val endInt = callModel.end.split(":").mapNotNull(String::toInt)
        setEditTextTimeListener(vEditTextEnd, endInt[0], endInt[1])
    }

    /**
     * Добавить слушатель закрытия диалога
     * @param listener реализация слушателя
     */
    fun setCancelListener(listener: OnCanceledListener): AddCallDialog {
        mCloseListener = listener
        return this
    }

    /**
     * Задать открытие диалога ввода даты по нажатию на [EditText]
     */
    private fun setEditTextTimeListener(eText: EditText, preHour: Int = 0, preMinute: Int = 0) {
        eText.setOnClickListener {
            TimePickerDialog(context, TimePickerDialog.OnTimeSetListener { _, hourOfDay, minuteOfDay ->
                val hour: String
                val minute: String
                if (Math.floor((hourOfDay / 10).toDouble()).toInt() > 0) {
                    hour = hourOfDay.toString()
                } else {
                    hour = "0" + Integer.toString(hourOfDay)
                }

                if (Math.floor((minuteOfDay / 10).toDouble()).toInt() > 0) {
                    minute = minuteOfDay.toString()
                } else {
                    minute = "0" + Integer.toString(minuteOfDay)
                }

                eText.setText("$hour:$minute")
            }, preHour, preMinute, true).show()
        }
    }

    /**
     * Слушатель закрытия диалога
     */
    interface OnCanceledListener {

        /**
         * Закрыли
         */
        fun onCanceled()
    }
}