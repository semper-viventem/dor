package ru.apps.semper_viventem.dor.domain.iteractor

import io.adev.rxwrapper.RxAdapter
import io.reactivex.ObservableEmitter
import ru.apps.semper_viventem.dor.domain.repository.TemplatesRepository
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 08.07.2017.
 */
class RemoveTemplateById @Inject constructor(
        private val mTemplatesRepository: TemplatesRepository
) : RxAdapter<Void, Int>() {
    override fun execute(emitter: ObservableEmitter<Void>, criteria: Int?) {
        mTemplatesRepository.removeTemplate(criteria!!)
    }
}