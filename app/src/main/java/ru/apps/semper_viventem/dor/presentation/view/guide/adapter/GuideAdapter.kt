package ru.apps.semper_viventem.dor.presentation.view.guide.adapter

import android.support.v4.view.PagerAdapter
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.pawegio.kandroid.inflateLayout
import kotlinx.android.synthetic.main.page_guide.view.*
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.presentation.models.GuideModel

/**
 * @author Kulikov Konstantin
 * @since 01.09.2017.
 */
class GuideAdapter : PagerAdapter() {

    private var mPages: List<GuideModel> = emptyList()

    fun setData(pages: List<GuideModel>) {
        mPages = pages
        notifyDataSetChanged()
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val page = mPages[position]
        val itemView = container.context.inflateLayout(R.layout.page_guide)

        itemView.vTextGuideTitle.text = page.title
        itemView.vTextGuideDescription.text = page.description

        Glide.with(itemView.context)
                .load(page.image)
                .crossFade()
                .into(itemView.vImageViewGuide)

        container.addView(itemView)

        return itemView
    }

    override fun isViewFromObject(view: View?, obj: Any?): Boolean = view == obj

    override fun getCount(): Int = mPages.size
}