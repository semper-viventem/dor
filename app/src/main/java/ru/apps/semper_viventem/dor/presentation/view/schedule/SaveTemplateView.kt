package ru.apps.semper_viventem.dor.presentation.view.schedule

import com.arellomobile.mvp.MvpView

/**
 * @author Kulikov Konstantin
 * @since 08.07.2017.
 */
interface SaveTemplateView: MvpView {

    fun templateIsSaved(name: String)
}