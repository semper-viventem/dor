package ru.apps.semper_viventem.dor.data.entity

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/**
 * @author Kulikov Konstantin
 * @since 09.05.2017.
 */
@RealmClass
open class TeacherEntity: RealmObject() {

    companion object {
        const val ID = "id"
    }

    @PrimaryKey
    open var id: Int? = null

    open var firstName: String = ""

    open var lastName: String = ""

    open var patronymic: String = ""

    open var phone: String = ""

    open var email: String = ""

    open var address: String = ""

    open var info: String = ""
}