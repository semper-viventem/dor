package ru.apps.semper_viventem.dor.data.common.rx.bus

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

/**
 * @author Kulikov Konstantin
 * @since 06.05.17
 */
open class RxBus<E : BusEvent> {

    private val mBus = PublishSubject.create<E>().toSerialized()

    fun post(event: E) {
        mBus.onNext(event)
    }

    fun <T : E> events(type: Class<T>): Observable<T> {
        return events().ofType(type)
    }

    fun events(): Observable<E> {
        return mBus
    }

}