package ru.apps.semper_viventem.dor.presentation.view.main

import com.arellomobile.mvp.MvpView
import ru.apps.semper_viventem.dor.presentation.event.SnackBarAction

/**
 * @author Kulikov Konstantin
 * @since 02.05.2017.
 */
interface MainView : MvpView{

    /**
     * Показать SnackИar
     */
    fun onShowSnackBar(message: String, length: Int, action: SnackBarAction?)

    /**
     * Скрыть кнопку покупки
     */
    fun hidePremiumMenuItem()

    /**
     * Нарисовать бэкграунд
     * @param image URI изображения
     */
    fun setBackground(image: String?)

    /**
     * Показать прогрес
     */
    fun showProgresBar()

    /**
     * Скрыть прогрес
     */
    fun hideProgresBar()

    /**
     * Показать диалог информации
     */
    fun showInformationDialog()
}