package ru.apps.semper_viventem.dor.presentation.view.schedule.fragment

import android.content.Context
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.PagerSnapHelper
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.google.android.gms.analytics.HitBuilders
import com.google.android.gms.analytics.Tracker
import com.pawegio.kandroid.inflateLayout
import kotlinx.android.synthetic.main.fragment_schedule.*
import ru.apps.semper_viventem.dor.App
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.presentation.common.MODE_TRANSFORM_DURATION
import ru.apps.semper_viventem.dor.presentation.common.fragments.BaseFragment
import ru.apps.semper_viventem.dor.presentation.common.fragments.FabFragmentMarker
import ru.apps.semper_viventem.dor.presentation.models.NoteModel
import ru.apps.semper_viventem.dor.presentation.models.SubjectModel
import ru.apps.semper_viventem.dor.presentation.models.WeekModel
import ru.apps.semper_viventem.dor.presentation.presenter.schedule.SchedulePresenter
import ru.apps.semper_viventem.dor.presentation.view.schedule.ScheduleView
import ru.apps.semper_viventem.dor.presentation.view.schedule.adapter.MainScheduleAdapter
import ru.apps.semper_viventem.dor.presentation.view.schedule.dialog.*
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 02.05.2017.
 */
class ScheduleFragment : BaseFragment(), FabFragmentMarker, ScheduleView {

    companion object {
        fun newInstance(): ScheduleFragment {
            val fragment = ScheduleFragment()
            val args = Bundle()

            fragment.arguments = args
            return fragment
        }
    }

    @InjectPresenter
    lateinit var mSchedulePresenter: SchedulePresenter

    @Inject
    lateinit var mTracker: Tracker

    private var mFab: FloatingActionButton? = null
    private lateinit var mScheduleAdapter: MainScheduleAdapter
    private val mSnapHelper = PagerSnapHelper()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater?.inflate(R.layout.fragment_schedule, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        App.component.inject(this)

        mTracker.setScreenName(this::class.java.simpleName)
        mTracker.send(HitBuilders.ScreenViewBuilder().build())

        mFab?.setImageDrawable(resources.getDrawable(R.drawable.ic_edit_24dp))
        mFab?.setOnClickListener {
            mSchedulePresenter.switchEditMode()
        }

        mScheduleAdapter = MainScheduleAdapter(object : MainScheduleAdapter.OnListener {
            override fun onSubjectSelected(position: Int, weekDay: Int, subjectModel: SubjectModel) {
                mSchedulePresenter.onSubjectSelected(position, weekDay, subjectModel)
            }

            override fun onSubjectLongSelected(subjectModel: SubjectModel) {
                mSchedulePresenter.onSubjectLongSelected(subjectModel)
            }

            override fun onSubjectAddSelected(weekDay: Int) {
                mSchedulePresenter.onSubjectAddSelected(weekDay)
            }

        })

        vHorizontalRecyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        vHorizontalRecyclerView.adapter = mScheduleAdapter
        mSnapHelper.attachToRecyclerView(vHorizontalRecyclerView)

        mSchedulePresenter.loadSchedule(true)
    }

    override fun getToolbar(activityContext: Context): Toolbar? {
        val toolbar = activityContext.inflateLayout(R.layout.toolbar_default, attachToRoot = false) as Toolbar
        toolbar.inflateMenu(R.menu.main)
        toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.toolbar_now -> mSchedulePresenter.nowOffset()
                R.id.toolbar_next -> mSchedulePresenter.nextOffset()
                R.id.toolbar_back -> mSchedulePresenter.previousOffset()
                R.id.toolbar_save_template -> mSchedulePresenter.onSaveTemplateSelected()
                R.id.toolbar_copy_template -> mSchedulePresenter.onSelectTemplateSelected()
            }
            true
        }
        toolbar.setTitle(R.string.app_name)
        return toolbar
    }

    override fun fabProvided(fab: FloatingActionButton) {
        mFab = fab
    }

    override fun onWeekLoadedComplite(weekModel: WeekModel, selectPosition: Int?) {
        mScheduleAdapter.setData(weekModel.weekDays)

        if (selectPosition != null) {
            vHorizontalRecyclerView.smoothScrollToPosition(selectPosition)
        }
    }

    override fun showAddSubject(weekDay: Int) {
        SelectSubjectDialog.newInstance(
                showRemoveSubjectButton = false,
                selectListener = { subject ->
                    mSchedulePresenter.onAddSubject(weekDay, subject)
                },
                subjectsIsEmptyListener = {
                    mSchedulePresenter.onSwitchToAddSubjectMenu()
                }
        ).show(fragmentManager, "")
    }

    override fun showReplaceSubject(oldSubjectPosition: Int, weekDay: Int) {
        SelectSubjectDialog.newInstance(
                showRemoveSubjectButton = true,
                selectListener = { subject ->
                    mSchedulePresenter.onReplaceSubject(weekDay, oldSubjectPosition, subject)
                },
                subjectsIsEmptyListener = {
                    mSchedulePresenter.onSwitchToAddSubjectMenu()
                },
                removeListener = {
                    mSchedulePresenter.onRemoveSubject(weekDay, oldSubjectPosition)
                }
        ).show(fragmentManager, "")
    }

    override fun showDialogSaveTemplate(week: WeekModel) {
        SaveTemplateDialog.newInstance(
                week = week,
                listener = { name ->
                    mSchedulePresenter.onTemplateIsSaved(name)
                }
        ).show(fragmentManager, "")
    }

    override fun showDialogSelectTemplate() {
        SelectTemplateDialog.newInstance { template ->
            mSchedulePresenter.onTemplateSelected(template)
        }.show(fragmentManager, "")
    }

    override fun onWeekIsEmpty() {
        WeekIsEmptyDIalog(
                mContext = context,
                mPositiveListener = { _, _ ->
                    mSchedulePresenter.onSelectTemplateSelected()
                },
                mNegativeListener = { _, _ ->
                    // ничего
                }).show()
    }

    override fun turnOnEditMode() {
        //TODO включить режим редактирования
        mScheduleAdapter.turnOnEditMode()
        vBackgroundOval.animate().setDuration(MODE_TRANSFORM_DURATION).scaleX(10000.0f).scaleY(10000.0f).start()
        mFab?.animate()?.setDuration(MODE_TRANSFORM_DURATION)?.rotation(360f)?.start()
    }

    override fun turnOffEditMode() {
        //TODO выключить режим редактирования
        mScheduleAdapter.turnOffEditMode()
        vBackgroundOval.animate().setDuration(MODE_TRANSFORM_DURATION).scaleX(1.0f).scaleY(1.0f).start()
        mFab?.animate()?.setDuration(MODE_TRANSFORM_DURATION)?.rotation(-360f)?.start()
    }

    override fun showSubjectDetails(subject: SubjectModel, date: String, position: Int) {
        SubjectDetailsDialog.newInstance(
                title = subject.name,
                params = NoteModel.NoteParams(
                        parentId = subject.id,
                        date = date,
                        position = position
                ),
                saveListener = {
                    mSchedulePresenter.loadSchedule()
                })
                .show(fragmentManager, "")
    }
}