package ru.apps.semper_viventem.dor.presentation.models.mapper

import ru.apps.semper_viventem.dor.domain.model.Teacher
import ru.apps.semper_viventem.dor.domain.model.mapper.TwoWayMapper
import ru.apps.semper_viventem.dor.presentation.models.TeacherModel
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 08.05.2017.
 */
class TeacherMapper @Inject constructor(): TwoWayMapper<Teacher, TeacherModel> {
    override fun reverseMap(from: TeacherModel): Teacher {
        return Teacher(
                from.id,
                from.firstName,
                from.lastName,
                from.patronymic,
                from.phone,
                from.email,
                from.address,
                from.info
        )
    }

    override fun map(from: Teacher): TeacherModel {
        return TeacherModel(
                from.id,
                from.firstName,
                from.lastName,
                from.patronymic,
                from.phone,
                from.email,
                from.address,
                from.info
        )
    }
}