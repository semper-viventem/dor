package ru.apps.semper_viventem.dor.data.common.rx.bus.ui

import ru.apps.semper_viventem.dor.data.common.rx.bus.BusEvent

/**
 * @author Kulikov Konstantin
 * @since 06.05.17
 */
interface UiEvent : BusEvent