package ru.apps.semper_viventem.dor.presentation.view.subjects.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.pawegio.kandroid.inflateLayout
import kotlinx.android.synthetic.main.holder_add_subject_teachers.view.*
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.presentation.models.TeacherModel

/**
 * @author Kulikov Konstantin
 * @since 08.05.2017.
 */
class SubjectTeachersAdapter(
        private val mListener: OnSelectListener
): RecyclerView.Adapter<SubjectTeachersAdapter.SubjectTeacherHolder>() {

    private var mTeachers: List<TeacherModel> = emptyList()

    fun setData(data: List<TeacherModel>) {
        mTeachers = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubjectTeachersAdapter.SubjectTeacherHolder {
        return SubjectTeacherHolder(parent.context.inflateLayout(R.layout.holder_add_subject_teachers, attachToRoot = false))
    }

    override fun onBindViewHolder(holder: SubjectTeachersAdapter.SubjectTeacherHolder?, position: Int) {
        holder?.bind(mTeachers[position])
    }

    override fun getItemCount(): Int {
        return mTeachers.size
    }

    inner class SubjectTeacherHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind (teacher: TeacherModel) {
            itemView.setOnClickListener {  }
            itemView.vCheckBox.isChecked = teacher.isChecked
            itemView.setOnClickListener { itemView.vCheckBox.isChecked = !itemView.vCheckBox.isChecked }
            itemView.vCheckBox.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    mListener.onSelectedTeacher(position, teacher)
                } else {
                    mListener.onUnselectedTeacher(position, teacher)
                }
            }
            itemView.vTextTeacherName.text = teacher.getNameFormat()
        }
    }


    /**
     * Слушатель нажатий на список
     */
    interface OnSelectListener {

        /**
         * Учитель выбран
         */
        fun onSelectedTeacher(index: Int, teacher: TeacherModel)

        /**
         * Учитель не выбран
         */
        fun onUnselectedTeacher(index: Int, teacher: TeacherModel)
    }
}