package ru.apps.semper_viventem.dor.data.repository

import io.realm.Realm
import ru.apps.semper_viventem.dor.data.entity.TeacherEntity
import ru.apps.semper_viventem.dor.data.entity.mapper.TeacherEntityMapper
import ru.apps.semper_viventem.dor.domain.model.Teacher
import ru.apps.semper_viventem.dor.domain.repository.TeacherRepository
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Kulikov Konstantin
 * @since 10.05.2017.
 */
@Singleton
class TeacherDataRepository @Inject constructor(
        private val mTeacherMapper: TeacherEntityMapper
): TeacherRepository {

    override fun getTeachers(): List<Teacher> {
        return Realm.getDefaultInstance().use { realm ->
            val result = realm.where(TeacherEntity::class.java)
                    .findAll()
            mTeacherMapper.map(result)
        }
    }

    override fun addTeacher(teacher: Teacher) {
        val item = mTeacherMapper.reverseMap(teacher)
        Realm.getDefaultInstance().use { realm ->
            if (item.id == null || item.id == 0) {
                item.id = getNextKey(realm)
            }
            realm.executeTransaction { realm ->
                realm.copyToRealmOrUpdate(item)
            }
        }
    }

    override fun getTeacherById(teacherId: Int): Teacher {
        Realm.getDefaultInstance().use { realm ->
            val result = realm.where(TeacherEntity::class.java)
                    .equalTo(TeacherEntity.ID, teacherId)
                    .findFirst()
            if (result == null) {
                throw IllegalStateException("teacher item not found with id='$teacherId'")
            } else {
                return mTeacherMapper.map(result)
            }
        }
    }

    override fun removeTeacherById(teacherId: Int) {
        Realm.getDefaultInstance().use { realm ->
            realm.executeTransaction { realm ->
                val result = realm.where(TeacherEntity::class.java)
                        .equalTo(TeacherEntity.ID, teacherId)
                        .findAll()
                result.deleteAllFromRealm()
            }
        }
    }

    private fun getNextKey(realm: Realm): Int {
        val maxKey = realm.where(TeacherEntity::class.java).max(TeacherEntity.ID)
        return if (maxKey != null) maxKey.toInt() + 1 else 1
    }
}