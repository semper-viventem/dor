package ru.apps.semper_viventem.dor.presentation.common.fragments

import android.support.design.widget.FloatingActionButton

/**
 * @author Kulikov Konstantin
 * @since 06.05.2017.
 */
interface FabFragmentMarker {

    /**
     * Передаем fab во фрагмент.
     * Только при условии, что fab будет показан
     */
    fun fabProvided(fab: FloatingActionButton)
}