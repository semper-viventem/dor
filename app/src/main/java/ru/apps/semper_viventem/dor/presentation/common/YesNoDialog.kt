package ru.apps.semper_viventem.dor.presentation.common

import android.content.Context
import android.support.v7.app.AlertDialog
import ru.apps.semper_viventem.dor.R

/**
 * @author Kulikov Konstantin
 * @since 14.07.2017.
 */
class YesNoDialog(val mContext: Context) {

    private var mPositiveListener: () -> Unit = {}
    private var mNegativeListener: () -> Unit = {}
    private var mTitle: String = ""
    private var mBody: String = ""

    /**
     * Слушатель кнопки "Да"
     *
     * @param listener лямбда слушателя
     */
    fun setPositiveListener(listener: () -> Unit): YesNoDialog {
        mPositiveListener = listener
        return this
    }

    /**
     * Слушатель кнопки "Нет"
     *
     * @param listener лямбда слушателя
     */
    fun setNegativeListener(listener: () -> Unit): YesNoDialog {
        mNegativeListener = listener
        return this
    }

    /**
     * Задать заголовок
     *
     * @param text текст заголовка
     */
    fun setTitle(text: String) : YesNoDialog {
        mTitle = text
        return this
    }

    /**
     * Задать текст сообщения
     *
     * @param text текст сообщения
     */
    fun setBody(text: String) : YesNoDialog {
        mBody = text
        return this
    }

    /**
     * Показать диалог
     */
    fun show() {
        AlertDialog.Builder(mContext)
                .setTitle(mTitle)
                .setMessage(mBody)
                .setPositiveButton(mContext.getString(R.string.yes), { _, _ -> mPositiveListener()})
                .setNegativeButton(mContext.getString(R.string.no), { _, _ -> mNegativeListener()})
                .create()
                .show()
    }
}