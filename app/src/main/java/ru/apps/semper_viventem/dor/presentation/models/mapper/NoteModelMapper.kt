package ru.apps.semper_viventem.dor.presentation.models.mapper

import ru.apps.semper_viventem.dor.domain.model.Note
import ru.apps.semper_viventem.dor.domain.model.mapper.TwoWayMapper
import ru.apps.semper_viventem.dor.presentation.models.NoteModel
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 22.07.2017.
 */
class NoteModelMapper @Inject constructor(): TwoWayMapper<Note, NoteModel> {

    override fun reverseMap(from: NoteModel): Note = Note(
            id = from.id,
            text = from.text,
            photos = from.photos,
            params = Note.NoteParams(
                    parentId = from.params?.parentId ?: 0,
                    date = from.params?.date ?: "",
                    position = from.params?.position ?: 0
            )
    )

    override fun map(from: Note): NoteModel = NoteModel(
            id = from.id,
            text = from.text,
            photos = from.photos,
            params = NoteModel.NoteParams(
                    parentId = from.params?.parentId ?: 0,
                    date = from.params?.date ?: "",
                    position = from.params?.position ?: 0
            )
    )
}