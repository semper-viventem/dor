package ru.apps.semper_viventem.dor.domain.iteractor

import io.adev.rxwrapper.RxAdapter
import io.reactivex.ObservableEmitter
import ru.apps.semper_viventem.dor.domain.model.Teacher
import ru.apps.semper_viventem.dor.domain.repository.TeacherRepository
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 08.05.2017.
 */
class GetTeacherList @Inject constructor(
        private val mTeacherRepository: TeacherRepository
): RxAdapter<List<Teacher>, Void>() {

    override fun execute(emitter: ObservableEmitter<List<Teacher>>, criteria: Void?) {

        val result = mTeacherRepository.getTeachers()
        emitter.onNext(result)
    }
}