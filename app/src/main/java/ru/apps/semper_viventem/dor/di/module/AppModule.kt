package ru.apps.semper_viventem.dor.di.module

import android.content.Context
import android.content.SharedPreferences
import com.google.android.gms.analytics.GoogleAnalytics
import com.google.android.gms.analytics.Tracker
import com.pawegio.kandroid.defaultSharedPreferences
import dagger.Module
import dagger.Provides
import ru.apps.semper_viventem.dor.App
import ru.apps.semper_viventem.dor.R
import javax.inject.Singleton

/**
 * @author Kulikov Konstantin
 * @since 01.05.2017.
 */
@Module
class AppModule(
        private val mApp: App
) {
    private var mTracker: Tracker? = null

    @Provides
    @Singleton
    fun provideContext(): Context = mApp.applicationContext

    @Provides
    @Singleton
    fun provideApp(): App = mApp

    @Provides
    @Singleton
    fun provideSharedPreferances(): SharedPreferences = mApp.defaultSharedPreferences

    @Provides
    @Singleton
    fun provideTracker(): Tracker {
        if (mTracker == null) {
            val analytics = GoogleAnalytics.getInstance(mApp)
            mTracker = analytics.newTracker(R.xml.global_tracker)
        }
        return mTracker!!
    }
}