package ru.apps.semper_viventem.dor.presentation.models

/**
 * @author Kulikov Konstantin
 * @since 17.08.2017.
 */
class UserModel(
        var id: Int,
        var firstName: String,
        var lastName: String,
        var photo: String,
        var body: String,
        var school: String
)