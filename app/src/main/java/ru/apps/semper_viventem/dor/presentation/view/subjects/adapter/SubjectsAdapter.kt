package ru.apps.semper_viventem.dor.presentation.view.subjects.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.chauthai.swipereveallayout.ViewBinderHelper
import com.pawegio.kandroid.inflateLayout
import kotlinx.android.synthetic.main.holder_subject.view.*
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.presentation.common.formatTeacherName
import ru.apps.semper_viventem.dor.presentation.models.SubjectModel

/**
 * @author Kulikov Konstantin
 * @since 07.05.2017.
 */
class SubjectsAdapter(
        private val mListener: OnSelectListener
): RecyclerView.Adapter<SubjectsAdapter.SubjectHolder>() {

    private val mViewBinderHolder = ViewBinderHelper()
    private var mSubjectList: List<SubjectModel> = emptyList()

    fun setData(data: List<SubjectModel>) {
        mSubjectList = data
        notifyDataSetChanged()
    }

    init {
        mViewBinderHolder.setOpenOnlyOne(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubjectHolder {
        return SubjectHolder(parent.context.inflateLayout(R.layout.holder_subject, attachToRoot = false))
    }

    override fun onBindViewHolder(holder: SubjectHolder?, position: Int) {
        holder?.bind(mSubjectList[position])
    }

    override fun getItemCount(): Int {
        return mSubjectList.size
    }

    inner class SubjectHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(subject: SubjectModel) {
            mViewBinderHolder.bind(itemView.vSwipeLayout, subject.id.toString())
            itemView.vTextSubjectName.text = subject.name
            itemView.vTextTeacherName.text = formatTeacherName(subject.teachers)
            itemView.vTextClassRoom.text = subject.classRoom
            itemView.vLayoutBody.setOnClickListener { mListener.subjectSelected(subject) }
            itemView.vLayoutRemove.setOnClickListener {
                mViewBinderHolder.closeLayout(subject.id.toString())
                notifyItemRemoved(position)
                (mSubjectList as MutableList).remove(subject)
                mListener.subjectRemove(subject)
            }
            itemView.vLayoutRemove.forceLayout()
            itemView.vLayoutRemove.requestLayout()

            //TODO надо передать [vLayoutRemove] высоту объекта [vLayoutBody], чтобы иконка отрисовывалась по центру
//            itemView.vLayoutRemove.layoutParams = ViewGroup.LayoutParams(
//                    itemView.context.resources.getDimensionPixelOffset(R.dimen.holder_remove_wight),
//                    itemView.vLayoutBody.layoutParams.height)
        }
    }

    /**
     * Слушатель нажатий на элементы списка
     */
    interface OnSelectListener {

        /**
         * Предмет выбран
         *
         * @param subject модель выбранного предмета
         */
        fun subjectSelected(subject: SubjectModel)

        /**
         * Удалить предмет
         *
         * @param subject модель предмета
         */
        fun subjectRemove(subject: SubjectModel)
    }
}