package ru.apps.semper_viventem.dor.data.entity.mapper

import ru.apps.semper_viventem.dor.data.entity.TeacherEntity
import ru.apps.semper_viventem.dor.domain.model.Teacher
import ru.apps.semper_viventem.dor.domain.model.mapper.TwoWayMapper
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 09.05.2017.
 */
class TeacherEntityMapper @Inject constructor() : TwoWayMapper<TeacherEntity, Teacher> {

    override fun reverseMap(from: Teacher): TeacherEntity {
        val teacher = TeacherEntity()
        teacher.id = from.id
        teacher.firstName = from.firstName
        teacher.lastName = from.lastName
        teacher.patronymic = from.patronymic
        teacher.phone = from.phone
        teacher.email = from.email
        teacher.address = from.address
        teacher.info = from.info

        return teacher
    }

    override fun map(from: TeacherEntity): Teacher {
        return Teacher(
                id = from.id!!,
                firstName = from.firstName,
                lastName = from.lastName,
                patronymic = from.patronymic,
                phone = from.phone,
                email = from.email,
                address = from.address,
                info = from.info
        )
    }
}