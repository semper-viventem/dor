package ru.apps.semper_viventem.dor.domain.iteractor

import io.adev.rxwrapper.RxAdapter
import io.reactivex.ObservableEmitter
import ru.apps.semper_viventem.dor.domain.repository.CallRepository
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 11.06.2017.
 */
class RemoveCall @Inject constructor(
        private val mCallRepository: CallRepository
) : RxAdapter<Void, Int>() {

    override fun execute(emitter: ObservableEmitter<Void>, criteria: Int?) {
        mCallRepository.removeCall(criteria!!)
    }
}