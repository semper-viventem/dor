package ru.apps.semper_viventem.dor.presentation.common.factory

import android.R
import android.content.Context
import android.widget.ArrayAdapter

/**
 * @property mCriteriaList массив соответствий <id способа сортировки> -> <id ресурса строки>
 *
 * @author Kulikov Konstantin
 * @since 17.06.2017.
 */
class SpinnerAdapterFactory(
        private vararg val mCriteriaArray: Pair<Int, Int>
) {

    /**
     * @return адаптер для spinner'а [ArrayAdapter]
     */
    fun create(context: Context): ArrayAdapter<String> {
        val list = ArrayList<String>()
        mCriteriaArray.forEach { (first, second) ->
            list.add(first, context.getString(second))
        }
        val adapter = ArrayAdapter<String>(context, R.layout.simple_spinner_item, list)
        adapter.setDropDownViewResource(android.support.design.R.layout.support_simple_spinner_dropdown_item)
        return adapter
    }

}