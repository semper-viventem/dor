package ru.apps.semper_viventem.dor.domain.model

import ru.apps.semper_viventem.dor.data.common.*

/**
 * @author Kulikov Konstantin
 * @since 06.05.2017.
 *
 * @property period период этой недели. Он же будет primary key
 * @property weekDays список дней недели. Их должно быть 7!
 */
class Week  (
        var period: String,
        var weekDays: List<WeekDay> = emptyList()
) {

    companion object {
        const val WEEK_MONDAY = MONDAY
        const val WEEK_TUESDAY = TUESDAY
        const val WEEK_WEDNESDAY = WEDNESDAY
        const val WEEK_THURSDAY = THURSDAY
        const val WEEK_FRIDAY = FRIDAY
        const val WEEK_SATURDAY = SATURDAY
        const val WEEK_SUNDAY = SUNDAY
    }

    override fun toString(): String {
        return "WeekModel(period='$period', mWeekDays=$weekDays)"
    }


}