package ru.apps.semper_viventem.dor.presentation.view.template.fragment

import  android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.google.android.gms.analytics.HitBuilders
import com.google.android.gms.analytics.Tracker
import com.pawegio.kandroid.inflateLayout
import kotlinx.android.synthetic.main.fragment_template.*
import ru.apps.semper_viventem.dor.App
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.presentation.common.fragments.BaseFragment
import ru.apps.semper_viventem.dor.presentation.models.TemplateModel
import ru.apps.semper_viventem.dor.presentation.presenter.template.TemplatePresenter
import ru.apps.semper_viventem.dor.presentation.view.template.TemplateView
import ru.apps.semper_viventem.dor.presentation.view.template.adapter.TemplateAdapter
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 05.05.2017.
 */
class TemplateFragment : BaseFragment(), TemplateView {

    companion object {
        fun newInstance(): TemplateFragment {
            val fragment = TemplateFragment()
            val args = Bundle()

            fragment.arguments = args
            return fragment
        }
    }

    @InjectPresenter
    lateinit var mTemplatePresenter: TemplatePresenter
    @Inject
    lateinit var mTracker: Tracker

    private lateinit var mAdapter: TemplateAdapter

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_template, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        App.component.inject(this)

        mTracker.setScreenName(this::class.java.simpleName)
        mTracker.send(HitBuilders.ScreenViewBuilder().build())


        mAdapter = TemplateAdapter(object : TemplateAdapter.OnSelectListener {
            override fun onSelected(templateModel: TemplateModel) {
                mTemplatePresenter.onTemplateSelected(templateModel)
            }

            override fun onRemove(templateModel: TemplateModel) {
                mTemplatePresenter.removeTemplate(templateModel)
            }

        })
        vRecyclerView.layoutManager = LinearLayoutManager(context)
        vRecyclerView.adapter = mAdapter

        mTemplatePresenter.loadTemplate()
    }

    override fun getToolbar(activityContext: Context): Toolbar? {
        val toolbar = activityContext.inflateLayout(R.layout.toolbar_default, attachToRoot = false) as Toolbar
        toolbar.setTitle(R.string.fragment_template_title)
        return toolbar
    }

    override fun onTemplateListLoaded(templates: List<TemplateModel>) {
        mAdapter.setData(templates)
        if (templates.isNotEmpty()) {
            vImageIsEmpty.visibility = View.GONE
        } else {
            vImageIsEmpty.visibility = View.VISIBLE
        }
    }
}