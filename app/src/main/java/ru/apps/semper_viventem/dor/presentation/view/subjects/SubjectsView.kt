package ru.apps.semper_viventem.dor.presentation.view.subjects

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.apps.semper_viventem.dor.presentation.models.SubjectModel

/**
 * @author Kulikov Konstantin
 * @since 05.05.2017.
 */
interface SubjectsView: MvpView {

    /**
     * Список предметов успешно загружен
     */
    fun onSubjectsLoadedComplited(subjects: List<SubjectModel>)

    /**
     * Показать диалог добавления товара
     */
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showAddSubjectDialog()

    /**
     * Показать детализацию выбранного предмета
     * @param subjectId идентификатор
     */
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showSelectedSubjectDialog(subjectId: Int)
}