package ru.apps.semper_viventem.dor.data.repository

import io.realm.Realm
import ru.apps.semper_viventem.dor.data.common.formatTemplateDate
import ru.apps.semper_viventem.dor.data.entity.TemplateEntity
import ru.apps.semper_viventem.dor.data.entity.mapper.TemplateEntittyMapper
import ru.apps.semper_viventem.dor.domain.model.Template
import ru.apps.semper_viventem.dor.domain.repository.TemplatesRepository
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Kulikov Konstantin
 * @since 08.07.2017.
 */
@Singleton
class TemplatesDataRepository @Inject constructor(
        private val mTemplateEntityMapper: TemplateEntittyMapper
) : TemplatesRepository {

    override fun addTemplate(template: Template) {
        Realm.getDefaultInstance().use { realm ->
            val newTemplate: TemplateEntity
            if (template.id == null || template.id == 0) {
                template.id = getNextKey(realm)
                newTemplate = getNewDaysModel( mTemplateEntityMapper.reverseMap(template) )
            } else {
                newTemplate = mTemplateEntityMapper.reverseMap(template)
            }
            realm.executeTransaction { realm ->
                realm.copyToRealmOrUpdate(newTemplate)
            }
        }
    }

    override fun getTemplateList(): List<Template> {
        return Realm.getDefaultInstance().use { realm ->
            val result = realm.where(TemplateEntity::class.java)
                    .findAll()
            mTemplateEntityMapper.map(result)
        }
    }

    override fun getTemplateById(id: Int): Template {
        return Realm.getDefaultInstance().use { realm ->
            val result = realm.where(TemplateEntity::class.java)
                    .equalTo(TemplateEntity.ID, id)
                    .findFirst()
            if (result != null) {
                mTemplateEntityMapper.map(result)
            } else {
                throw IllegalStateException("subject item not found with id='$id'")
            }
        }
    }

    override fun removeTemplate(id: Int) {
        Realm.getDefaultInstance().use { realm ->
            realm.executeTransaction { realm ->
                val result = realm.where(TemplateEntity::class.java)
                        .equalTo(TemplateEntity.ID, id)
                        .findAll()
                result.deleteAllFromRealm()
            }
        }
    }

    private fun getNextKey(realm: Realm): Int {
        val maxKey = realm.where(TemplateEntity::class.java).max(TemplateEntity.ID)
        return if (maxKey != null) maxKey.toInt() + 1 else 1
    }

    /**
     * Обновить периоды дней недели (чтобы не было зависимостей с текущеми днями)
     * @param templateEntity шаблон со старыми днями
     * @return шаблон с новыми днями [TemplateEntity]
     */
    private fun getNewDaysModel(templateEntity: TemplateEntity): TemplateEntity {
        templateEntity.days?.forEach { it.date = formatTemplateDate(it.date, templateEntity.id.toString()) }
        return templateEntity
    }
}