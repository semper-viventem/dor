package ru.apps.semper_viventem.dor.presentation.view.schedule.dialog

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatDialogFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.dialog_select_template.*
import kotlinx.android.synthetic.main.layout_header.*
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.presentation.models.TemplateModel
import ru.apps.semper_viventem.dor.presentation.presenter.schedule.SelectTemplatePresenter
import ru.apps.semper_viventem.dor.presentation.view.schedule.SelectTemplateView
import ru.apps.semper_viventem.dor.presentation.view.schedule.adapter.SelectTemplateAdapter

/**
 * @author Kulikov Konstantin
 * @since 08.07.2017.
 */
class SelectTemplateDialog: MvpAppCompatDialogFragment(), SelectTemplateView {

    companion object {
        fun newInstance(
                onSelected: (template: TemplateModel) -> Unit = {}
        ) : SelectTemplateDialog {
            val dialog = SelectTemplateDialog()
            val args = Bundle()
            dialog.arguments = args
            dialog.mOnSelected = onSelected
            return dialog
        }
    }

    @InjectPresenter
    lateinit var mPresenter: SelectTemplatePresenter
    private var mOnSelected: (template: TemplateModel) -> Unit = {}

    private lateinit var mAdapter: SelectTemplateAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_select_template, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        vTextHeadTitle.text = getString(R.string.dialog_select_template)
        vButtonNegative.setOnClickListener { dismiss() }

        mAdapter = SelectTemplateAdapter(object : SelectTemplateAdapter.OnSelectListener {
            override fun onSelected(template: TemplateModel) {
                mOnSelected(template)
                dismiss()
            }
        })
        vRecyclerView.layoutManager = LinearLayoutManager(context)
        vRecyclerView.adapter = mAdapter

        mPresenter.loadTemplateList()
    }

    override fun onTemplateLoaded(template: List<TemplateModel>) {
        if (template.isEmpty()) {
            vTextNotTemplates.visibility = View.VISIBLE
        } else {
            vTextNotTemplates.visibility = View.GONE
            mAdapter.setData(template)
        }
    }
}