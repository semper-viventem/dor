package ru.apps.semper_viventem.dor.presentation.view.schedule.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatDialogFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.dialog_save_template.*
import kotlinx.android.synthetic.main.layout_dialog_buttons.*
import kotlinx.android.synthetic.main.layout_header.*
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.presentation.models.WeekModel
import ru.apps.semper_viventem.dor.presentation.presenter.schedule.SaveTemplatePresenter
import ru.apps.semper_viventem.dor.presentation.view.schedule.SaveTemplateView

/**
 * @author Kulikov Konstantin
 * @since 08.07.2017.
 */
class SaveTemplateDialog: MvpAppCompatDialogFragment(), SaveTemplateView {

    companion object {
        fun newInstance(week: WeekModel, listener: (name: String) -> Unit = {}): SaveTemplateDialog {
            val dialog = SaveTemplateDialog()
            val args = Bundle()
            dialog.mWeek = week
            dialog.mListener = listener
            dialog.arguments = args
            return dialog
        }
    }

    @InjectPresenter
    lateinit var mPresenter: SaveTemplatePresenter

    private lateinit var mWeek: WeekModel
    private var mListener: (name: String) -> Unit = {}

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_save_template, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        vTextHeadTitle.text = getString(R.string.save_template_dialog_title)
        mPresenter.createTemplateByWeek(mWeek)
        vButtonPositive.setOnClickListener {
            checkAndSave()
        }
        vButtonNegative.setOnClickListener { dismiss() }
    }

    private fun checkAndSave() {
        val name = vEditTextTemplateName.text
        if (name.isBlank()) {
            vEditTextTemplateName.error = getString(R.string.dialog_save_template_input_error)
        } else {
            mPresenter.saveTemplate(name.toString())
        }
    }

    override fun templateIsSaved(name: String) {
        mListener(name)
        dismiss()
    }
}