package ru.apps.semper_viventem.dor.presentation.event

import android.view.View

/**
 * @author Kulikov Konstantin
 * @since 18.06.2017.
 */

class SnackBarAction(
        val text: String,
        val listener: (v: View) -> Unit
)
