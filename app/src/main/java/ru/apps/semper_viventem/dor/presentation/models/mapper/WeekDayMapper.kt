package ru.apps.semper_viventem.dor.presentation.models.mapper

import ru.apps.semper_viventem.dor.domain.model.WeekDay
import ru.apps.semper_viventem.dor.domain.model.mapper.TwoWayMapper
import ru.apps.semper_viventem.dor.presentation.models.WeekDayModel
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 06.05.2017.
 */
class WeekDayMapper @Inject constructor(
        private val mSubjectMapper: SubjectMapper
) : TwoWayMapper<WeekDay, WeekDayModel>{

    override fun reverseMap(from: WeekDayModel): WeekDay {
        return WeekDay(
                date = from.date,
                subjects = mSubjectMapper.reverseMap(from.subjects),
                note = from.note,
                color = from.color,
                weekDay = from.weekDay
        )
    }

    override fun map(from: WeekDay): WeekDayModel {
        return WeekDayModel(
                date = from.date,
                subjects = mSubjectMapper.map(from.subjects),
                note = from.note,
                color = from.color,
                weekDay = from.weekDay
        )
    }
}