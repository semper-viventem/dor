package ru.apps.semper_viventem.dor.presentation.models.mapper

import ru.apps.semper_viventem.dor.domain.model.Template
import ru.apps.semper_viventem.dor.domain.model.mapper.TwoWayMapper
import ru.apps.semper_viventem.dor.presentation.models.TemplateModel
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 08.07.2017.
 */
class TemplateModelMapper @Inject constructor(
        private val mWeekDayModlMapper: WeekDayMapper
) : TwoWayMapper<Template, TemplateModel> {

    override fun reverseMap(from: TemplateModel): Template {
        return Template(
                from.id,
                from.name,
                mWeekDayModlMapper.reverseMap(from.days)
        )
    }

    override fun map(from: Template): TemplateModel {
        return TemplateModel(
                from.id,
                from.name,
                mWeekDayModlMapper.map(from.days)
        )
    }
}