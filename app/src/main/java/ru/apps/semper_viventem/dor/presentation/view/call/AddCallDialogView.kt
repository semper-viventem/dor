package ru.apps.semper_viventem.dor.presentation.view.call

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.apps.semper_viventem.dor.presentation.models.CallModel

/**
 * @author Kulikov Konstantin
 * @since 18.06.2017.
 */
interface AddCallDialogView: MvpView {

    /**
     * Закрывает экран
     */
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun closeDialog()

    /**
     * Выбранный звонок загружен
     * @param callModel модель звонка
     */
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun callLoaded(callModel: CallModel)
}