package ru.apps.semper_viventem.dor.di.module

import dagger.Module
import dagger.Provides
import ru.apps.semper_viventem.dor.data.entity.mapper.*
import ru.apps.semper_viventem.dor.data.repository.*
import ru.apps.semper_viventem.dor.domain.repository.*
import javax.inject.Singleton

/**
 * @author Kulikov Konstantin
 * @since 01.05.2017.
 */
@Module
class DataModule {

    @Singleton
    @Provides
    fun provideSubjectRepository(mapper: SubjectEntityMapper) : SubjectRepository = SubjectDataRepository(mapper)

    @Singleton
    @Provides
    fun provideTeacherRepository(mapper: TeacherEntityMapper) : TeacherRepository = TeacherDataRepository(mapper)

    @Singleton
    @Provides
    fun provideCallRepository(mapper: CallEntityMapper) : CallRepository = CallDataRepository(mapper)

    @Singleton
    @Provides
    fun provideWeekRepository(mapper: WeekEntityMapper): WeekRepository = WeekDataRepository(mapper)

    @Singleton
    @Provides
    fun provideTemplateRepository(mapper: TemplateEntittyMapper): TemplatesRepository = TemplatesDataRepository(mapper)

    @Singleton
    @Provides
    fun provideNoteRepository(mapper: NoteEntityMapper): NoteRepository = NoteDataRepository(mapper)
}