package ru.apps.semper_viventem.dor.domain.repository

import ru.apps.semper_viventem.dor.domain.model.Teacher

/**
 * @author Kulikov Konstantin
 * @since 10.05.2017.
 */
interface TeacherRepository {

    /**
     * Получить список преподавателей
     *
     * @return список преподавателей [Teacher]
     */
    fun getTeachers(): List<Teacher>

    /**
     * Добавить преподавателя
     *
     * @param teacher модель преподавателя
     */
    fun addTeacher(teacher: Teacher)

    /**
     * Получить преподавателя по его идентификатору
     *
     * @param teacherId идентификатор преподавателя
     * @return модель преподавателя
     */
    fun getTeacherById(teacherId: Int): Teacher

    /**
     * Удалить преподавателя по его идентификатору
     *
     * @param teacherId идентификатор преподавателя
     */
    fun removeTeacherById(teacherId: Int)
}