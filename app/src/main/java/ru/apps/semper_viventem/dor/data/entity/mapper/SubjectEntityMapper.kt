package ru.apps.semper_viventem.dor.data.entity.mapper

import ru.apps.semper_viventem.dor.data.entity.SubjectEntity
import ru.apps.semper_viventem.dor.domain.model.Subject
import ru.apps.semper_viventem.dor.domain.model.mapper.TwoWayMapper
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 09.05.2017.
 */
class SubjectEntityMapper @Inject constructor(
        private val mTeacherEntityMapper: TeacherEntityMapper
) : TwoWayMapper<SubjectEntity, Subject> {
    override fun reverseMap(from: Subject): SubjectEntity {
        val subject = SubjectEntity()
        subject.id = from.id
        subject.name = from.name
        subject.classRoom = from.classRoom
        subject.info = from.info

        subject.teachers = RealmListMapper(mTeacherEntityMapper).map(from.teachers)

        return subject
    }

    override fun map(from: SubjectEntity): Subject {
        return Subject(
                id = from.id!!,
                name = from.name,
                classRoom = from.classRoom,
                teachers = mTeacherEntityMapper.map(from.teachers?: emptyList()),
                info = from.info
        )
    }
}