package ru.apps.semper_viventem.dor.domain.model

import android.graphics.Color

/**
 * @author Kulikov Konstantin
 * @since 06.05.2017.
 *
 * @property date дата
 * @property subjects список предметов в этот день
 * @property note заметка
 * @property color цвет этого дня
 */
class WeekDay (
        var date: String = "",
        var weekDay: Int,
        var subjects: List<Subject>,
        var note: String = "",
        var color: Int = Color.WHITE
) {

    override fun toString(): String {
        return "WeekDayModel(date='$date', subjects=$subjects, note='$note', color=$color)"
    }
}