package ru.apps.semper_viventem.dor.presentation.view.subjects.fragment

import android.content.Context
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.google.android.gms.analytics.HitBuilders
import com.google.android.gms.analytics.Tracker
import com.pawegio.kandroid.inflateLayout
import kotlinx.android.synthetic.main.fragment_subjects.*
import ru.apps.semper_viventem.dor.App
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.presentation.common.factory.DecoratorFactory
import ru.apps.semper_viventem.dor.presentation.common.fragments.BaseFragment
import ru.apps.semper_viventem.dor.presentation.common.fragments.FabFragmentMarker
import ru.apps.semper_viventem.dor.presentation.models.SubjectModel
import ru.apps.semper_viventem.dor.presentation.presenter.subjects.SubjectsPresenter
import ru.apps.semper_viventem.dor.presentation.view.subjects.SubjectsView
import ru.apps.semper_viventem.dor.presentation.view.subjects.adapter.SubjectsAdapter
import ru.apps.semper_viventem.dor.presentation.view.subjects.dialog.AddSubjectsDialog
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 05.05.2017.
 */
class SubjectsFragment : BaseFragment(), SubjectsView, FabFragmentMarker {

    companion object {
        fun newInstance(): SubjectsFragment {
            val fragment = SubjectsFragment()
            val args = Bundle()

            fragment.arguments = args
            return fragment
        }
    }

    @InjectPresenter
    lateinit var mSubjectsPresenter: SubjectsPresenter
    @Inject
    lateinit var mTracker: Tracker

    private lateinit var mSubjectsAdapter: SubjectsAdapter

    private var mFab: FloatingActionButton? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_subjects, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        App.component.inject(this)

        mTracker.setScreenName(this::class.java.simpleName)
        mTracker.send(HitBuilders.ScreenViewBuilder().build())


        mSubjectsAdapter = SubjectsAdapter(object : SubjectsAdapter.OnSelectListener {
            override fun subjectSelected(subject: SubjectModel) {
                mSubjectsPresenter.subjectSelected(subject)
            }

            override fun subjectRemove(subject: SubjectModel) {
                mSubjectsPresenter.subjectRemoveSelected(subject)
            }
        })
        vRecyclerView.layoutManager = LinearLayoutManager(context)
        vRecyclerView.adapter = mSubjectsAdapter
        vRecyclerView.addItemDecoration(DecoratorFactory().getDefaultDecorator())
        vRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                if (dy > 0)
                    mFab?.hide()
                else if (dy < 0)
                    mFab?.show()
            }
        })

        mFab?.setOnClickListener {
            mSubjectsPresenter.addSubjectButtonSelected()
        }
        mFab?.setImageDrawable(resources.getDrawable(R.drawable.ic_add))



        mSubjectsPresenter.loadSubjectList()
    }

    override fun getToolbar(activityContext: Context): Toolbar? {
        val toolbar = activityContext.inflateLayout(R.layout.toolbar_default, attachToRoot = false) as Toolbar
        toolbar.setTitle(R.string.fragment_subjects_title)
        return toolbar
    }

    override fun onSubjectsLoadedComplited(subjects: List<SubjectModel>) {
        mSubjectsAdapter.setData(subjects)
        if (subjects.isNotEmpty()) {
            vImageIsEmpty.visibility = View.GONE
        } else {
            vImageIsEmpty.visibility = View.VISIBLE
        }
    }

    override fun showAddSubjectDialog() {
        AddSubjectsDialog.newInstance()
                .setOnAddedListener(object : AddSubjectsDialog.OnSubjectAddedListener {
                    override fun onSubjectAdded() {
                        mSubjectsPresenter.loadSubjectList()
                    }
                })
                .show(fragmentManager, "")
    }

    override fun showSelectedSubjectDialog(subjectId: Int) {
        AddSubjectsDialog.newInstance(subjectId)
                .setOnAddedListener(object : AddSubjectsDialog.OnSubjectAddedListener {
                    override fun onSubjectAdded() {
                        mSubjectsPresenter.loadSubjectList()
                    }
                })
                .show(fragmentManager, "")
    }

    override fun fabProvided(fab: FloatingActionButton) {
        mFab = fab
    }
}