package ru.apps.semper_viventem.dor.presentation.presenter.schedule

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.adev.rxwrapper.util.observer
import ru.apps.semper_viventem.dor.App
import ru.apps.semper_viventem.dor.data.common.rx.asyncUseCase
import ru.apps.semper_viventem.dor.domain.iteractor.GetAllTemplates
import ru.apps.semper_viventem.dor.presentation.models.TemplateModel
import ru.apps.semper_viventem.dor.presentation.models.mapper.TemplateModelMapper
import ru.apps.semper_viventem.dor.presentation.view.schedule.SelectTemplateView
import timber.log.Timber
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 08.07.2017.
 */
@InjectViewState
class SelectTemplatePresenter : MvpPresenter<SelectTemplateView>() {

    @Inject
    lateinit var mGetTemplateList: GetAllTemplates
    @Inject
    lateinit var mTemplateMapper: TemplateModelMapper

    init {
        App.component.inject(this)
    }

    private var mTemplateList: List<TemplateModel> = emptyList()

    /**
     * Загрузить список шаблонов
     */
    fun loadTemplateList() {
        asyncUseCase(mGetTemplateList).execute(observer({ templates ->
            mTemplateList = mTemplateMapper.map(templates)
            mTemplateList.sortedBy { it.name }

            viewState.onTemplateLoaded(mTemplateList)
        }, { error ->
            Timber.e(error)
        }))
    }
}