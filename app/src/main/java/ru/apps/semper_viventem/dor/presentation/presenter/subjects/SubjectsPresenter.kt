package ru.apps.semper_viventem.dor.presentation.presenter.subjects

import android.content.Context
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.adev.rxwrapper.util.observer
import ru.apps.semper_viventem.dor.App
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.data.common.rx.asyncUseCase
import ru.apps.semper_viventem.dor.data.common.rx.bus.ui.UiBus
import ru.apps.semper_viventem.dor.domain.iteractor.GetLocalSubjectList
import ru.apps.semper_viventem.dor.domain.iteractor.RemoveSubjectById
import ru.apps.semper_viventem.dor.domain.iteractor.SaveLocalSubject
import ru.apps.semper_viventem.dor.presentation.event.NotificationEvent
import ru.apps.semper_viventem.dor.presentation.event.ShowProgresEvent
import ru.apps.semper_viventem.dor.presentation.event.ShowProgresEvent.ProgresStatus.HIDE
import ru.apps.semper_viventem.dor.presentation.event.ShowProgresEvent.ProgresStatus.SHOW
import ru.apps.semper_viventem.dor.presentation.event.SnackBarAction
import ru.apps.semper_viventem.dor.presentation.models.SubjectModel
import ru.apps.semper_viventem.dor.presentation.models.mapper.SubjectMapper
import ru.apps.semper_viventem.dor.presentation.view.subjects.SubjectsView
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 05.05.2017.
 */
@InjectViewState
class SubjectsPresenter: MvpPresenter<SubjectsView>() {

    @Inject
    lateinit var mContext: Context
    @Inject
    lateinit var mUiBus: UiBus
    @Inject
    lateinit var mGetSubjectList: GetLocalSubjectList
    @Inject
    lateinit var mSubjectMapper: SubjectMapper
    @Inject
    lateinit var mRemoveSubject: RemoveSubjectById
    @Inject
    lateinit var mSaveSubject: SaveLocalSubject

    private var mSubjectList: List<SubjectModel> = emptyList()

    init {
        App.component.inject(this)
    }

    /**
     * Загрузить список предметов
     */
    fun loadSubjectList() {
        mUiBus.post(ShowProgresEvent(SHOW))
        asyncUseCase(mGetSubjectList).execute(observer({ subjects ->
            mSubjectList = mSubjectMapper.map(subjects)
            viewState.onSubjectsLoadedComplited(mSubjectList)
            mUiBus.post(ShowProgresEvent(HIDE))
        }, { error ->
            error.printStackTrace()
            mUiBus.post(NotificationEvent(mContext.getString(R.string.error_with_loading_subjects)))
            mUiBus.post(ShowProgresEvent(HIDE))
        }))
    }

    /**
     * Кнопка добавления нового предмета
     */
    fun addSubjectButtonSelected() {
        viewState.showAddSubjectDialog()
    }

    /**
     * Предмет выбран
     *
     * @param subject модель предмета
     */
    fun subjectSelected(subject: SubjectModel) {
        viewState.showSelectedSubjectDialog(subject.id)
    }

    /**
     * Запрос на удаление предмета из репозитория
     *
     * @param subject модеь предмета
     */
    fun subjectRemoveSelected(subject: SubjectModel) {
        asyncUseCase(mRemoveSubject).execute(observer({
            // onNext
        },{
            //loadSubjectList()
            (mSubjectList as MutableList).remove(subject)
            if (mSubjectList.isEmpty()) viewState.onSubjectsLoadedComplited(mSubjectList)
            mUiBus.post(NotificationEvent(mContext.getString(R.string.subject_was_be_removed), action = SnackBarAction(mContext.getString(R.string.cancel), {
                saveSubject(subject)
            })))
        },{ error ->
            error.printStackTrace()
            mUiBus.post(NotificationEvent(mContext.getString(R.string.error_with_remove_subject)))
        }), subject.id)
    }

    private fun saveSubject(subject: SubjectModel) {
        asyncUseCase(mSaveSubject).execute(observer({
            // onNext
        }, {
            loadSubjectList()
        }, { error ->
            error.printStackTrace()
        }), mSubjectMapper.reverseMap(subject))
    }
}