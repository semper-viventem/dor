package ru.apps.semper_viventem.dor.presentation.common

/**
 * @author Kulikov Konstantin
 * @since 14.07.2017.
 */

/** Разрешение камеры **/
const val PERMISSION_REQUEST_CAMERA = 1

/** Разрешение хранилища **/
const val PERMISSION_REQUEST_STORAGE = 2

/** Событие получения фото из галереи **/
const val RESULT_LOAD_IMAGE_FROM_GALLERY = 3

/** Событие получения фото из камеры **/
const val RESULT_LOAD_IMAGE_FROM_CAMERA = 4

const val MODE_TRANSFORM_DURATION: Long = 500