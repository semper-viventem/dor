package ru.apps.semper_viventem.dor.presentation.models

import java.io.Serializable

/**
 * @author Kulikov Konstantin
 * @since 07.05.2017.
 *
 * @property id идентификатор
 * @property firstName имя преподавателя
 * @property lastName фамилия преподавателя
 * @property patronymic отчество преподавателя
 * @property patronymic телефонный номер преподавателя
 * @property email адрес электронной почты преподавателя
 * @property address адрес преподавателя
 * @property info дополнительная информация о преподавателе
 */
class TeacherModel(
        var id: Int = 0,
        var firstName: String = "",
        var lastName: String = "",
        var patronymic: String = "",
        var phone: String = "",
        var email: String = "",
        var address: String = "",
        var info: String = ""
) : Serializable{

    /**
     * Выбран ли этот объект
     */
    var isChecked: Boolean = false

    /**
     * Вернуть имя учителя в формате [Имя Фамилия Отчество]
     */
    fun getNameFormat() = "$lastName $firstName $patronymic"

    override fun toString(): String {
        return "TeacherModel(id='$id', firstName='$firstName', lastName='$lastName', patronymic='$patronymic', phone='$phone', email='$email', address='$address', info='$info')"
    }
}