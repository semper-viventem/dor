package ru.apps.semper_viventem.dor.presentation.models

/**
 * @author Kulikov Konstantin
 * @since 08.07.2017.
 */
class TemplateModel (
        val id: Int? = null,
        var name: String = "",
        var days: List<WeekDayModel> = emptyList(),
        var isOnline: Boolean = false
)