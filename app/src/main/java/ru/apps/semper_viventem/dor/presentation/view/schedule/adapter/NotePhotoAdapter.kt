package ru.apps.semper_viventem.dor.presentation.view.schedule.adapter

import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.pawegio.kandroid.inflateLayout
import kotlinx.android.synthetic.main.holder_photo.view.*
import ru.apps.semper_viventem.dor.R

/**
 * @author Kulikov Konstantin
 * @since 18.07.2017.
 */
class NotePhotoAdapter (
        private val mListener: OnSelectListener
) : RecyclerView.Adapter<NotePhotoAdapter.NotePhotoBaseHolder>() {

    companion object {
        private const val TYPE_DEFAULT = 0
        private const val TYPE_ADD_PHOTO = 1
    }

    private var mUriList: List<String> = emptyList()

    fun setData(data: List<String>) {
        mUriList = data
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> TYPE_ADD_PHOTO
            else -> TYPE_DEFAULT
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotePhotoBaseHolder {
        return when (viewType) {
            TYPE_DEFAULT -> PhotoHolder(parent.context.inflateLayout(R.layout.holder_photo, attachToRoot = false))
            TYPE_ADD_PHOTO -> AddPhotoHolder(parent.context.inflateLayout(R.layout.holder_add_photo, attachToRoot = false))
            else -> throw Exception("unknown type: $viewType")
        }
    }

    override fun onBindViewHolder(holder: NotePhotoBaseHolder?, position: Int) {
        when (holder) {
            is PhotoHolder -> holder.bind(mUriList[position-1])
            is AddPhotoHolder -> holder.bind()
        }
    }

    override fun getItemCount(): Int {
        return mUriList.size + 1
    }

    abstract class NotePhotoBaseHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private inner class PhotoHolder(itemView: View): NotePhotoBaseHolder(itemView) {
        fun bind(uri: String) {
            Glide.with(itemView.context)
                    .load(Uri.parse(uri))
                    .into(itemView.vImageView)
            itemView.setOnClickListener { mListener.onSelectListener(uri, adapterPosition - 1) }
            itemView.vRemoveButton.setOnClickListener {
                (mUriList as ArrayList).removeAt(adapterPosition-1)
                mListener.onRemoveSelectListener(uri, adapterPosition - 1)
                notifyItemRemoved(adapterPosition)
            }
        }
    }

    private inner class AddPhotoHolder(itemView: View): NotePhotoBaseHolder(itemView) {
        fun bind() {
            itemView.setOnClickListener { mListener.onAddPhotoSelected() }
        }
    }

    interface OnSelectListener {

        fun onRemoveSelectListener(uri: String, position: Int)

        fun onSelectListener(uri: String, position: Int)

        fun onAddPhotoSelected()
    }
}