package ru.apps.semper_viventem.dor.presentation.presenter.schedule

import android.content.Context
import android.net.Uri
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.adev.rxwrapper.util.observer
import ru.apps.semper_viventem.dor.App
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.data.common.rx.asyncUseCase
import ru.apps.semper_viventem.dor.data.common.rx.bus.ui.UiBus
import ru.apps.semper_viventem.dor.domain.iteractor.GetNoteById
import ru.apps.semper_viventem.dor.domain.iteractor.GetNoteByParams
import ru.apps.semper_viventem.dor.domain.iteractor.SaveLocalNote
import ru.apps.semper_viventem.dor.domain.model.Note
import ru.apps.semper_viventem.dor.presentation.event.NotificationEvent
import ru.apps.semper_viventem.dor.presentation.models.NoteModel
import ru.apps.semper_viventem.dor.presentation.models.mapper.NoteModelMapper
import ru.apps.semper_viventem.dor.presentation.view.schedule.SubjectDetailsView
import timber.log.Timber
import javax.inject.Inject


/**
 * @author Kulikov Konstantin
 * @since 13.07.2017.
 */
@InjectViewState
class SubjectDetailsPresenter : MvpPresenter<SubjectDetailsView>() {

    @Inject
    lateinit var mContext: Context
    @Inject
    lateinit var mUiBus: UiBus
    @Inject
    lateinit var mNoteMapper: NoteModelMapper
    @Inject
    lateinit var mGetNoteById: GetNoteById
    @Inject
    lateinit var mSaveNote: SaveLocalNote
    @Inject
    lateinit var mGetNoteByParams: GetNoteByParams

    private var mNoteModel: NoteModel = NoteModel()

    init {
        App.component.inject(this)
    }

    /**
     * Загрузить заметку
     *
     * @param noteId идентификатор заметки
     * @param params параметры идентификации заметки
     */
    fun loadNote(noteId: Int, params: NoteModel.NoteParams) {

        mNoteModel.params = params
        if (noteId != -1) {
            loadNoteById(noteId)
        } else {
            loadNoteByParams(params)
        }
    }

    private fun loadNoteById(noteId: Int) {
        asyncUseCase(mGetNoteById).execute(observer({ note ->
            mNoteModel = mNoteMapper.map(note)
            viewState.noteLoaded(mNoteModel)
        }, { error ->
            Timber.e(error)
        }), noteId)
    }

    private fun loadNoteByParams(noteParams: NoteModel.NoteParams) {
        asyncUseCase(mGetNoteByParams).execute(observer({ note ->
            mNoteModel = mNoteMapper.map(note)
            viewState.noteLoaded(mNoteModel)
        }, { error ->
            Timber.e(error)
        }), Note.NoteParams(
                parentId = noteParams.parentId,
                date = noteParams.date,
                position = noteParams.position
        ))
    }

    /**
     * Обновить текст заметки
     * @param text новый текст
     */
    fun onTextUpdate(text: String) {
        mNoteModel.text = text
    }

    /**
     * Добавить фото с камеры
     */
    fun onAddPhotoCamera(uri: Uri) {
        if (mNoteModel.photos.find { it == uri.toString() } != null) {
            showAlredyMessage()
            return
        }
        mNoteModel.photos.add(uri.toString())
        viewState.noteLoaded(mNoteModel)
    }

    /**
     * Добавить фото из галереи
     */
    fun onAddPhotoGalery(uri: Uri) {
        if (mNoteModel.photos.find { it == uri.toString() } != null) {
            showAlredyMessage()
            return
        }
        mNoteModel.photos.add(uri.toString())
        viewState.noteLoaded(mNoteModel)
    }

    /**
     * Удалить фото
     */
    fun onRemovePhoto(uri: String) {
        mNoteModel.photos.remove(uri)
    }

    /**
     * Сохранить изменения
     */
    fun onSaveNote() {
        asyncUseCase(mSaveNote).execute(observer({
            // onNext
        }, {
            viewState.closeDialog()
        }, { error ->
            Timber.e(error)
            mUiBus.post(NotificationEvent(mContext.getString(R.string.notification_error_with_save_note)))
        }), mNoteMapper.reverseMap(mNoteModel))
    }

    private fun showAlredyMessage() {
        mUiBus.post(NotificationEvent(mContext.getString(R.string.notification_this_photo_alredy)))
    }
}