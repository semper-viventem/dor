package ru.apps.semper_viventem.dor.di.component

import dagger.Component
import ru.apps.semper_viventem.dor.di.module.AppModule
import ru.apps.semper_viventem.dor.di.module.DataModule
import ru.apps.semper_viventem.dor.di.module.NavigationModule
import ru.apps.semper_viventem.dor.presentation.common.factory.DecoratorFactory
import ru.apps.semper_viventem.dor.presentation.presenter.call.AddCallDialogPresenter
import ru.apps.semper_viventem.dor.presentation.presenter.call.CallPresenter
import ru.apps.semper_viventem.dor.presentation.presenter.guide.GuidePresenter
import ru.apps.semper_viventem.dor.presentation.presenter.main.MainPresenter
import ru.apps.semper_viventem.dor.presentation.presenter.schedule.SaveTemplatePresenter
import ru.apps.semper_viventem.dor.presentation.presenter.schedule.SchedulePresenter
import ru.apps.semper_viventem.dor.presentation.presenter.schedule.SelectTemplatePresenter
import ru.apps.semper_viventem.dor.presentation.presenter.schedule.SubjectDetailsPresenter
import ru.apps.semper_viventem.dor.presentation.presenter.settings.SettingsPresenter
import ru.apps.semper_viventem.dor.presentation.presenter.subjects.SelectSubjectPresenter
import ru.apps.semper_viventem.dor.presentation.presenter.subjects.SubjectsPresenter
import ru.apps.semper_viventem.dor.presentation.presenter.teachers.TeachersPresenter
import ru.apps.semper_viventem.dor.presentation.presenter.template.TemplatePresenter
import ru.apps.semper_viventem.dor.presentation.view.call.fragment.CallFragment
import ru.apps.semper_viventem.dor.presentation.view.guide.fragment.GuideFragment
import ru.apps.semper_viventem.dor.presentation.view.main.activity.MainActivity
import ru.apps.semper_viventem.dor.presentation.view.schedule.fragment.ScheduleFragment
import ru.apps.semper_viventem.dor.presentation.view.settings.fragment.SettingsFragment
import ru.apps.semper_viventem.dor.presentation.view.subjects.dialog.AddSubjectsDialog
import ru.apps.semper_viventem.dor.presentation.view.subjects.fragment.SubjectsFragment
import ru.apps.semper_viventem.dor.presentation.view.teachers.dialog.AddTeacherDialog
import ru.apps.semper_viventem.dor.presentation.view.teachers.fragment.TeachersFragment
import ru.apps.semper_viventem.dor.presentation.view.template.fragment.TemplateFragment
import ru.apps.semper_viventem.dor.widget.ScheduleWidget
import javax.inject.Singleton

/**
 * @author Konstantin Kulikov
 * @since 01.05.17
 */
@Singleton
@Component(modules = arrayOf(
        AppModule::class,
        DataModule::class,
        NavigationModule::class
))
interface AppComponent {

    fun inject(mainPresenter: MainPresenter)
    fun inject(mainPresenter: MainActivity)
    fun inject(schedulePresenter: SchedulePresenter)
    fun inject(callPresenter: CallPresenter)
    fun inject(templatePresenter: TemplatePresenter)
    fun inject(teachersPresenter: TeachersPresenter)
    fun inject(subjectsPresenter: SubjectsPresenter)
    fun inject(addSubjectsDialog: AddSubjectsDialog)
    fun inject(addTeacherDialog: AddTeacherDialog)
    fun inject(addCallDialogPresenter: AddCallDialogPresenter)
    fun inject(selectSubjectPresenter: SelectSubjectPresenter)
    fun inject(saveTemplatePresenter: SaveTemplatePresenter)
    fun inject(selectTemplatePresenter: SelectTemplatePresenter)
    fun inject(subjectDetailsPresenter: SubjectDetailsPresenter)
    fun inject(decoratorFactory: DecoratorFactory)
    fun inject(settingsPresenter: SettingsPresenter)
    fun inject(guidePresenter: GuidePresenter)
    fun inject(scheduleWidget: ScheduleWidget)
    fun inject(scheduleFragment: ScheduleFragment)
    fun inject(callFragment: CallFragment)
    fun inject(templateFragment: TemplateFragment)
    fun inject(subjectsFragment: SubjectsFragment)
    fun inject(teachersFragment: TeachersFragment)
    fun inject(settingsFragment: SettingsFragment)
    fun inject(guideFragment: GuideFragment)

}