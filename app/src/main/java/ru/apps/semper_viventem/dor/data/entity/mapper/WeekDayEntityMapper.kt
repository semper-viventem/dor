package ru.apps.semper_viventem.dor.data.entity.mapper

import ru.apps.semper_viventem.dor.data.entity.WeekDayEntity
import ru.apps.semper_viventem.dor.domain.model.WeekDay
import ru.apps.semper_viventem.dor.domain.model.mapper.TwoWayMapper
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 25.06.2017.
 */
class WeekDayEntityMapper @Inject constructor(
        private val mSubjectMapper: SubjectEntityMapper
): TwoWayMapper<WeekDayEntity, WeekDay> {

    override fun reverseMap(from: WeekDay): WeekDayEntity {
        return WeekDayEntity().apply {
            date = from.date
            note = from.note
            subjects = RealmListMapper(mSubjectMapper).map(from.subjects)
            color = from.color
            weekDay = from.weekDay
        }
    }

    override fun map(from: WeekDayEntity): WeekDay {
        return WeekDay(
                date = from.date,
                note = from.note,
                color = from.color,
                weekDay = from.weekDay,
                subjects = mSubjectMapper.map(from.subjects?: emptyList())
        )
    }
}