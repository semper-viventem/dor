package ru.apps.semper_viventem.dor.data.common.rx.bus.ui

import ru.apps.semper_viventem.dor.data.common.rx.bus.RxBus
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Kulikov Konstantin
 * @since 06.05.17
 */
@Singleton
class UiBus @Inject constructor() : RxBus<UiEvent>()