package ru.apps.semper_viventem.dor.data.entity

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/**
 * @author Kulikov Konstantin
 * @since 22.07.2017.
 */

@RealmClass
open class RealmString: RealmObject() {

    @PrimaryKey
    open var text: String = ""
}