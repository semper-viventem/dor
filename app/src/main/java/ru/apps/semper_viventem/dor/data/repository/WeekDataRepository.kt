package ru.apps.semper_viventem.dor.data.repository

import io.realm.Realm
import ru.apps.semper_viventem.dor.data.entity.WeekEntity
import ru.apps.semper_viventem.dor.data.entity.mapper.WeekEntityMapper
import ru.apps.semper_viventem.dor.domain.model.Week
import ru.apps.semper_viventem.dor.domain.repository.WeekRepository
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 25.06.2017.
 */
class WeekDataRepository @Inject constructor(
        private val mWeekEntityMapper: WeekEntityMapper
): WeekRepository {

    override fun addNewDay(week: Week) {
        Realm.getDefaultInstance().use { realm ->
            realm.executeTransaction { realm ->
                realm.copyToRealmOrUpdate(mWeekEntityMapper.reverseMap(week))
            }
        }
    }

    override fun getWeek(period: String): Week? {
        return Realm.getDefaultInstance().use { realm ->
            val result = realm.where(WeekEntity::class.java)
                    .equalTo(WeekEntity.PERIOD, period)
                    .findFirst()

            mWeekEntityMapper.map(result)
        }
    }

    override fun getAllWeek(): List<Week> {
        return Realm.getDefaultInstance().use { realm ->
            val result = realm.where(WeekEntity::class.java)
                    .findAll()

            mWeekEntityMapper.map(result).filterNotNull()
        }
    }
}