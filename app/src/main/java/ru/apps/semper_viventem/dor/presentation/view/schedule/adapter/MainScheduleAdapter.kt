package ru.apps.semper_viventem.dor.presentation.view.schedule.adapter

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.pawegio.kandroid.dp
import com.pawegio.kandroid.inflateLayout
import kotlinx.android.synthetic.main.layout_week_day.view.*
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.presentation.common.WeekDayIdMapper
import ru.apps.semper_viventem.dor.presentation.common.factory.DecoratorFactory
import ru.apps.semper_viventem.dor.presentation.models.SubjectModel
import ru.apps.semper_viventem.dor.presentation.models.WeekDayModel

/**
 * @author Kulikov Konstantin
 * @since 27.07.2017.
 */
class MainScheduleAdapter(
        private val mListener: OnListener
): RecyclerView.Adapter<MainScheduleAdapter.MainScheduleHolder>() {

    private var mWeekDayList: List<WeekDayModel> = emptyList()
    private var mAdapterMap: HashMap<Int, ScheduleSubjectsAdapter> = HashMap()
    private var isEditMode = false

    fun setData(weekDays: List<WeekDayModel>) {
        mWeekDayList = weekDays
        notifyDataSetChanged()
        mAdapterMap.clear()
    }

    fun turnOnEditMode() {
        isEditMode = true
        notifyDataSetChanged()
    }

    fun turnOffEditMode() {
        isEditMode = false
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainScheduleHolder {
        return MainScheduleHolder(parent.context.inflateLayout(R.layout.layout_week_day, attachToRoot =  false))
    }

    override fun onBindViewHolder(holder: MainScheduleHolder?, position: Int) {
        holder?.bind(mWeekDayList[position])
    }

    override fun getItemCount(): Int {
        return mWeekDayList.size
    }

    inner class MainScheduleHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(weekDay: WeekDayModel) {

            itemView.vTextWeekDayTitle.text = itemView.context.getString(WeekDayIdMapper.mapToTextId(weekDay.weekDay))
            itemView.vTextWeekDayDate.text = weekDay.date

            if (android.os.Build.VERSION.SDK_INT >= 21) {
                if (isEditMode) {
                    itemView.vWeekDayPlace.animate().z(itemView.context.dp(5).toFloat()).start()
                } else {
                    itemView.vWeekDayPlace.animate().z(itemView.context.dp(2).toFloat()).start()
                }
            }


            // Если адаптера нет, то создаем
            if (mAdapterMap[weekDay.weekDay] == null) {
                mAdapterMap.put(weekDay.weekDay, ScheduleSubjectsAdapter(object : ScheduleSubjectsAdapter.OnSelectListener {
                    override fun onSelected(position: Int, weekDay: Int, subject: SubjectModel) {
                        mListener.onSubjectSelected(position, weekDay, subject)
                    }

                    override fun onLongSelected(subject: SubjectModel) {
                        mListener.onSubjectLongSelected(subject)
                    }

                    override fun onAddNewSubjectSelected(weekDay: Int) {
                        mListener.onSubjectAddSelected(weekDay)
                    }
                }, weekDay.weekDay))
            }
            mAdapterMap[weekDay.weekDay]?.setEditMode(isEditMode)

            itemView.vRecyclerView.layoutManager = LinearLayoutManager(itemView.context)
            itemView.vRecyclerView.adapter = mAdapterMap[weekDay.weekDay]
            itemView.vRecyclerView.addItemDecoration(DecoratorFactory().getDefaultDecorator())
            mAdapterMap[weekDay.weekDay]?.setData(weekDay.subjects)

        }
    }

    interface OnListener {
        fun onSubjectSelected(position: Int, weekDay: Int, subjectModel: SubjectModel)

        fun onSubjectLongSelected(subjectModel: SubjectModel)

        fun onSubjectAddSelected(weekDay: Int)
    }
}