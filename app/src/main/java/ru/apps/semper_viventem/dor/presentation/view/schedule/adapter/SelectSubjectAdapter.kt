package ru.apps.semper_viventem.dor.presentation.view.schedule.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.pawegio.kandroid.inflateLayout
import kotlinx.android.synthetic.main.holder_select_subject.view.*
import kotlinx.android.synthetic.main.holder_select_subject_remove.view.*
import ru.apps.semper_viventem.dor.R
import ru.apps.semper_viventem.dor.presentation.models.SubjectModel

/**
 * @author Kulikov Konstantin
 * @since 26.06.2017.
 */
class SelectSubjectAdapter(
        private val mListener: OnSelectListener,
        private val mShowRemoveItem: Boolean = false
) : RecyclerView.Adapter<SelectSubjectAdapter.BaseSelectSubjectHolder>() {

    companion object {
        const val TYPE_DEFAULT = 0
        const val TYPE_REMOVE_OR_ADD = 1
    }

    private var mSubjects: List<SubjectModel> = emptyList()

    fun setData(data: List<SubjectModel>) {
        mSubjects = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseSelectSubjectHolder {
        return when(viewType) {
            TYPE_DEFAULT -> SelectSubjectHolder(parent.context.inflateLayout(R.layout.holder_select_subject, attachToRoot = false))
            TYPE_REMOVE_OR_ADD -> RemoveOrAddHolder(parent.context.inflateLayout(R.layout.holder_select_subject_remove, attachToRoot = false))
            else -> throw Exception("unknown type: $viewType")
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (!mShowRemoveItem) return TYPE_DEFAULT
        return when (position) {
            0 -> TYPE_REMOVE_OR_ADD
            else -> TYPE_DEFAULT
        }
    }

    override fun onBindViewHolder(holder: BaseSelectSubjectHolder?, position: Int) {
        when (holder) {
            is SelectSubjectHolder -> holder.bind(mSubjects[position - (if (mShowRemoveItem) 1 else 0)])
            is RemoveOrAddHolder -> holder.bind()
        }
    }

    override fun getItemCount(): Int {
        return if (mShowRemoveItem)
            mSubjects.size + 1
        else
            mSubjects.size
    }

    abstract class BaseSelectSubjectHolder(itemView: View): RecyclerView.ViewHolder(itemView)

    inner class RemoveOrAddHolder(itemView: View): BaseSelectSubjectHolder(itemView) {
        fun bind() {
            itemView.vTextRemoveSubject.text = itemView.context.getString(R.string.holder_select_subject_remove)
            itemView.setOnClickListener { mListener.removeSubject() }
        }
    }

    inner class SelectSubjectHolder(itemView: View): BaseSelectSubjectHolder(itemView) {
        fun bind(subject: SubjectModel) {
            itemView.vTextSubjectName.text = subject.name
            itemView.setOnClickListener { mListener.onSelected(subject) }
        }
    }

    interface OnSelectListener {

        /**
         * Выбран предмет
         * @param subject модель предмета
         */
        fun onSelected(subject: SubjectModel)

        /**
         * Удалить текущий предмет
         */
        fun removeSubject()
    }
}