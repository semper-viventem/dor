package ru.apps.semper_viventem.dor.presentation.view.about

import android.content.Context
import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import ru.apps.semper_viventem.dor.R

/**
 * @author Kulikov Konstantin
 * @since 12.09.2017.
 */
class AboutDialog(
        mContext: Context,
        mOkListener: (dialog: DialogInterface, which: Int) -> Unit = { _, _ -> }
) {

    private val dialog = AlertDialog.Builder(mContext)
            .setTitle(mContext.getString(R.string.dialog_about_title))
            .setMessage(mContext.getString(R.string.dialog_about_message))
            .setPositiveButton(mContext.getString(R.string.good), mOkListener)
            .create()

    fun show() {
        dialog.show()
    }
}