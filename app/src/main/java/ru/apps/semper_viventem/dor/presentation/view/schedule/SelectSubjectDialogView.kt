package ru.apps.semper_viventem.dor.presentation.view.schedule

import com.arellomobile.mvp.MvpView
import ru.apps.semper_viventem.dor.presentation.models.SubjectModel

/**
 * @author Kulikov Konstantin
 * @since 26.06.2017.
 */
interface SelectSubjectDialogView : MvpView {

    /**
     * Список предметов загружен
     * @param subjects список моделей предметов
     */
    fun onSubjectLoaded(subjects: List<SubjectModel>)
}