package ru.apps.semper_viventem.dor.domain.iteractor

import io.adev.rxwrapper.RxAdapter
import io.reactivex.ObservableEmitter
import ru.apps.semper_viventem.dor.domain.model.Call
import ru.apps.semper_viventem.dor.domain.repository.CallRepository
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 11.06.2017.
 */
class GetCallById @Inject constructor(
        val mCallRepository: CallRepository
) : RxAdapter<Call, Int>() {

    override fun execute(emitter: ObservableEmitter<Call>, criteria: Int?) {
        val result = mCallRepository.getCallById(criteria!!)
        emitter.onNext(result)
    }
}