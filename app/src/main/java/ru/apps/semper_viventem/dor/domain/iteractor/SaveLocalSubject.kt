package ru.apps.semper_viventem.dor.domain.iteractor

import io.adev.rxwrapper.RxAdapter
import io.reactivex.ObservableEmitter
import ru.apps.semper_viventem.dor.domain.model.Subject
import ru.apps.semper_viventem.dor.domain.repository.SubjectRepository
import timber.log.Timber
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 08.05.2017.
 */
class SaveLocalSubject @Inject constructor(
        private val mSubjectRepository: SubjectRepository
): RxAdapter<Void, Subject>() {

    override fun execute(emitter: ObservableEmitter<Void>, criteria: Subject?) {
        mSubjectRepository.addSubject(criteria!!)
        Timber.d(criteria.toString())
    }
}