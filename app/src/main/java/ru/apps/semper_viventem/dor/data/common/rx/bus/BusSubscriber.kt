package ru.apps.semper_viventem.dor.data.common.rx.bus

import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import ru.apps.semper_viventem.dor.data.common.rx.bus.BusEvent
import ru.apps.semper_viventem.dor.data.common.rx.bus.BusSubscriberContract
import ru.apps.semper_viventem.dor.data.common.rx.bus.RxBus
import javax.inject.Inject
import kotlin.reflect.KClass

/**
 * @author Kulikov Konstantin
 * @since 06.05.17
 */
open class BusSubscriber<E : BusEvent> @Inject constructor() : BusSubscriberContract<E> {

    val subscriptions = mutableListOf<Disposable>()

    private lateinit var mBus: RxBus<E>
    override fun bus(bus: RxBus<E>) {
        mBus = bus
    }

    override fun <T : E> subscribe(clazz: KClass<T>, callback: (T) -> Unit) {
        subscribe(createSubscriptions(clazz), callback)
    }

    override fun unsubscribe() {
        subscriptions.forEach { it.dispose() }
        subscriptions.clear()
    }

    protected fun <T : E> createSubscriptions(clazz: KClass<T>): Observable<T> {
        return mBus.events(clazz.java)
    }

    protected fun <T : E> subscribe(subscription: Observable<T>, callback: (T) -> Unit) {
        subscriptions.add(subscription.subscribe(callback))
    }
}