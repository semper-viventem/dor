package ru.apps.semper_viventem.dor.domain.iteractor

import io.adev.rxwrapper.RxAdapter
import io.reactivex.ObservableEmitter
import ru.apps.semper_viventem.dor.domain.repository.SubjectRepository
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 09.05.2017.
 */
class RemoveSubjectById @Inject constructor(
        private var mSubjectRepository: SubjectRepository
) : RxAdapter<Void, Int>() {

    override fun execute(emitter: ObservableEmitter<Void>, criteria: Int?) {
        mSubjectRepository.removeSubjectById(criteria!!)
    }
}