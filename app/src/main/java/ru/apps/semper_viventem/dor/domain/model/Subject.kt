package ru.apps.semper_viventem.dor.domain.model

/**
 * @author Kulikov Konstantin
 * @since 06.05.2017.
 *
 * @property id идентификатор
 * @property name имя предмета
 * @property classRoom наименование стандартной аудитории для проведения занятий
 */
class Subject(
        var id: Int = 0,
        var name: String = "",
        var teachers: List<Teacher> = emptyList(),
        var classRoom: String = "",
        var info: String = ""
) {
    override fun toString(): String {
        return "Subject(id='$id', name='$name', teachers=$teachers, classRoom='$classRoom', info='$info')"
    }
}