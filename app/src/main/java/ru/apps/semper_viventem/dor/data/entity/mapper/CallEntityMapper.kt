package ru.apps.semper_viventem.dor.data.entity.mapper

import ru.apps.semper_viventem.dor.data.entity.CallEntity
import ru.apps.semper_viventem.dor.domain.model.Call
import ru.apps.semper_viventem.dor.domain.model.mapper.TwoWayMapper
import javax.inject.Inject

/**
 * @author Kulikov Konstantin
 * @since 11.06.2017.
 */
class CallEntityMapper @Inject constructor(): TwoWayMapper<CallEntity, Call> {

    override fun reverseMap(from: Call): CallEntity {
        return CallEntity().apply {
            id = from.id
            begin = from.begin
            end = from.end
            day = from.day
        }
    }

    override fun map(from: CallEntity): Call {
        return Call(
                id = from.id?:0,
                begin = from.begin,
                end = from.end,
                day = from.day
        )
    }
}